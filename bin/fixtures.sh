#!/usr/bin/env bash
projectPath="$(dirname "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )")";

database_name=base5;

if ! mysql -uweb -pweb -D$database_name -e "update user set team_id = NULL"
then
  exit
fi

mysql -uweb -pweb -D$database_name -e "update form_group set upper_form_group_id = NULL"

"$projectPath"/bin/console doctrine:fixtures:load --no-interaction
