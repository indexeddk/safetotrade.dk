#Symfony 5

##Install
    # cp .env.local.dist .env.local
    # cp bin/.releash.sh.dist bin/.releash.sh
    # composer install
    # yarn install (ONLY IF DEV)
    # php bin/console doctrine:migration:migrate --no-interaction
    # php bin/console doctrine:fixtures:load --no-interaction (ONLY IF DEV)
    # php bin/console app:setup (ONLY IF PROD)
    
####Setup crontab
    0 2 * * *   php bin/console users:cleanup   >/dev/null 2>&1

#
#
##dev
####Using Webpack Encore
    # yarn encore dev --watch
    # yarn encore prod
    
Once the project is live uncomment /public/build/ in _/.gitignore_ to transfer building app files to prod server:

    #/public/build/
    
####Update translation files

da: 

    php bin/console translation:update --dump-messages --force --output-format=yml --prefix="" da
    
en: 

    php bin/console translation:update --dump-messages --force --output-format=yml --prefix="__" en
    
... or just use (only 'da' for now)
    
    # .bin/trans_update.sh
    
#
#
## sortering af bruger uden discriminator

#### nuværende løsning
    SELECT * FROM `user` where JSON_SEARCH(roles, 'one', "ROLE_ADMIN") is not null

Det vil ikke virke med nedarvning

#### Løsning med level

    integer roleLevel

    110     ROLE_HOSTMASTER
    100     ROLE_SUPER_ADMIN
    90      ROLE_ADMIN
    80      ROLE_LEADER
    70      ROLE_EMPLOYEE
    40      ROLE_VOLUNTARY
    30      ROLE_CUSTOMER
    10      ROLE_USER
    
Søgning

    SELECT * FROM `user` where roleLevel >= '90'

roles kan vi beholde til hvis der er tilføjelser der ikke er i hieraki
