<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class MailHelper extends AbstractController
{
    /** @var MailerInterface  */
    private $mailer;
    /** @var string  */
    private $adminMail;
    /** @var string  */
    private $adminName;

    public function __construct(MailerInterface $mailer, string $adminMail, string $adminName)
    {
        $this->mailer = $mailer;
        $this->adminMail = $adminMail;
        $this->adminName = $adminName;
    }

    public function sendMail($to, $from = false, $subject = false, $data = false, $template = "mail/default.html.twig", $basicHtml = false)
    {
        //to
        $toEmail = $to;
        $toName = "";
        if (is_array($to)) {
            $toEmail = $to[0];
            $toName = $to[1];
        }
        if (is_object($to)) {
            if ($to instanceof User) {
                $toEmail = $to->getEmail();
                $toName = $to->getName();
            }
        }

        //from
        $fromEmail = $from;
        $fromName = "";
        if (is_array($from)) {
            $fromEmail = $from[0];
            $fromName = $from[1];
        }
        if (!$fromEmail) {
            $fromEmail = $this->adminMail;
            if (!trim($fromName)) {
                $fromName = $this->adminName;
            }
        }

        //init Symfony Mailer
        if (!$template && $basicHtml) {

            //basic html mail
            $email = (new TemplatedEmail())
                ->to(new Address($toEmail, $toName))
                ->from(new Address($fromEmail, $fromName))
                ->subject($subject)
                ->html($basicHtml)
            ;
        }
        else {

            //twig html mail
            $email = (new TemplatedEmail())
                ->to(new Address($toEmail, $toName))
                ->from(new Address($fromEmail, $fromName))
                ->subject($subject)
                ->htmlTemplate($template)
                ->context([
                    "data" => $data,
                    "title" => (isset($data["title"]) ? $data["title"] : ""),
                    "preheader" => (isset($data["preheader"]) ? $data["preheader"] : ""),
                    "firstname" => (isset($data["firstname"]) ? $data["firstname"] : ""),
                    "senderInfo" => (isset($data["senderInfo"]) ? $data["senderInfo"] : ""),
                    "content" => (isset($data["content"]) ? $data["content"] : ""),
                    "toEmail" => $toEmail,
                ])
            ;
        }

        //send and return
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            return false;
        }
        return true;
    }
}

