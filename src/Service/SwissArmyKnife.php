<?php

namespace App\Service;

use App\Controller\ControllerBaseTrait;
use App\Entity\Deal;
use App\Entity\DeletedUser;
use App\Entity\Host;
use App\Entity\Notification;
use App\Entity\User;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @property SluggerInterface slugger
 * @property Host host
 * @property RequestStack request
 * @property TranslatorInterface translator
 */
class SwissArmyKnife extends AbstractController
{

    use ControllerBaseTrait;

    public function __construct(RequestStack $request, TranslatorInterface $translator, SluggerInterface $slugger, HostHelper $hostHelper)
    {
        $this->request = $request;
        $this->translator = $translator;
        $this->slugger = $slugger;
        $this->host = $hostHelper->getHost();
    }

    /**
     * @param $string
     * @param string $domain
     * @return false|string
     */
    public function t($string, $domain = "backend")
    {
        return $this->translator->trans($string, [], $domain);
    }

    /**
     * @param int $lenght
     * @param bool $excludeSpecialChars
     * @return false|string
     */
    public function generatePassword($lenght = 8, $excludeSpecialChars = false)
    {
        $chars = "ABCDEFGHJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789";
        $specialChars = "!&#*_?%";

        if ($excludeSpecialChars) {
            $password = substr(str_shuffle($chars), 0, $lenght);
        }
        else {
            $password = substr(str_shuffle($chars), 0, ($lenght-1));
            $specialChar = substr(str_shuffle($specialChars), 0, 1);
            $password = substr_replace($password, $specialChar, rand(0,$lenght), 0);
        }

        return $password;
    }

    /**
     * @param int $lenght
     * @return false|string
     * @throws Exception
     */
    public function generateVerificationNumber($lenght = 6)
    {
        if($lenght > 19) {
            //(int)$number max out on this number: 9223372036854775807
            throw new Exception("MAX LENGHT = 19");
        }

        //$number = "";
        //for ($i=0 ; $i<$lenght ; $i++) {
        //    $int = rand(1,9);
        //    $number .= $int;
        //}

        $number = substr(str_shuffle('123456789123456789123456789123456789'), 0, $lenght);

        return (int)$number;
    }

    /**
     * @param bool $seed
     * @param bool $md5
     * @return false|string
     */
    public function makeSecret($seed = false, $md5 = true)
    {
        if (!$seed) {
            $seed = $this->generatePassword(10);
        }
        $secret = password_hash($seed, PASSWORD_DEFAULT);
        if ($md5) {
            $secret = md5($secret);
        }
        return $secret;
    }

    /**
     * @param User $entity
     * @return false|string
     */
    public function getProfileImagePath(User $entity)
    {
        $possibleGravatar = "https://s.gravatar.com/avatar/".md5($entity->getEmail())."?s=254&r=PG&d=404";
        if (@file_get_contents($possibleGravatar)) {
            $profileImagePath = $possibleGravatar;
        }
        else {
            $profileImagePath = User::DEFAULT_PROFILE_IMAGE_PATH;
        }

        return $profileImagePath;
    }

    /**
     * @param bool $valueAsKeys
     * @param bool $includeChooseValue
     * @return array
     */
    public function getDealConditions($valueAsKeys = false, $includeChooseValue = false)
    {
        $deal = new Deal();

        $options = [];
        if($includeChooseValue) {
            $options["none"] = $this->t("Vælg stand");
        }

        $options[10] = "10 - ".$this->t("Helt ny");
        $options[9] = "9 - ".$this->t("Ingen brugsspor");
        $options[8] = "8 - ".$this->t("Få usynlige brugsspor");
        $options[7] = "7 - ".$this->t("Få synlige brugsspor");
        $options[6] = "6 - ".$this->t("Flere synlige brugsspor");
        $options[5] = "5 - ".$this->t("Kettere slidt");
        $options[4] = "4 - ".$this->t("Tydelig slitage");
        $options[3] = "3 - ".$this->t("Meget slidt");
        $options[2] = "2 - ".$this->t("Tæt ved defekt");
        $options[1] = "1 - ".$this->t("Defekt");

        //$options[$deal::CONDITION_PERFECT] = $this->t("Perfekt");
        //$options[$deal::CONDITION_GOOD] = $this->t("God");
        //$options[$deal::CONDITION_FAIR] = $this->t("Rimelig");
        //$options[$deal::CONDITION_BAD] = $this->t("Dårlig");
        //$options[$deal::CONDITION_BROKEN] = $this->t("Defekt");

        if ($valueAsKeys) {
            $options = array_flip($options);
        }

        return $options;
    }

    /**
     * @param $type
     * @param User $toUser
     * @param bool|User $fromUser
     * @param bool|Deal $deal
     * @param bool|string $text
     * @return Notification
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function notify($type, User $toUser, $fromUser = false, $deal = false, $text = false)
    {
        if (!$fromUser) {
            $fromUser = $this->getUser();
        }
        $entity = new Notification();
        $entity->setHost($this->host);
        $entity->setUuid(Uuid::v4());
        $entity->setType($type);
        $entity->setFromUser($fromUser);
        $entity->setToUser($toUser);
        if($deal) {
            $entity->setDeal($deal);
        }
        if($text) {
            $entity->setText($text);
        }

        $this->em()->persist($entity);
        $this->em()->flush();

        //@todo: send notification to buyer?

        return $entity;
    }

    /**
     * @param $imageFile
     * @param $imageDir
     * @return false|string
     */
    public function uploadImage($imageFile, $imageDir)
    {
        $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = date("YmdHis")."_".$safeFilename.".".$imageFile->guessExtension();
        try {
            $imageFile->move(
                $this->getParameter($imageDir) ?? $imageDir,
                $newFilename
            );
        } catch (FileException $e) {
            // ...
        }

        return $newFilename;
    }

    /**
     * @param User $user
     * @return false|string
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteUser(User $user)
    {
        //create "meta user" which blocks e-mail + phone for 1 year.
        $deleteUser = new DeletedUser();
        $deleteUser->setHost($this->host);
        $deleteUser->setUuid(Uuid::v4());
        $deleteUser->setUserUuid($user->getUuid());
        $deleteUser->setName($user->getName());
        $deleteUser->setAddress($user->getAddress());
        $deleteUser->setZip($user->getZip());
        $deleteUser->setCity($user->getCity());
        $deleteUser->setEmail($user->getEmail());
        $deleteUser->setPhone($user->getPhone());

        $this->em()->persist($deleteUser);
        $this->em()->flush();

        $randomString = $this->generatePassword(20, true);
        $randomNumber = $this->generateVerificationNumber(16);

        $user->setDeleted(true);
        $user->setFirstname("Slettet bruger");
        $user->setLastname("");
        $user->setAddress("");
        $user->setZip(null);
        $user->setCity("");
        $user->setCountry("");
        $user->setPhone($randomNumber);
        $user->setEmail($randomString."@deleted.dk");
        $user->setUsername($randomString."@deleted.dk");
        $user->setPassword($randomString); //break password!
        $user->setProfileImagePath("");
        $user->setBirthday(null);
        $user->setSlug("");

        $this->em()->flush();

        return true;
    }

    ///**
    // * @param $imageFile
    // * @param $width
    // * @param $height
    // * @return false|string
    // */
    //public function BETAresizeBETA($imageFile, $width, $height)
    //{
    //    $runtimeConfig = array(
    //        "thumbnail" => array(
    //            //"size" => array(50, 50)
    //            "size" => array($width, $height)
    //        )
    //    );
    //
    //    //return "<img src=\"".$this["imagine"]->filter('/relative/path/to/image.jpg', 'my_thumb', $runtimeConfig)."\" />";
    //    return "<img src=\"".$this["imagine"]->filter($imageFile, 'custom', $runtimeConfig)."\" />";
    //}
}

