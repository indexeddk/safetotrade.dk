<?php

namespace App\Service;

use App\Entity\Host;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;

class HostHelper extends AbstractController
{
    /** @var RequestStack $request */
    private $request;

    /** @var EntityManagerInterface $em */
    private $em;

    public function __construct(RequestStack $request, EntityManagerInterface $em)
    {
        $this->request = $request;
        $this->em = $em;
    }

    public function getHost()
    {
        $request = $this->request->getCurrentRequest();

        if(!$request) {
            return false;
        }

        $host = $request->headers->get('host');
        $host = str_replace("www.", "", $host);
        $host = trim(mb_strtolower($host, "UTF-8"));

        /** @var Host $host */
        $host = $this->em->getRepository('App:Host')->findOneBy(['domain' => $host]);

        if(!$host) {
            $host = $this->em->getRepository('App:Host')->findOneBy(['id' => 1]);
        }

        return $host;
    }
}

