<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Host;
use App\Entity\User;
use App\Service\SwissArmyKnife;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;
    /** @var SwissArmyKnife */
    private $swissArmyKnife;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->swissArmyKnife = $swissArmyKnife;
    }

    public function getDependencies()
    {
        return array(
            HostFixtures::class,
            CompanyFixtures::class,
        );
    }

    public function load(ObjectManager $manager)
    {

        /** @var Host $host1 */
        $host1 = $this->getReference("host1");

        /** @var Company $company1 */
        $company1 = $this->getReference("company1");

        $entity = $this->createEntity("hostmaster", "hostmaster", "Hostmaster", "Admin", ["ROLE_HOSTMASTER"], $host1, $company1);
        $manager->persist($entity);


        /** @var Company $company2 */
        $company2 = $this->getReference("company2");

        $entity = $this->createEntity("sadmin", "sadmin", "Super", "Admin", ["ROLE_SUPER_ADMIN"], $host1, $company2);
        $manager->persist($entity);

        $entity = $this->createEntity("admin", "admin", "Company", "Admin", ["ROLE_ADMIN"], $host1, $company2);
        $manager->persist($entity);

        //$entity = $this->createEntity("", "user10", "user10", "user10", ["ROLE_USER"], $host1, $company2);
        //$manager->persist($entity);


        $manager->flush();
    }

    private function createEntity(string $email, string $username, string $firstname, string $lastname, array $roles, Host $host, Company $company = null): User
    {
        $entity = new User();
        $entity
            ->setHost($host)
            ->setCompany($company)
            ->setUuid(Uuid::v4())
            ->setEmail($email)
            ->setUsername($username)
            ->setRoles($roles)
            ->setFirstname($firstname)
            ->setLastname($lastname)
        ;

        //password and secret
        $password = "test";
        $entity
            ->setPassword($this->passwordEncoder->encodePassword($entity, $password))
            ->setSecret($this->swissArmyKnife->makeSecret($password));

        $this->setReference($username, $entity);

        return $entity;

    }
}
