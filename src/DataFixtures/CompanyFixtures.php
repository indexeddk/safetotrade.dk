<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Host;
use App\Service\SwissArmyKnife;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CompanyFixtures extends Fixture implements DependentFixtureInterface
{
    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;
    /** @var SwissArmyKnife */
    private $swissArmyKnife;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->swissArmyKnife = $swissArmyKnife;
    }

    public function getDependencies()
    {
        return array(
            HostFixtures::class,
        );
    }

    public function load(ObjectManager $manager)
    {
        /** @var Host $host1 */
        $host1 = $this->getReference("host1");

        $entity = $this->createEntity('company1', "Indexed", $host1);
        $manager->persist($entity);

        $entity = $this->createEntity("company2", "Base5company", $host1);
        $manager->persist($entity);


        $manager->flush();
    }

    private function createEntity(string $reference, string $name, Host $host): Company
    {
        $entity = new Company();
        $entity
            ->setName($name)
            ->setHost($host);

        $this->setReference($reference, $entity);

        return $entity;

    }
}
