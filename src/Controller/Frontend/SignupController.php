<?php

namespace App\Controller\Frontend;

use App\Controller\ControllerBaseTrait;
use App\Controller\ControllerExternalServices;
use App\Entity\Host;
use App\Entity\User;
use App\Form\Signup\SignupInfoType;
use App\Form\Signup\SignupStartType;
use App\Form\Signup\SignupVerifyType;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property ControllerExternalServices externalServices
 * @property Host host
 */
class SignupController extends AbstractController
{
    use ControllerBaseTrait;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     * @param ControllerExternalServices $externalServices
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife, ControllerExternalServices $externalServices)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
        $this->externalServices = $externalServices;
    }

    /**
     * SIGN UP FLOW | 1: start
     *
     * @Route({
     *     "en": "/signup/start/{uuid}",
     *     "da": "/tilmeld/start/{uuid}"
     * }, name="frontend_signup_start_with_id")
     *
     * @Route({
     *     "en": "/signup/start",
     *     "da": "/tilmeld/start"
     * }, name="frontend_signup_start")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function signupStart(Request $request)
    {
        //reset
        $this->get("session")->remove("indexed/signup/confirmed");

        //do not start on a new entity just because user returns to step 1 (OBS!)
        if (!$request->get("uuid") && $this->get("session")->get("indexed/signup/entity")) {
            $sessionEntity = $this->get("session")->get("indexed/signup/entity");
            $entity = $this->em()->getRepository("App:User")->findOneBy(["host" =>$this->host, "uuid" => $sessionEntity->getUuid()]);
            if($entity) {
                return $this->redirectToRoute("frontend_signup_start_with_id", ["uuid" => $sessionEntity->getUuid()]);
            }
            else {
                $this->get("session")->remove("indexed/signup/entity");
                return $this->redirectToRoute("frontend_signup_start_with_id");
            }
        }
        else {
            $entity = new User();
            $entity->setUuid(Uuid::v4());
            $entity->setHost($this->host);
            $entity->setRoles(["ROLE_CUSTOMER"]);
            $entity->setVerificationCodeEmail($this->swissArmyKnife->generateVerificationNumber(6));
            $entity->setVerificationCodeSms($this->swissArmyKnife->generateVerificationNumber(6));
            $entity->setCountry("DK");
        }

        //if user returns to step 1 (OBS!)
        if($request->get("uuid")) {
            $entity = $this->em()->getRepository("App:User")->findOneBy(["host" =>$this->host, "uuid" => $request->get("uuid")]);
        }

        //just in case: existing verifid user not allowed to "begin sign up again"
        if($entity->getVerifiedSms() && $entity->getVerifiedEmail()) {
            $this->get("session")->remove("indexed/signup/entity");
            return $this->redirectToRoute("frontend_signup_start");
        }

        //form
        $form = $this->createForm(SignupStartType::class, $entity);
        $form->add("submit", SubmitType::class, ["label" => "Videre"]);

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {

            //password
            $password = $form->get("password")->getData();

            //save password until mail is sent
            $this->get("session")->set("indexed/signup/password", $password);

            //set password + secret + username
            $entity->setPassword($this->passwordEncoder->encodePassword($entity, $password));
            $entity->setSecret($this->swissArmyKnife->makeSecret($password));
            $entity->setUsername($entity->getEmail());

            $phone = $entity->getPhone();
            $phone = str_replace(["+45"," "], ["",""], $phone);
            if(substr($phone, 0 ,4) == "0045") {
                $phone = str_replace("0045", "", $phone);
            }
            $entity->setPhone($phone);

            //save entity, if returns to step 1 (OBS!)
            if (!$this->get("session")->get("indexed/signup/entity")) {
                $this->em()->persist($entity);
            }
            $this->em()->flush();

            //does deleted user exist
            $deletedUserByEmail = $this->em()->getRepository("App:DeletedUser")->findOneBy(["host" => $this->host, "email" => $entity->getEmail()]);
            $deletedUserByPhone = $this->em()->getRepository("App:DeletedUser")->findOneBy(["host" => $this->host, "phone" => $entity->getPhone()]);
            $userByPhone = $this->em()->getRepository("App:User")->findOneBy(["host" => $this->host, "phone" => $entity->getPhone()]);
            if ($userByPhone == $entity) {
                $userByPhone = false;
            }

            $error = false;
            if ($deletedUserByEmail) {
                $this->addFlash("error", "E-mail findes allerede. Indtast venligst en ny e-mail og prøv igen");
                $error = true;
            }
            if ($deletedUserByPhone || $userByPhone) {
                $this->addFlash("error", "Mobilnummer findes allerede. Indtast venligst et nyt mobilnummer og prøv igen");
                $error = true;
            }
            if (strlen($phone) != 8) {
                $this->addFlash("error", "Mobilnummer skal være på præcis 8 tegn");
                $error = true;
            }

            if ($error) {
                return $this->redirectToRoute("frontend_signup_start_with_id", ["uuid" => $entity->getUuid()]);
            }

            //save entity in case user returns to step 1 (OBS!)
            $this->get("session")->set("indexed/signup/entity", $entity);

            return $this->redirectToRoute("frontend_signup_info", ["uuid" => $entity->getUuid()]);
        }

        return $this->render("frontend/signup/start.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * SIGN UP FLOW | 2: info
     *
     * @Route({
     *     "en": "/signup/info/{uuid}",
     *     "da": "/tilmeld/info/{uuid}"
     * }, name="frontend_signup_info")
     * @param Request $request
     * @param User $entity
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function signupInfo(Request $request, User $entity)
    {
        if($entity->getVerifiedSms() && $entity->getVerifiedEmail()) {
            return $this->redirectToRoute("frontend_signup_start");
        }

        $form = $this->createForm(SignupInfoType::class, $entity);
        $form->add("submit", SubmitType::class, ["label" => "Videre"]);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {

            //send verification mail to new user
            $this->signupVerifyEmailSendMail($entity);
            $entity->setVerifiedEmail(false);

            //send verification mail to new user
            $this->signupVerifySmsSendSms($entity);
            $entity->setVerifiedSms(false);

            $this->em()->flush();

            return $this->redirectToRoute("frontend_signup_verify", ["uuid" => $entity->getUuid()]);
        }

        return $this->render("frontend/signup/info.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * SIGN UP FLOW | 3: verify
     *
     * @Route({
     *     "en": "/signup/verify/{uuid}",
     *     "da": "/tilmeld/verificering/{uuid}"
     * }, name="frontend_signup_verify")
     * @param Request $request
     * @param User $entity
     * @return RedirectResponse|Response
     */
    public function signupVerify(Request $request, User $entity)
    {
        if($entity->getVerifiedSms() && $entity->getVerifiedEmail() && $this->get("session")->get("indexed/signup/confirmed")) {
            $this->get("session")->remove("indexed/signup/confirmed");
            return $this->redirectToRoute("frontend_signup_confirmation", ["uuid" => $entity->getUuid()]);
        }

        $form = $this->createForm(SignupVerifyType::class, $entity);
        $form->add("submit", SubmitType::class, ["label" => "Opret profil"]);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {

            //verified
            $verified = false;
            if($entity->getVerifiedSms() && $entity->getVerifiedEmail()) {
                $verified = true;
            }

            //verified - error messages
            if(!$entity->getVerifiedEmail()) {
                $this->addFlash("error", "Du mangler at bekræfte din e-mail. Bekræft din e-mail og prøv igen.");
            }
            if(!$entity->getVerifiedSms()) {
                $this->addFlash("error", "Du mangler at bekræfte dit mobilnummer. Bekræft dit mobilnummer og prøv igen.");
            }

            if($verified) {
                $password = $this->get("session")->get("indexed/signup/password");
                $this->get("session")->remove("indexed/signup/password");
                $this->get("session")->remove("indexed/signup/entity");

                //welcome mail
                $mailData = [];
                $mailData["content"] = $this->renderView("backend/mail/welcome.html.twig", [
                    "host" => $this->host->getDomain(),
                    "user" => $entity,
                    //"password" => $password
                    "password" => "(Selvvalgt)"
                ]);
                $mailData["firstname"] = $entity->getFirstname();
                $this->mailer->sendMail($entity, false, $this->swissArmyKnife->t("Velkommen", "mail"), $mailData);

                //add to ActiveCampaign
                $this->externalServices->activeCampaignSync($entity);

                return $this->redirectToRoute("frontend_signup_confirmation", ["uuid" => $entity->getUuid()]);
            }
        }

        return $this->render("frontend/signup/verify.html.twig", [
            "form" => $form->createView(),
            "user" => $entity
        ]);
    }

    /**
     * SIGN UP FLOW | 4: confirmation
     *
     * @Route({
     *     "en": "/signup/confirmation/{uuid}",
     *     "da": "/tilmeld/kvittering/{uuid}"
     * }, name="frontend_signup_confirmation")
     *
     * @param Request $request
     * @param User $entity
     * @return RedirectResponse|Response
     */
    public function signupConfirmation(Request $request, User $entity)
    {
        $this->get("session")->set("indexed/signup/confirmed", true);
        return $this->render("frontend/signup/confirmation.html.twig", [
            "user" => $entity
        ]);
    }

    /**
     * Helper Functions
     */

    /**
     * @Route("/signup/verify/email/{uuid}", name="frontend_signup_verify_email")
     * @param Request $request
     * @param User $entity
     * @return RedirectResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function signupVerifyEmail(Request $request, User $entity)
    {
        $verified = false;

        $realCode = $entity->getVerificationCodeEmail();
        $code = $request->get("code");
        if (trim($realCode) == trim($code)) {
            $verified = true;
            $entity->setVerifiedEmail(true);
            $this->em()->flush();
        }

        return $this->json([
            "verified" => $verified
        ]);
    }

    /**
     * @Route("/signup/verify/sms/{uuid}", name="frontend_signup_verify_sms")
     * @param Request $request
     * @param User $entity
     * @return RedirectResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function signupVerifySms(Request $request, User $entity)
    {
        $verified = false;

        $realCode = $entity->getVerificationCodeSms();
        $code = $request->get("code");
        if (trim($realCode) == trim($code)) {
            $verified = true;
            $entity->setVerifiedSms(true);
            $this->em()->flush();
        }

        return $this->json([
            "verified" => $verified
        ]);
    }

    /**
     * @Route("/signup/verify/email/resend/{uuid}", name="frontend_signup_verify_email_resend_code")
     * @param Request $request
     * @param User $entity
     * @return RedirectResponse|Response
     */
    public function signupVerifyEmailResendCodeMail(Request $request, User $entity)
    {
        return $this->json([
            "sent" => $this->signupVerifyEmailSendMail($entity)
        ]);
    }

    /**
     * @Route("/signup/verify/sms/resend/{uuid}", name="frontend_signup_verify_sms_resend_code")
     * @param Request $request
     * @param User $entity
     * @return RedirectResponse|Response
     */
    public function signupVerifySmsResendCodeMail(Request $request, User $entity)
    {
        return $this->json([
            "sent" => $this->signupVerifySmsSendSms($entity)
        ]);
    }

    /**
     * @param User $entity
     * @return bool
     */
    public function signupVerifyEmailSendMail(User $entity)
    {
        $entity->setVerificationCodeEmail($this->swissArmyKnife->generateVerificationNumber(6));
        $this->em()->flush();

        $mailData = [];
        $mailData["content"] = $this->renderView("backend/mail/signup_verify_email.html.twig", [
            "code" => $entity->getVerificationCodeEmail(),
        ]);
        $mailData["firstname"] = $entity->getFirstname();
        return $this->mailer->sendMail($entity, false, $this->swissArmyKnife->t("E-mail bekræftelseskode", "mail"), $mailData);
    }

    /**
     * @param User $entity
     * @return bool|JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function signupVerifySmsSendSms(User $entity)
    {

        //check for last sent
        if ($entity->getLastSmsSent()) {

            //only one sms pr minute
            if ($entity->getLastSmsSent()->format("Y-m-d H:i") == date("Y-m-d H:i")) {
                return "Kun én sms pr minut";
            }
        }
        $entity->setLastSmsSent(new Datetime("NOW"));

        $entity->setVerificationCodeSms($this->swissArmyKnife->generateVerificationNumber(6));
        $this->em()->flush();

        $smsText = "Din Safe2Trade bekræftelseskode: ".$entity->getVerificationCodeSms()." \n\nVenlig hilsen Safe2Trade\n\n";

        //return true;
        return $this->externalServices->suresms_send($entity->getPhone(), $smsText);
    }
}