<?php

namespace App\Controller\Frontend;

use App\Controller\ControllerBaseTrait;
use App\Entity\Blog;
use App\Entity\Host;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property Host host
 */
class PageController extends AbstractController
{
    use ControllerBaseTrait;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
    }

    /**
     * FRONTPAGE
     *
     * @Route({
     *     "en": "/home",
     *     "da": "/"
     * }, name="frontend_index")
     */
    public function index()
    {
        //$blogs = $this->em()->getRepository("App:Blog")->getEntityQuery(false);
        //return $this->render("frontend/frontpage.html.twig", [
        //    "blogs" => $blogs
        //]);
        return $this->redirectToRoute("app_login");
    }

    /**
     * TERMS: Privacy
     *
     * @Route({
     *     "en": "/terms/privacy",
     *     "da": "/privatlivspolitik"
     * }, name="frontend_terms_privacy")
     */
    public function frontendTermsPrivacy()
    {
        $pdfFileName = "privatlivspolitik.pdf";
        $file = new File(__DIR__."/../../../public/".$pdfFileName);
        return $this->file($file, $pdfFileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * TERMS: Privacy
     *
     * @Route({
     *     "en": "/terms/service",
     *     "da": "/tjeneste-og-bruger-vilkaar"
     * }, name="frontend_terms_service")
     */
    public function frontendTermsService()
    {
        $pdfFileName = "tjenesteogbrugervilkaar.pdf";
        $file = new File(__DIR__."/../../../public/".$pdfFileName);
        return $this->file($file, $pdfFileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * BLOG
     *
     * @Route({
     *     "en": "/en/blog/{slug}",
     *     "da": "/blog/{slug}"
     * }, name="blog")
     *
     * @param Blog $entity
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function blog(Blog $entity)
    {
        $blogs = $this->em()->getRepository("App:Blog")->getEntityQuery(false);
        return $this->render("frontend/blog.html.twig", [
            "blogs" => $blogs,
            "entity" => $entity
        ]);
    }

}