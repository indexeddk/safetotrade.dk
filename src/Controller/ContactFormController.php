<?php
namespace App\Controller;

use App\Entity\Host;
use App\Entity\Inquiry;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @Route("/contact-form")
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property Host host
 */

class ContactFormController extends AbstractController
{
    use ControllerBaseTrait;
    
    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
    }

    /**
     * Create inquiry
     *
     * @Route("/inquiry/create", name="contact_form_create_inquery")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createInquery(Request $request)
    {
        $email   = filter_var($request->get("email"),FILTER_SANITIZE_EMAIL);
        $subject = filter_var($request->get("subject"),FILTER_SANITIZE_STRING);
        $text    = filter_var($request->get("text"),FILTER_SANITIZE_STRING);

        $inquery = new Inquiry();
        $inquery->setHost($this->host);
        $inquery->setUuid(Uuid::v4());
        $inquery->setEmail($email);
        $inquery->setSubject($subject);
        $inquery->setText($text);

        $this->em()->persist($inquery);
        $this->em()->flush();

        $content = "<p>";
            $content .= "Der er kommet en ny kontakthenvendelse<br><br>";
            $content .= "<strong>E-mail:</strong> ".$email."<br>";
            $content .= "<strong>Emne:</strong> ".$subject."<br><br>";
            $content .= "<strong>Tekst:</strong><br><em>".$text."</em><br>";
        $content .= "<p>";

        $mailData = [
            "title" => "Kontaktformular",
            "preheader" => "Kontaktformular",
            "firstname" => "Safe2Trade",
            "senderInfo" => "",
            "content" => $content,
            "toEmail" => $email,
        ];

        $to = $this->getParameter("app.admin_email");
        $this->mailer->sendMail($to, $email, "Ny kontakthenvendelse", $mailData);

        return $this->json([
            "status" => "success"
        ]);
    }

}