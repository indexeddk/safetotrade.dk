<?php

namespace App\Controller\Login;

use App\Controller\ControllerBaseTrait;
use App\Entity\Content;
use App\Entity\Host;
use App\Entity\User;
use App\Form\UserNewPassword;
use App\Form\UserResetPassword;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class LoginController
 *
 * @package App\Controller\Login
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property Host host
 */
class LoginController extends AbstractController
{
    use ControllerBaseTrait;
    
    /**
     * AdminController constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->host = $this->hostHelper->getHost();
        $this->swissArmyKnife = $swissArmyKnife;
    }

    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //    $this->redirectToRoute("target_path");
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $contentHeadline = $this->getDoctrine()
            ->getRepository(Content::class)
            ->findOneBy(["name" => "login_intro_headline"])
        ;
        $content = $this->getDoctrine()
            ->getRepository(Content::class)
            ->findOneBy(["name" => "login_intro"])
        ;

        return $this->render("login/login.html.twig", [
            "contentHeadline" => $contentHeadline,
            "content" => $content,
            "last_username" => $lastUsername,
            "error" => $error
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     * @throws \Exception
     */
    public function logout()
    {
        throw new \Exception("This method can be blank - it will be intercepted by the logout key on your firewall");
    }

    /**
     * @Route("/redirect-after-logout", name="app_logout_redirect")
     * @throws \Exception
     */
    public function app_logout_redirect()
    {
        return $this->redirect("https://www.safe2trade.dk");
    }

    /**
     * @Route("/iframe/wp-login", name="iframe_wp_login")
     * @throws \Exception
     */
    public function iframe_wp_login()
    {
        return $this->render("login/iframe_login.twig");
    }

    /**
     * @Route("/login/forgot-password", name="login_forgot_password")
     * @param Request $request
     * @param SwissArmyKnife $sak
     * @return Response
     */
    public function loginForgotPassword(Request $request, SwissArmyKnife $sak)
    {
        $form = $this->createForm(UserResetPassword::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $formData = $form->getData();

            /** @var User $user */
            $user = $this->em()->getRepository("App:User")
                ->findOneBy(["email" => $formData->getEmail()])
            ;

            if ($user && $user->getEmail()) {
                $mailData = [];
                $mailData["content"] = $this->renderView("backend/mail/reset_password.html.twig", [
                    "host" => $this->host->getDomain(),
                    "user" => $user
                ]);
                $mailData["firstname"] = $user->getFirstname();
                $this->mailer->sendMail($user, false, $this->swissArmyKnife->t("Nulstilling af adgangskode"), $mailData);

                $this->addFlash("info", "<a href='".$this->generateUrl('login_forgot_password_resend', ["uuid" => $user->getUuid()])."'>".$sak->t("Gensend mail? Klik her", "mail")."</a>");
                $this->addFlash("success", "Mail for nulstilling af adgangskode er afsendt.");
                return $this->redirectToRoute("base_backend_admin");
            }
            else {
                $this->addFlash("error", "Kunne ikke finde e-mail ".$formData->getEmail().". Prøv venligst igen.");
            }
        }

        $content = $this->getDoctrine()
            ->getRepository(Content::class)
            ->findOneBy(["name" => "forgot_password"])
        ;

        return $this->render("login/forgot_password.html.twig", [
            "content" => $content,
            "request" => $request,
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/login/forgot-password/resend/{uuid}", name="login_forgot_password_resend")
     * @param User $user
     * @param SwissArmyKnife $sak
     * @return Response
     */
    public function loginForgotPasswordResend(User $user, SwissArmyKnife $sak)
    {
        if ($user && $user->getEmail()) {
            $mailData = [];
            $mailData["content"] = $this->renderView("backend/mail/reset_password.html.twig", [
                "host" => $this->host->getDomain(),
                "user" => $user
            ]);
            $mailData["firstname"] = $user->getFirstname();
            $this->mailer->sendMail($user, false, $this->swissArmyKnife->t("Nulstilling af adgangskode"), $mailData);

            $this->addFlash("info", "<a href='".$this->generateUrl('login_forgot_password_resend', ["uuid" => $user->getUuid()])."'>".$sak->t("Gensend mail? Klik her", "mail")."</a>");
            $this->addFlash("success", "Mail for nulstilling af adgangskode er afsendt.");
        }
        else {
            $this->addFlash("error", "Noget gik galt. Prøv venligst igen.");
        }

        return $this->redirectToRoute("app_login");
    }

    /**
     * @Route("/login/new-password/{secret}", name="login_new_password", requirements={"secret"=".+"})
     * @param User $user
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function loginNewPassword(User $user, Request $request)
    {
        $form = $this->createForm(UserNewPassword::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $formData = $form->getData();
            $password = $formData->getPassword();

            //validate password
            if (strlen($password) < 8) {
                $this->addFlash("error", "Adgangskode skal minimum være 8 karakterer lang. Prøv venligst igen.");
            }
            else {
                //encode password
                $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
                $user->setSecret($this->swissArmyKnife->makeSecret($password));

                $this->em()->persist($user);
                $this->em()->flush();

                if ($user->getEmail()) {
                    $this->addFlash("success", "Din nye adgangskode er nu gemt.");
                }

                return $this->redirectToRoute("base_backend_admin");
            }
        }

        return $this->render("login/new_password.html.twig", [
            "request" => $request,
            "form" => $form->createView()
        ]);
    }

}
