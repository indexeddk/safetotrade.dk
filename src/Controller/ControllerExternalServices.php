<?php

namespace App\Controller;

use ActiveCampaign;
use http\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ControllerExternalServices extends AbstractController
{

    /**
     * ActiveCampaign SYNC
     *
     * @param $user
     * @return bool
     */
    public function activeCampaignUnsubscribe($user)
    {
        return $this->activeCampaignSync($user, true);
    }
    /**
     * ActiveCampaign SYNC
     *
     * @param $user
     * @param bool $unsubscribe
     * @return bool
     */
    public function activeCampaignSync($user, $unsubscribe = false)
    {
        $url = "https://indexed55980.api-us1.com";
        $apiKey = "a54ec9ba6775e7ab716e2b7f0e807e11db2c6e9a7a695d1e70ec8cdd784aa41831d4f0f1";

        $ac = new ActiveCampaign($url, $apiKey);
        $post_data = [
            'first_name' => $user->getFirstname(),
            'last_name' => $user->getLastname(),
            'contact_email' => $user->getEmail(),
            'email' => $user->getEmail(),
            'phone' => $user->getPhone(),
            'tags' => 'api',

            // Custom Fields
            //'field[%IPADRESSE%]' => $_SERVER["REMOTE_ADDR"],

            // Submission ID - is unique to each submission
            //'field[%SUBMISSION_ID%]' => $data['submission_id-505'],

            // Add to automation list
            //"automation" => "1",
        ];

        // Remove from active list
        if ($unsubscribe) {
            $post_data["p[1]"]      = 1;    // 1 = Master list
            $post_data["status[1]"] = 2;    // 2 = Remove
        }
        else {
            $post_data["p[1]"]      = 1;    // 1 = Master list
            $post_data["status[1]"] = 1;    // 2 = Active
        }

        $reponse = $ac->api("contact/sync", $post_data);

        return $reponse;
    }

    /**
     * DAWA
     * - Autocomplete city by zip
     *
     * @Route("/dawa/get-city-by-zipcode", name="external_services_dawa_get_city_by_zip")
     * @param Request $request
     * @return JsonResponse
     */

    public function searchGetCityByZipCode(Request $request) {

        $zip = $request->get("zip");
        $city = false;

        if(!is_numeric($zip)) {
            return $this->json(["status" => "failed", "msg" => "zip code is nan"]);
        }

        try {
            $dawaUrl = "https://dawa.aws.dk/postnumre/".$zip;
            $content = file_get_contents($dawaUrl);
            $city = json_decode($content, true);

        }
        catch (Exception $exception) {
            return $this->json(["status" => "failed", "msg" => "cannot connect to dawa.aws.dk"]);
        }

        if(isset($city["navn"])) {
            return $this->json(["status" => "success", "city" => $city["navn"]]);
        }

        return $this->json(["status" => "failed", "msg" => "no city found"]);
    }

    /**
     * SURE SMS
     *
     * @param $phone
     * @param $message
     * @param string $from
     * @return bool
     */
    public function suresms_send($phone, $message, $from = "Safe2Trade")
    {
        if (!is_numeric($phone) || !trim($message)) {
            return false;
        }

        $phone = trim($phone);
        if (strlen($phone) != 8) {
            return false;
        }

        //$user = "Indexed";
        //$pass = "Indexed123";
        $user = "apikey";
        $pass = "6923C86B-78F1-4FA5";

        $curlUrl = "https://api.suresms.com/Script/SendSMS.aspx?login=".$user."&password=".urlencode($pass)."&to=+45".$phone."&from=".$from."&text=".urlencode($message);

        ob_start();
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $curlUrl);
        $result = curl_exec($curl);

        $hs = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $hs);
        $body = substr($result, $hs);

        curl_close($curl);
        ob_end_clean();

        //dump($result);
        //dump($header);
        //dump($body);
        //exit;

        if (0 == strcmp(substr($header, 0, 13), "Message sent.") || 0 == strcmp(substr($header, 0, 39), "The same message to the same recipient")) {
            //The sms was sent
            return true;
        }
        else {
            //The sms was NOT sent
            return false;
        }
    }

    /**
     * CP SMS
     *
     * @param $phone
     * @param $message
     * @param string $from
     * @return bool
     */
    public function cpsms_send($phone, $message, $from = "SafeToTrade")
    {
        if (!is_numeric($phone) || !trim($message)) {
            return false;
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.cpsms.dk/v2/send",
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => '{"to":"45'.$phone.'", "message": "'.$message.'", "from": "'.$from.'", "timestamp": '.strtotime("NOW").'}',
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic ".base64_encode('[USER]:[PASS/APIKEY]')
            ),
        ));

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($httpCode == 200) {
            return true;
        }
        else {
            return false;
        }
    }

}
