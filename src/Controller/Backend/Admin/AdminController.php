<?php

namespace App\Controller\Backend\Admin;

use App\Controller\ControllerBaseTrait;
use App\Entity\User;
use App\Form\Type\AdminType;
use App\Form\UserNewPassword;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @Route("/admin/admin")
 */
class AdminController extends AbstractController
{
    use ControllerBaseTrait;
    private $host;

    /**
     * AdminController constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->host = $this->hostHelper->getHost();
        $this->swissArmyKnife = $swissArmyKnife;
    }

    /**
     * @Route("/", name="base_backend_admin")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function view(Request $request, PaginatorInterface $paginator)
    {
        if ($request->get("q")) {
            $queryBuilder = $this->getDoctrine()->getRepository("App:User")->getBySearch($request->get("q"));
        }
        else {
            $queryBuilder = $this->getDoctrine()->getRepository("App:User")->getAllAdminsByHost();
        }

        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));

        return $this->render("backend/admin/index.html.twig", [
            "entities" => $entities,
            "q" => $request->get("q")
        ]);
    }

    /**
     * @Route("/profile", name="base_backend_admin_profile")
     * @param Request $request
     * @return Response
     */
    public function profile(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $defaultData = ['message' => 'Type your message here'];
        $formProfileImage = $this->createFormBuilder($defaultData)
            ->add("image", FileType::class, [
                "mapped" => false
            ])
            ->getForm();

        $formProfileImage->handleRequest($request);
        if ($formProfileImage->isSubmitted() && $formProfileImage->isValid()) {

            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $formProfileImage->get("image")->getData();

            $newFilename = false;
            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "backend_profile_directory");
            }

            $user->setProfileImagePath("/uploads/backend/profiles/".$newFilename);

            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("base_backend_admin_profile");
        }

        $form = $this->createForm(AdminType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("base_backend_admin_profile");
        }

        $formPassword = $this->createForm(UserNewPassword::class, $user);
        $formPassword->handleRequest($request);
        if ($formPassword->isSubmitted() && $formPassword->isValid()) {
            $password = $formPassword->getData()->getPassword();
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setSecret($this->swissArmyKnife->makeSecret($password));
            $this->addFlash("success", "Adgangskoden er nu ændret");
            $this->em()->flush();
            return $this->redirectToRoute("base_backend_admin_profile");
        }

        return $this->render("backend/admin/profile.html.twig", [
            "form" => $form->createView(),
            "formProfileImage" => $formProfileImage->createView(),
            "formPassword" => $formPassword->createView(),
            "user" => $user,
            "server" => $_SERVER
        ]);
    }

    /**
     * @Route("/create", name="base_backend_admin_create")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $entity = new User();
        $entity->setHost($this->host);
        $entity->setUuid(Uuid::v4());
        $entity->setRoles(["ROLE_ADMIN"]);

        $form = $this->createForm(AdminType::class, $entity);
        $form->add("submit", SubmitType::class, ["label" => "Gem"]);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {

            /** @var User $entity */
            $entity = $form->getData();

            //encode password
            $password = substr(str_shuffle("ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz123456789!&#*"), 0, 8);
            //$password = "test";
            $entity->setPassword($this->passwordEncoder->encodePassword($entity, $password));
            $entity->setSecret($this->swissArmyKnife->makeSecret($password));
            $entity->setUsername($entity->getEmail());

            //profile image
            $profileImagePath = $this->swissArmyKnife->getProfileImagePath($entity);
            $entity->setProfileImagePath($profileImagePath);

            $this->em()->persist($entity);
            $this->em()->flush();

            //$clonedEntity = clone $entity;
            //$clonedEntity->setPassword($password);

            $mailData = [];
            $mailData["content"] = $this->renderView("backend/mail/welcome.html.twig", [
                "host"=> $this->host->getDomain(),
                "user" => $entity,
                "password" => $password
            ]);
            $mailData["firstname"] = $entity->getFirstname();
            $this->mailer->sendMail($entity, false, $this->swissArmyKnife->t("Velkommen", "mail"), $mailData);

            $this->addFlash("success", "Admin oprettet! Der er udsendt velkomstmail med autogenereret adgangskode.");

            return $this->redirectToRoute("base_backend_admin");
        }

        return $this->render("backend/admin/create.html.twig", array(
            "form" => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{slug}", name="base_backend_admin_edit")
     * @param User $entity
     * @param Request $request
     * @return Response
     */
    public function edit(User $entity, Request $request)
    {
        $form = $this->createForm(AdminType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("base_backend_admin");
        }

        return $this->render("backend/admin/edit.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/change-password/{slug}", name="base_backend_admin_change_password")
     * @param User $entity
     * @param Request $request
     * @return Response
     */
    public function changePassword(User $entity, Request $request)
    {

        $form = $this->createForm(UserNewPassword::class, $entity);
        $form->add("submit", SubmitType::class, ["label" => "Gem"]);
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $formData = $form->getData();

            //encode password
            $password = $formData->getPassword();

            $entity->setPassword($this->passwordEncoder->encodePassword($entity, $password));
            $entity->setSecret($this->swissArmyKnife->makeSecret($password));
            $this->em()->flush();

            $this->addFlash("success", "Gemt!");

            return $this->redirectToRoute("base_backend_admin");
        }

        return $this->render("backend/admin/change_password.html.twig", [
            "form" => $form->createView(),
            "entity" => $entity
        ]);
    }

    /**
     * @Route("/send-mail/reset-password/{slug}", name="base_backend_admin_send_mail_reset_password")
     * @param User $entity
     * @return Response
     */
    public function sendMailResetPassword(User $entity)
    {
        $mailData = [];
        $mailData["content"] = $this->renderView("backend/mail/reset_password.html.twig", ["host"=> $this->host->getDomain(), "user" => $entity]);
        $mailData["firstname"] = $entity->getFirstname();
        $this->mailer->sendMail($entity, false, $this->swissArmyKnife->t("Nulstilling af adgangskode", "mail"), $mailData);

        $this->addFlash("success", "Mail for nulstilling af adgangskode er afsendt.");

        return $this->redirectToRoute("base_backend_admin");
    }

    /**
     * AJAX
     * @Route("/delete/multiple", name="base_backend_admin_delete_multiple")
     * @param Request $request
     * @return Response
     */
    public function deleteMultiple(Request $request)
    {

        // @todo: ekstra sikring her? IF HOST?

        $ids = $request->get("ids");
        $entities = $this->em()->getRepository("App:User")->findBy(["id" => $ids]);

        if ($entities) {
            foreach ($entities as $entity) {
                $this->em()->remove($entity);
                $this->em()->flush();
            }
        }

        return $this->json(["reload" => false]);
    }

    /**
     * @Route("/delete/{slug}", name="base_backend_admin_delete")
     * @param User $entity
     * @return Response
     */
    public function delete(User $entity)
    {
        $this->em()->remove($entity);
        $this->em()->flush();

        return $this->redirectToRoute("base_backend_admin");
    }


}
