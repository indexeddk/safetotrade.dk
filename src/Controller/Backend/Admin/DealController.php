<?php

namespace App\Controller\Backend\Admin;

use App\Controller\ControllerBaseTrait;
use App\Controller\ControllerExternalServices;
use App\Entity\Deal;
use App\Entity\User;
use App\Form\DealType;
use App\Repository\DealRepository;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @Route("/admin/deal")
 *
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property ControllerExternalServices externalServices
 */
class DealController extends AbstractController
{
    use ControllerBaseTrait;
    private $host;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     *
     * @param ControllerExternalServices $externalServices
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife, ControllerExternalServices $externalServices)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
        $this->externalServices = $externalServices;
    }

    /**
     * @Route("/", name="backend_admin_deal_index", methods={"GET"})
     * @param Request $request
     * @param DealRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, DealRepository $repository, PaginatorInterface $paginator): Response
    {
        $queryBuilder = $repository->getEntityQuery($request->get("q"));
        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));
        return $this->render("backend/admin/deal/index.html.twig", [
            "entities" => $entities,
            "title" => "Handler"
        ]);
    }

    /**
     * @Route("/new", name="backend_admin_deal_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        /** @var User $thisUser */
        //$thisUser = $this->getUser();

        $entity = new Deal();
        $entity->setHost($this->host);
        $entity->setUuid(Uuid::v4());
        $entity->setOrigin($entity::ORIGIN_BACKEND);
        $entity->setStatus($entity::STATUS_NEW);

        $options = ["conditions" => $this->swissArmyKnife->getDealConditions(true)];
        $form = $this->createForm(DealType::class, $entity, $options);
        $form->add("Gem", SubmitType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get("image")
                ->getData()
            ;

            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "deal_image_directory");
                $entity->setImage($newFilename);
            }

            $this->em()->persist($entity);
            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("backend_admin_deal_index");
        }

        return $this->render("backend/base_form.html.twig", [
            "entity" => $entity,
            "form" => $form->createView(),
            "title" => "Opret ny handel",
            "backRoute" => "backend_admin_deal_index"
        ]);
    }

    /**
     * @Route("/edit/{id}", name="backend_admin_deal_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Deal $entity
     * @return Response
     */
    public function edit(Request $request, Deal $entity): Response
    {
        $options = ["conditions" => $this->swissArmyKnife->getDealConditions(true)];
        $form = $this->createForm(DealType::class, $entity, $options);

        $form->add("Gem", SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get("image")
                ->getData()
            ;

            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "deal_image_directory");
                $entity->setImage($newFilename);
            }
            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("backend_admin_deal_index");
        }

        return $this->render("backend/base_form.html.twig", [
            "entity" => $entity,
            "form" => $form->createView(),
            "title" => "Rediger handel",
            "imageDir" => "/uploads/deal/",
            "imageName" => $entity->getImage() ?? null,
            "backRoute" => "backend_admin_deal_new"
        ]);
    }

    /**
     * @Route("/delete/{id}", name="backend_admin_deal_delete", methods={"DELETE"})
     * @param Request $request
     * @param Deal $entity
     * @return Response
     */
    public function delete(Request $request, Deal $entity): Response
    {
        if ($this->isCsrfTokenValid("delete".$entity->getId(), $request->request->get("_token"))) {
            $this->em()->remove($entity);
            $this->em()->flush();
            $this->addFlash("success", "Slettet!");
        }

        return $this->redirectToRoute("backend_admin_deal_index");
    }

    /**
     * AJAX
     * @Route("/delete/multiple", name="base_backend_admin_deal_delete_multiple")
     *
     * @param Request $request
     * @return Response
     */
    public function deleteMultiple(Request $request)
    {
        // @todo: ekstra sikring her? IF HOST?

        $ids = $request->get("ids");
        $entities = $this->em()->getRepository("App:Deal")
            ->findBy(["id" => $ids])
        ;

        if ($entities) {
            foreach ($entities as $entity) {
                $this->em()->remove($entity);
                $this->em()->flush();
            }
        }

        return $this->json(["reload" => false]);
    }

}
