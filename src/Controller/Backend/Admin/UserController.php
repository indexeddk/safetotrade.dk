<?php

namespace App\Controller\Backend\Admin;

use App\Controller\ControllerBaseTrait;
use App\Controller\ControllerExternalServices;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @Route("/admin/user")
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property ControllerExternalServices externalServices
 */
class UserController extends AbstractController
{
    use ControllerBaseTrait;

    private $host;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     *
     * @param ControllerExternalServices $externalServices
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife, ControllerExternalServices $externalServices)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
        $this->externalServices = $externalServices;
    }

    /**
     * @Route("/", name="backend_admin_user_index", methods={"GET"})
     * @param Request $request
     * @param UserRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, UserRepository $repository, PaginatorInterface $paginator): Response
    {
        $queryBuilder = $repository->getUserEntityQuery($request->get("q"));
        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));

        return $this->render('backend/admin/user/index.html.twig', [
            "entities" => $entities,
            "title" => "Brugere"
        ]);
    }

    /**
     * @Route("/new", name="backend_admin_user_new", methods={"GET","POST"})
     * @param Request $request
     * @param SwissArmyKnife $swissArmyKnife
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @return Response
     */
    public function new(Request $request, SwissArmyKnife $swissArmyKnife, UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer): Response
    {

        $entity = new User();
        $entity
            ->setHost($this->getUser()->getHost())
            ->setCompany($this->getUser()->getCompany())
            ->setUuid(Uuid::v4())
            ->setRoles(["ROLE_CUSTOMER"])
        ;

        $form = $this->createForm(UserType::class, $entity);
        $form->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success"]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * password
             */
            $password = $swissArmyKnife->generatePassword();
            $entity->setPassword($passwordEncoder->encodePassword($entity, $password));
            $entity->setSecret($swissArmyKnife->makeSecret($password));

            if (!$entity->getUsername()) {
                $entity->setUsername($entity->getEmail());
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            //$mailData = [];
            //$mailData["content"] = $this->renderView("backend/mail/welcome.html.twig", [
            //    "host"=> $this->getUser()->getHost()->getDomain(),
            //    "user" => $entity,
            //    "password" => $password
            //]);
            //$mailData["firstname"] = $entity->getFirstname();
            //$mailer->sendMail($entity, false, $swissArmyKnife->t("Velkommen", "mail"), $mailData);
            //
            //$this->addFlash("success", "Bruger oprettet! Der er udsendt velkomstmail med autogenereret adgangskode.");

            $this->externalServices->activeCampaignSync($entity);

            return $this->redirectToRoute('backend_admin_user_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Opret ny bruger',
            "backRoute" => "backend_admin_user_index"
        ]);
    }

    /**
     * @Route("/edit/{id}", name="backend_admin_user_edit", methods={"GET","POST"})
     * @param Request $request
     * @param User $entity
     * @return Response
     */
    public function edit(Request $request, User $entity): Response
    {
        $form = $this->createForm(UserType::class, $entity);
        $form->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success"]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->externalServices->activeCampaignSync($entity);

            return $this->redirectToRoute('backend_admin_user_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Rediger bruger',
            "backRoute" => "backend_admin_user_index"
        ]);
    }

    /**
     * @Route("/delete/{id}", name="backend_admin_user_delete", methods={"DELETE"})
     * @param Request $request
     * @param User $entity
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Request $request, User $entity): Response
    {
        if ($this->isCsrfTokenValid('delete' . $entity->getId(), $request->request->get('_token'))) {
            //$entityManager = $this->getDoctrine()->getManager();
            //$entityManager->remove($entity);
            //$entityManager->flush();
            $this->swissArmyKnife->deleteUser($entity);
            $this->addFlash("success", "Slettet!");
        }

        return $this->redirectToRoute('backend_admin_user_index');
    }

    /**
     * AJAX
     *
     * @Route("/delete/multiple", name="base_backend_admin_user_delete_multiple")
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteMultiple(Request $request)
    {

        // @todo: ekstra sikring her? IF HOST?

        $ids = $request->get("ids");
        $entities = $this->em()->getRepository("App:User")->findBy(["id" => $ids]);

        if ($entities) {
            foreach ($entities as $entity) {
                //$this->em()->remove($entity);
                //$this->em()->flush();

                $this->swissArmyKnife->deleteUser($entity);
                $this->addFlash("success", "Slettet!");
            }
        }

        return $this->json(["reload" => false]);
    }

}
