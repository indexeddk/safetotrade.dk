<?php

namespace App\Controller\Backend\Admin;

use App\Controller\ControllerBaseTrait;
use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/kategori")
 */
class CategoryController extends AbstractController
{
    use ControllerBaseTrait;

    /**
     * @Route("/", name="backend_admin_category_index", methods={"GET"})
     * @param Request $request
     * @param CategoryRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, CategoryRepository $repository, PaginatorInterface $paginator): Response
    {
        $queryBuilder = $repository->createQueryBuilder('category')
            ->where('category.company = :company')
            ->setParameter('company', $this->getUser()->getCompany());

        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));

        return $this->render('backend/admin/category/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Route("/new", name="backend_admin_category_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $entity = new Category();
        $entity->setCompany($this->getUser()->getCompany());
        $form = $this->createForm(CategoryType::class, $entity);
        $form->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success"]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('backend_admin_category_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Opret nyt kategori',
        ]);
    }

    /**
     * @Route("/{id}", name="backend_admin_category_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Category $entity
     * @return Response
     */
    public function edit(Request $request, Category $entity): Response
    {
        $form = $this->createForm(CategoryType::class, $entity);

        $form->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success"]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('backend_admin_category_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Rediger category',
        ]);
    }

    /**
     * @Route("/{id}", name="backend_admin_category_delete", methods={"DELETE"})
     * @param Request $request
     * @param Category $entity
     * @return Response
     */
    public function delete(Request $request, Category $entity): Response
    {
        if ($this->isCsrfTokenValid('delete' . $entity->getId(), $request->request->get('_token'))) {

            if ($entity->getFormGroups()->count()) {
                $this->addFlash('error', 'Kan ikke slette med spørgsmål på');

            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($entity);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('backend_admin_category_index');
    }
}
