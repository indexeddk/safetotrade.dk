<?php

namespace App\Controller\Backend\Admin;

use App\Entity\Team;
use App\Form\TeamType;
use App\Repository\TeamRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/team")
 */
class TeamController extends AbstractController
{
    /**
     * @Route("/", name="backend_admin_team_index", methods={"GET"})
     * @param Request $request
     * @param TeamRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, TeamRepository $repository, PaginatorInterface $paginator): Response
    {
        $queryBuilder = $repository->createQueryBuilder('team')
            ->where('team.company = :company')
            ->setParameter('company', $this->getUser()->getCompany());

        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));

        return $this->render('backend/admin/team/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Route("/new", name="backend_admin_team_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $entity = new Team();
        $entity->setCompany( $this->getUser()->getCompany());
        $form = $this->createForm(TeamType::class, $entity);

        $form->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success"]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('backend_admin_team_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Opret nyt team',
        ]);
    }

    /**
     * @Route("/{id}", name="backend_admin_team_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Team $entity
     * @return Response
     */
    public function edit(Request $request, Team $entity): Response
    {
        $form = $this->createForm(TeamType::class, $entity);

        $form->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success"]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('backend_admin_team_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Rediger team',
        ]);
    }

    /**
     * @Route("/{id}", name="backend_admin_team_delete", methods={"DELETE"})
     * @param Request $request
     * @param Team $entity
     * @return Response
     */
    public function delete(Request $request, Team $entity): Response
    {
        if ($this->isCsrfTokenValid('delete'.$entity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();
        }

        return $this->redirectToRoute('backend_admin_team_index');
    }
}
