<?php

namespace App\Controller\Backend\Admin;

use App\Controller\ControllerBaseTrait;
use App\Controller\ControllerExternalServices;
use App\Entity\Blog;
use App\Entity\User;
use App\Form\BlogType;
use App\Repository\BlogRepository;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/admin/blog")
 *
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property ControllerExternalServices externalServices
 */
class BlogController extends AbstractController
{
    use ControllerBaseTrait;

    private $host;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     *
     * @param ControllerExternalServices $externalServices
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife, ControllerExternalServices $externalServices)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
        $this->externalServices = $externalServices;
    }

    /**
     * @Route("/", name="backend_admin_blog_index", methods={"GET"})
     * @param Request $request
     * @param BlogRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, BlogRepository $repository, PaginatorInterface $paginator): Response
    {
        $queryBuilder = $repository->getEntityQuery(false);
        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));
        return $this->render("backend/admin/blog/index.html.twig", [
            "entities" => $entities,
        ]);
    }

    /**
     * @Route("/new", name="backend_admin_blog_new", methods={"GET","POST"})
     * @param Request $request
     * @param SluggerInterface $slugger
     * @return Response
     */
    public function new(Request $request, SluggerInterface $slugger): Response
    {

        //redirect back to list
        return $this->redirectToRoute("backend_admin_blog_index");


        ///** @var User $thisUser */
        //$thisUser = $this->getUser();
        //
        //$entity = new Blog();
        //$entity->setHost($this->host);
        //$entity->setCreatedBy($thisUser);
        //
        //$form = $this->createForm(BlogType::class, $entity);
        //$form->add("Gem", SubmitType::class);
        //
        //$form->handleRequest($request);
        //if ($form->isSubmitted() && $form->isValid()) {
        //    //image
        //    /** @var UploadedFile $imageFile */
        //    $imageFile = $form->get("image")
        //        ->getData()
        //    ;
        //    if ($imageFile) {
        //        $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "blog_image_directory");
        //        $entity->setImage($newFilename);
        //    }
        //
        //    $this->em()
        //        ->persist($entity)
        //    ;
        //    $this->em()
        //        ->flush()
        //    ;
        //    $this->addFlash("success", "Gemt!");
        //
        //    return $this->redirectToRoute("backend_admin_blog_index");
        //}

        //return $this->render("backend/base_form.html.twig", [
        //    "entity" => $entity,
        //    "form" => $form->createView(),
        //    "title" => "Opret nyt blogindlæg",
        //    "backRoute" => "backend_admin_blog_index"
        //]);
    }

    /**
     * @Route("/edit/{id}", name="backend_admin_blog_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Blog $entity
     * @return Response
     */
    public function edit(Request $request, Blog $entity): Response
    {
        $form = $this->createForm(BlogType::class, $entity);

        $form->add("Gem", SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get("image")
                ->getData()
            ;
            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "blog_image_directory");
                $entity->setImage($newFilename);
            }

            $this->addFlash("success", "Gemt!");
            $this->em()
                ->flush()
            ;

            return $this->redirectToRoute("backend_admin_blog_index");
        }

        return $this->render("backend/base_form.html.twig", [
            "entity" => $entity,
            "form" => $form->createView(),
            "title" => "Rediger blog",
            "imageDir" => "/uploads/blog/",
            "imageName" => $entity->getImage() ?? null
        ]);
    }

    /**
     * @Route("/delete/{id}", name="backend_admin_blog_delete", methods={"DELETE"})
     * @param Request $request
     * @param Blog $entity
     * @return Response
     */
    public function delete(Request $request, Blog $entity): Response
    {
        if ($this->isCsrfTokenValid("delete".$entity->getId(), $request->request->get("_token"))) {
            $this->em()
                ->remove($entity)
            ;
            $this->em()
                ->flush()
            ;
            $this->addFlash("success", "Slettet!");
        }

        return $this->redirectToRoute("backend_admin_blog_index");
    }
}
