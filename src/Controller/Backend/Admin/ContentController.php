<?php

namespace App\Controller\Backend\Admin;

use App\Controller\ControllerBaseTrait;
use App\Entity\Content;
use App\Form\ContentType;
use App\Repository\ContentRepository;
use http\Client\Response;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/content")
 */

class ContentController extends AbstractController
{
    use ControllerBaseTrait;

    /**
     * @Route("/", name="backend_admin_content_index", methods={"GET"})
     * @param Request $request
     * @param ContentRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, ContentRepository $repository, PaginatorInterface $paginator)
    {
        $queryBuilder = $repository->createQueryBuilder('content')
            ->where('content.company = :company')
            ->setParameter('company', $this->getUser()->getCompany())
            ->orderBy('content.humanName', 'ASC')
        ;

        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1), 20);

        return $this->render('backend/admin/content/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Route("/new", name="backend_admin_content_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response|\Symfony\Component\HttpFoundation\Response
     */
    public function new(Request $request)
    {
        $entity = new Content();
        $entity
            //->setHost($this->getUser()->getHost())
            ->setCompany($this->getUser()->getCompany());

        $form = $this->createForm(ContentType::class, $entity);
        $form->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success"]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->addFlash("success", "Tekst oprettet!");

            return $this->redirectToRoute('backend_admin_content_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Opret ny tekst',
        ]);
    }

    /**
     * @Route("/{id}", name="backend_admin_content_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Content $entity
     * @return Response|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, Content $entity)
    {
        $form = $this->createForm(ContentType::class, $entity);

        $form->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success"]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('backend_admin_content_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Rediger tekst',
        ]);
    }

    /**
     * @Route("/{id}", name="backend_admin_content_delete", methods={"DELETE"})
     * @param Request $request
     * @param Content $entity
     * @return Response|RedirectResponse
     */
    public function delete(Request $request, Content $entity)
    {
        if ($this->isCsrfTokenValid('delete' . $entity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();
        }

        return $this->redirectToRoute('backend_admin_content_index');
    }
}
