<?php

namespace App\Controller\Backend\Admin;

use App\Controller\ControllerBaseTrait;
use App\Controller\ControllerExternalServices;
use App\Entity\Review;
use App\Entity\User;
use App\Form\ReviewType;
use App\Repository\ReviewRepository;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @Route("/admin/review")
 *
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property ControllerExternalServices externalServices
 */
class ReviewController extends AbstractController
{
    use ControllerBaseTrait;
    private $host;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     *
     * @param ControllerExternalServices $externalServices
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife, ControllerExternalServices $externalServices)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
        $this->externalServices = $externalServices;
    }

    /**
     * @Route("/", name="backend_admin_review_index", methods={"GET"})
     * @param Request $request
     * @param ReviewRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, ReviewRepository $repository, PaginatorInterface $paginator): Response
    {
        $queryBuilder = $repository->getEntityQuery($request->get("q"));
        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));
        return $this->render("backend/admin/review/index.html.twig", [
            "entities" => $entities,
            "title" => "Anmeldelser"
        ]);
    }

    /**
     * @Route("/new", name="backend_admin_review_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        /** @var User $thisUser */
        //$thisUser = $this->getUser();

        $entity = new Review();
        $entity->setHost($this->host);
        $entity->setUuid(Uuid::v4());

        $form = $this->createForm(ReviewType::class, $entity);
        $form->add("Gem", SubmitType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get("image")->getData();

            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "review_image_directory");
                $entity->setImage($newFilename);
            }

            $this->em()->persist($entity);
            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("backend_admin_review_index");
        }

        return $this->render("backend/base_form.html.twig", [
            "entity" => $entity,
            "form" => $form->createView(),
            "title" => "Opret ny anmeldelse",
            "backRoute" => "backend_admin_review_index"
        ]);
    }

    /**
     * @Route("/edit/{id}", name="backend_admin_review_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Review $entity
     * @return Response
     */
    public function edit(Request $request, Review $entity): Response
    {
        $form = $this->createForm(ReviewType::class, $entity);

        $form->add("Gem", SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get("image")->getData();

            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "review_image_directory");
                $entity->setImage($newFilename);
            }

            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("backend_admin_review_index");
        }

        return $this->render("backend/base_form.html.twig", [
            "entity" => $entity,
            "form" => $form->createView(),
            "title" => "Rediger anmeldelse",
            "imageDir" => "/uploads/review/",
            "imageName" => $entity->getImage() ?? null,
            "backRoute" => "backend_admin_review_index"
        ]);
    }

    /**
     * @Route("/delete/{id}", name="backend_admin_review_delete", methods={"DELETE"})
     * @param Request $request
     * @param Review $entity
     * @return Response
     */
    public function delete(Request $request, Review $entity): Response
    {
        if ($this->isCsrfTokenValid("delete".$entity->getId(), $request->request->get("_token"))) {
            $this->em()->remove($entity);
            $this->em()->flush();
            $this->addFlash("success", "Slettet!");
        }

        return $this->redirectToRoute("backend_admin_review_index");
    }

    /**
     * AJAX
     * @Route("/delete/multiple", name="base_backend_admin_review_delete_multiple")
     *
     * @param Request $request
     * @return Response
     */
    public function deleteMultiple(Request $request)
    {
        // @todo: ekstra sikring her? IF HOST?

        $ids = $request->get("ids");
        $entities = $this->em()->getRepository("App:Review")->findBy(["id" => $ids]);

        if ($entities) {
            foreach ($entities as $entity) {

                $reviewChanges = $this->em()->getRepository("App:ReviewChange")->findBy(["review" => $entity]);
                if ($reviewChanges) {
                    foreach ($reviewChanges as $reviewChange) {
                        $this->em()->remove($reviewChange);
                    }
                    $this->em()->flush();
                }

                $abuses = $this->em()->getRepository("App:Abuse")->findBy(["review" => $entity]);
                if ($abuses) {
                    foreach ($abuses as $abuse) {
                        $this->em()->remove($abuse);
                    }
                    $this->em()->flush();
                }

                $this->em()->remove($entity);
                $this->em()->flush();
            }
        }

        return $this->json(["reload" => false]);
    }

}
