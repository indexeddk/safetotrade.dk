<?php

namespace App\Controller\Backend\Admin;

use App\Controller\ControllerBaseTrait;
use App\Controller\ControllerExternalServices;
use App\Entity\Message;
use App\Entity\User;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @Route("/admin/message")
 *
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property ControllerExternalServices externalServices
 */
class MessageController extends AbstractController
{
    use ControllerBaseTrait;
    private $host;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     *
     * @param ControllerExternalServices $externalServices
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife, ControllerExternalServices $externalServices)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
        $this->externalServices = $externalServices;
    }

    /**
     * @Route("/", name="backend_admin_message_index", methods={"GET"})
     * @param Request $request
     * @param MessageRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, MessageRepository $repository, PaginatorInterface $paginator): Response
    {
        $queryBuilder = $repository->getEntityQuery($request->get("q"));
        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));
        return $this->render("backend/admin/message/index.html.twig", [
            "entities" => $entities,
            "title" => "Beskeder"
        ]);
    }

    ///**
    // * @Route("/delete/{id}", name="backend_admin_message_delete", methods={"DELETE"})
    // * @param Request $request
    // * @param Message $entity
    // * @return Response
    // * @throws ORMException
    // * @throws OptimisticLockException
    // */
    //public function delete(Request $request, Message $entity): Response
    //{
    //    if ($this->isCsrfTokenValid("delete".$entity->getId(), $request->request->get("_token"))) {
    //        $this->em()->remove($entity);
    //        $this->em()->flush();
    //        $this->addFlash("success", "Slettet!");
    //    }
    //
    //    return $this->redirectToRoute("backend_admin_message_index");
    //}

    ///**
    // * AJAX
    // * @Route("/delete/multiple", name="base_backend_admin_message_delete_multiple")
    // *
    // * @param Request $request
    // * @return Response
    // * @throws ORMException
    // * @throws OptimisticLockException
    // */
    //public function deleteMultiple(Request $request)
    //{
    //    // @todo: ekstra sikring her? IF HOST?
    //
    //    $ids = $request->get("ids");
    //    $entities = $this->em()->getRepository("App:Message")
    //        ->findBy(["id" => $ids])
    //    ;
    //
    //    if ($entities) {
    //        foreach ($entities as $entity) {
    //            $this->em()->remove($entity);
    //            $this->em()->flush();
    //        }
    //    }
    //
    //    return $this->json(["reload" => false]);
    //}

}
