<?php
namespace App\Controller\Backend;

use App\Controller\ControllerBaseTrait;
use App\Controller\ControllerExternalServices;
use App\Entity\Deal;
use App\Entity\Review;
use App\Entity\User;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin")
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property ControllerExternalServices externalServices
 */

class BackendPageController extends AbstractController
{
    use ControllerBaseTrait;

    private $host;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     *
     * @param ControllerExternalServices $externalServices
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife, ControllerExternalServices $externalServices)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
        $this->externalServices = $externalServices;
    }

    /**
     * @Route("/", name="base_backend_admin_home")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $deal = new Deal();

        $fromDate = $request->get("fromDate");
        $toDate = $request->get("toDate");
        if($fromDate){
            $fromDate .= " 00:00:00";
        }
        else {
            $fromDate = "2019-01-01";
        }
        if($toDate){
            $toDate .= " 23:59:59";
        }
        else {
            $toDate = date("Y-m-d 23:59:59");
        }


        $deals = $em->getRepository("App:Deal")->createQueryBuilder("e")
            ->where("e.host = :host")
            ->setParameter("host", $this->getUser()->getHost())
            ->andWhere("e.status = :completed")
            ->setParameter("completed", $deal::STATUS_COMPLETED)
            ->andWhere("e.createdAt > :fromDate")
            ->andWhere("e.createdAt < :toDate")
            ->setParameter("fromDate", $fromDate)
            ->setParameter("toDate", $toDate)
            ->getQuery()
            ->getResult()
        ;

        $users = $em->getRepository("App:User")->createQueryBuilder("e")
            ->where("e.host = :host")
            ->setParameter("host", $this->getUser()->getHost())
            ->andWhere("e.rolesHierarchy = 30")
            ->andWhere("e.deleted IS NULL")
            ->andWhere("e.createdAt > :fromDate")
            ->andWhere("e.createdAt < :toDate")
            ->setParameter("fromDate", $fromDate)
            ->setParameter("toDate", $toDate)
            ->getQuery()
            ->getResult()
        ;

        $deletedUsers = $em->getRepository("App:DeletedUser")->createQueryBuilder("e")
            ->where("e.host = :host")
            ->setParameter("host", $this->getUser()->getHost())
            ->andWhere("e.createdAt > :fromDate")
            ->andWhere("e.createdAt < :toDate")
            ->setParameter("fromDate", $fromDate)
            ->setParameter("toDate", $toDate)
            ->getQuery()
            ->getResult()
        ;

        $reviews = $em->getRepository("App:Review")->createQueryBuilder("e")
            ->where("e.host = :host")
            ->setParameter("host", $this->getUser()->getHost())
            ->andWhere("e.createdAt > :fromDate")
            ->andWhere("e.createdAt < :toDate")
            ->setParameter("fromDate", $fromDate)
            ->setParameter("toDate", $toDate)
            ->getQuery()
            ->getResult()
        ;

        //calc total deals
        $dealsByUserId = [];
        $allBuyersAndSellers = [];
        /** @var Deal $deal */
        foreach ($deals as $deal) {
            $dealsByUserId[$deal->getSeller()->getId()][] = $deal->getId();
            $dealsByUserId[$deal->getBuyer()->getId()][] = $deal->getId();
            $allBuyersAndSellers[] = $deal->getSeller()->getId();
            $allBuyersAndSellers[] = $deal->getBuyer()->getId();
        }
        $allBuyersAndSellers = array_unique($allBuyersAndSellers);

        //allocate users by total deals
        $usersByTotalDeals = [
            "0" => [],
            "0-5" => [],
            "5-10" => [],
            "10+" => [],
        ];
        /** @var User $user */
        foreach ($dealsByUserId as $userId => $dealsArray) {
            if (sizeof($dealsArray) > 10) {
                $usersByTotalDeals["10+"][] = $userId;
            }
            elseif (sizeof($dealsArray) > 5) {
                $usersByTotalDeals["5-10"][] = $userId;
            }
            elseif (sizeof($dealsArray) > 0) {
                $usersByTotalDeals["0-5"][] = $userId;
            }
        }

        $usersWithNoDealsBuilder = $em->getRepository("App:User")->createQueryBuilder("e")
            ->where("e.host = :host")
            ->setParameter("host", $this->getUser()->getHost())
            ->andWhere("e.rolesHierarchy = 30")
            ->andWhere("e.deleted IS NULL")
        ;

        if (sizeof($allBuyersAndSellers) > 0) {
            $usersWithNoDealsBuilder->andWhere("e.id NOT IN (".implode(",", $allBuyersAndSellers).")");
        }

        $usersWithNoDeals = $usersWithNoDealsBuilder->getQuery()->getResult();

        $usersByTotalDeals["0"] = sizeof($usersWithNoDeals);

        //calc review, rating and avg. rating pr user
        $ratingsByUserId = [];

        /** @var Review $review */
        foreach ($reviews as $review) {
            if($review->getToUser()) {
                $ratingsByUserId[$review->getToUser()->getId()][] = $review->getRating();
            }
        }
        $avgRatingByUserId = [];
        foreach ($ratingsByUserId as $userId => $ratings) {
            $avgRating = array_sum($ratings) / sizeof($ratings);
            $avgRatingByUserId[$userId] = $avgRating;
        }

        //allocate users by rating
        $usersByRating = [
            //0 => [],
            1 => [],
            2 => [],
            3 => [],
            4 => [],
            5 => [],
        ];
        /** @var User $user */
        foreach ($avgRatingByUserId as $userId => $rating) {
            if ($rating == 5) {
                $usersByRating[5][] = $userId;
            }
            elseif ($rating >= 4) {
                $usersByRating[4][] = $userId;
            }
            elseif ($rating >= 3) {
                $usersByRating[3][] = $userId;
            }
            elseif ($rating >= 2) {
                $usersByRating[2][] = $userId;
            }
            elseif ($rating >= 1) {
                $usersByRating[1][] = $userId;
            }
            else {
                //$usersByRating[0][] = $userId;
            }
        }
        //krsort($usersByRating);

        //users with and without SoMe
        $usersWithSoMe = [];
        $usersWithoutSoMe = [];
        foreach ($users as $user) {
            if(trim($user->getSomeFacebook()) || trim($user->getSomeInstagram()) || trim($user->getSomeLinkedIn()) || trim($user->getSomeTwitter())) {
                $usersWithSoMe[] = $user;
            }
            else {
                $usersWithoutSoMe[] = $user;
            }
        }

        return $this->render("backend/admin/dashboard.html.twig", [
            "deals" => $deals,
            "deletedUsers" => $deletedUsers,
            "users" => $users,
            "usersWithSoMe" => $usersWithSoMe,
            "usersWithoutSoMe" => $usersWithoutSoMe,
            "usersByRating" => $usersByRating,
            "usersByTotalDeals" => $usersByTotalDeals,
            "reviews" => $reviews,
            "fromDate" => $request->get("fromDate"),
            "toDate" => $request->get("toDate"),

        ]);
    }
}
