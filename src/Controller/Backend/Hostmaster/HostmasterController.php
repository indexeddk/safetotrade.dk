<?php

namespace App\Controller\Backend\Hostmaster;

use App\Controller\ControllerBaseTrait;
use App\Entity\Host;
use App\Entity\User;
use App\Form\Type\AdminType;
use App\Form\Type\HostType;
use App\Form\UserNewPassword;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailHelper
 * @property SwissArmyKnife swissArmyKnife
 * @property Host host
 * @Route("/hostmaster")
 */
class HostmasterController extends AbstractController
{
    use ControllerBaseTrait;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailHelper
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailHelper, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailHelper = $mailHelper;
        $this->hostHelper = $hostHelper;
        $this->host = $this->hostHelper->getHost();
        $this->swissArmyKnife = $swissArmyKnife;
    }

    /**
     * @Route("/", name="base_backend_hostmaster_home")
     */
    public function index()
    {
        return $this->render("backend/hostmaster/dashboard.html.twig");
    }

    /**
     * @Route("/hosts", name="base_backend_hosts")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function viewHosts(Request $request, PaginatorInterface $paginator)
    {
        if ($request->get("q")) {
            $queryBuilder = $this->em()->getRepository("App:Host")->getBySearch($request->get("q"));
        }
        else {
            $queryBuilder = $this->em()->getRepository("App:Host")->getAllHosts();
        }
        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));

        return $this->render("backend/host/index.html.twig", [
            "entities" => $entities,
            "q" => $request->get("q")
        ]);
    }

    /**
     * @Route("/profile", name="base_backend_hostmaster_profile")
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function profile(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $defaultData = ['message' => 'Type your message here'];
        $formProfileImage = $this->createFormBuilder($defaultData)
            ->add("image", FileType::class, [
                "mapped" => false
            ])
            ->getForm();

        $formProfileImage->handleRequest($request);
        if ($formProfileImage->isSubmitted() && $formProfileImage->isValid()) {

            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $formProfileImage->get("image")->getData();

            $newFilename = false;
            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "backend_profile_directory");
            }

            $user->setProfileImagePath("/uploads/backend/profiles/".$newFilename);

            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("base_backend_hostmaster_profile");
        }

        $form = $this->createForm(AdminType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("base_backend_hostmaster_profile");
        }

        $formPassword = $this->createForm(UserNewPassword::class, $user);
        $formPassword->handleRequest($request);
        if ($formPassword->isSubmitted() && $formPassword->isValid()) {
            $password = $formPassword->getData()->getPassword();
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setSecret($this->swissArmyKnife->makeSecret($password));
            $this->addFlash("success", "Adgangskoden ændret!");
            $this->em()->flush();
            return $this->redirectToRoute("base_backend_hostmaster_profile");
        }
        //else {
        //SHOW ERROR HERE
        //dump ($formPassword->get);
        //$this->addFlash("error", "Adgangskoden ændret!");
        //}

        return $this->render("backend/admin/profile.html.twig", [
            "formProfileImage" => $formProfileImage->createView(),
            "form" => $form->createView(),
            "formPassword" => $formPassword->createView(),
            "user" => $user,
            "server" => $_SERVER
        ]);
    }

    /**
     * @Route("/hostmaster/admins/{slug}", name="base_backend_hostmaster_admins")
     * @param Host $entity
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function viewHostmasters(Host $entity, Request $request, PaginatorInterface $paginator)
    {
        if ($request->get("q")) {
            $queryBuilder = $this->em()->getRepository("App:User")->getBySearch($request->get("q"));
        }
        else {
            $queryBuilder = $this->em()->getRepository("App:User")->getAllAdminsByHost($entity);
        }
        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));

        return $this->render("backend/admin/index.html.twig", [
            "entities" => $entities,
            "q" => $request->get("q")
        ]);
    }

    /**
     * @Route("/host/create", name="base_backend_hosts_create")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createHost(Request $request)
    {
        $entity = new Host();

        $form = $this->createForm(HostType::class, $entity);
        $form->add("submit", SubmitType::class, ["label" => "Gem"]);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            /** @var User $entity */
            $entity = $form->getData();

            $this->em()->persist($entity);
            $this->em()->flush();

            $this->addFlash("success", "Domæne oprettet!");

            return $this->redirectToRoute("base_backend_hosts");
        }

        return $this->render("backend/host/create.html.twig", array(
            "form" => $form->createView(),
        ));
    }

    /**
     * @Route("/create", name="base_backend_hostmaster_admins_create")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createHostmaster(Request $request)
    {
        $entity = new User();
        $entity->setUuid(Uuid::v4());
        $entity->setHost($this->host);
        $entity->setRoles(["ROLE_ADMIN"]);

        $form = $this->createForm(AdminType::class, $entity);
        $form->add("submit", SubmitType::class, ["label" => "Gem"]);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            /** @var User $entity */
            $entity = $form->getData();

            //encode password
            $password = substr(str_shuffle("ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz123456789!&#*"), 0, 8);
            //$password = "test";
            $entity->setPassword($this->passwordEncoder->encodePassword($entity, $password));
            $entity->setSecret($this->swissArmyKnife->makeSecret($password));
            $entity->setUsername($entity->getEmail());

            //profile image
            $profileImagePath = $this->swissArmyKnife->getProfileImagePath($entity);
            $entity->setProfileImagePath($profileImagePath);

            $this->em()->persist($entity);
            $this->em()->flush();

            //$clonedEntity = clone $entity;
            //$clonedEntity->setPassword($password);

            $mailData = [];
            $mailData["content"] = $this->renderView("backend/mail/welcome.html.twig", [
                "host" => $this->host->getDomain(),
                "user" => $entity,
                "password" => $password
            ]);
            $mailData["firstname"] = $entity->getFirstname();
            $this->mailHelper->sendMail($entity, false, $this->swissArmyKnife->t("Velkommen", "mail"), $mailData);

            $this->addFlash("success", "Admin oprettet! Der er udsendt velkomstmail med autogenereret adgangskode.");

            return $this->redirectToRoute("base_backend_hostmasters");
        }

        return $this->render("backend/admin/create.html.twig", array(
            "form" => $form->createView(),
        ));
    }

    /**
     * @Route("/host/edit/{slug}", name="base_backend_hosts_edit")
     * @param Host $entity
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function editHost(Host $entity, Request $request)
    {
        $form = $this->createForm(HostType::class, $entity);

        $form->add('Gem', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("base_backend_hosts");
        }

        return $this->render("backend/admin/edit.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{slug}", name="base_backend_hostmaster_admins_edit")
     * @param User $entity
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function editHostmaster(User $entity, Request $request)
    {
        $form = $this->createForm(AdminType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em()->flush();
            $this->addFlash("success", "Gemt!");
            return $this->redirectToRoute("base_backend_hostmaster_admins", [
                "slug" => $entity->getHost()
                    ->getSlug()
            ]);
        }

        return $this->render("backend/base_form.html.twig", [
            "form" => $form->createView(),
            "title" => "Rediger administrator",
            //"backRoute" => "base_backend_hostmaster_admins",
            //"entity" => $entity
        ]);
    }

    /**
     * @Route("/change-password/{slug}", name="base_backend_hostmaster_admins_change_password")
     * @param User $entity
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function changePassword(User $entity, Request $request)
    {
        $form = $this->createForm(UserNewPassword::class, $entity);
        $form->add("submit", SubmitType::class, ["label" => "Gem"]);
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $formData = $form->getData();

            //encode password
            $password = $formData->getPassword();

            $entity->setPassword($this->passwordEncoder->encodePassword($entity, $password));
            $entity->setSecret($this->swissArmyKnife->makeSecret($password));
            $this->em()->flush();
            $this->addFlash("success", "Adgangskode gemt!");
            return $this->redirectToRoute("base_backend_hostmaster_admins", [
                "slug" => $entity->getHost()->getSlug()
            ]);
        }

        return $this->render("backend/admin/change_password.html.twig", [
            "form" => $form->createView(),
            "entity" => $entity
        ]);
    }

    /**
     * @Route("/send-mail/reset-password/{slug}", name="base_backend_hostmaster_admins_send_mail_reset_password")
     * @param User $entity
     * @return Response
     */
    public function sendMailResetPassword(User $entity)
    {
        $mailData = [];
        $mailData["content"] = $this->renderView("backend/mail/reset_password.html.twig", [
            "host" => $this->host->getDomain(),
            "user" => $entity
        ]);
        $mailData["firstname"] = $entity->getFirstname();
        $this->mailHelper->sendMail($entity, false, $this->swissArmyKnife->t("Nulstilling af adgangskode", "mail"), $mailData);

        $this->addFlash("success", "Mail for nulstilling af adgangskode er afsendt.");

        return $this->redirectToRoute("base_backend_hostmaster_admins", [
            "slug" => $entity->getHost()->getSlug()
        ]);
    }

    /**
     * AJAX
     * @Route("/hosts/delete/multiple", name="base_backend_hosts_delete_multiple")
     *
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteMultipleHosts(Request $request)
    {
        $ids = $request->get("ids");
        $entities = $this->em()->getRepository("App:Host")->findBy(["id" => $ids]);

        if ($entities) {
            foreach ($entities as $entity) {
                $this->em()->remove($entity);
                $this->em()->flush();
            }
        }

        return $this->json(["reload" => false]);
    }

    /**
     * AJAX
     * @Route("/delete/multiple", name="base_backend_hostmaster_admins_delete_multiple")
     *
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteMultipleHostmasters(Request $request)
    {
        $ids = $request->get("ids");
        $entities = $this->em()->getRepository("App:User")->findBy(["id" => $ids]);

        if ($entities) {
            foreach ($entities as $entity) {
                $this->em()->remove($entity);
                $this->em()->flush();
            }
        }

        return $this->json(["reload" => false]);
    }

    /**
     * @Route("/host/delete/{slug}", name="base_backend_hosts_delete")
     * @param Host $entity
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteHost(Host $entity)
    {
        $this->em()->remove($entity);
        $this->em()->flush();

        return $this->redirectToRoute("base_backend_hosts");
    }

    /**
     * @Route("/delete/{slug}", name="base_backend_hostmaster_admins_delete")
     * @param User $entity
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteHostmaster(User $entity)
    {
        $this->em()->remove($entity);
        $this->em()->flush();

        return $this->redirectToRoute("base_backend_hostmasters");
    }

}
