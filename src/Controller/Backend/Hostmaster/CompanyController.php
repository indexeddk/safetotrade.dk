<?php

namespace App\Controller\Backend\Hostmaster;

use App\Entity\Company;
use App\Form\CompanyType;
use App\Repository\CompanyRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/hostmaster/company")
 */
class CompanyController extends AbstractController
{
    /**
     * @Route("/", name="backend_hostmaster_company_index", methods={"GET"})
     * @param Request $request
     * @param CompanyRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, CompanyRepository $repository, PaginatorInterface $paginator): Response
    {
        $queryBuilder = $repository->createQueryBuilder('company')
            ->where('company.host = :host')
            ->setParameter('host', $this->getUser()->getHost());

        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));

        return $this->render('backend/hostmaster/company/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Route("/new", name="backend_hostmaster_company_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $entity = new Company();
        $entity->setHost($this->getUser()->getHost());
        $form = $this->createForm(CompanyType::class, $entity);

        $form->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success"]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('backend_hostmaster_company_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Opret ny virksomhed',
        ]);
    }

    /**
     * @Route("/{id}", name="backend_hostmaster_company_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Company $entity
     * @return Response
     */
    public function edit(Request $request, Company $entity): Response
    {
        $form = $this->createForm(CompanyType::class, $entity);

        $form->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success"]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('backend_hostmaster_company_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Rediger virksomhed',
        ]);
    }

    /**
     * @Route("/{id}", name="backend_hostmaster_company_delete", methods={"DELETE"})
     * @param Request $request
     * @param Company $entity
     * @return Response
     */
    public function delete(Request $request, Company $entity): Response
    {
        if ($this->isCsrfTokenValid('delete' . $entity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();
        }

        return $this->redirectToRoute('backend_hostmaster_company_index');
    }
}
