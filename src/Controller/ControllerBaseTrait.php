<?php

namespace App\Controller;


use App\Entity\User;
use Doctrine\ORM\EntityManager;

trait ControllerBaseTrait
{
    /**
     * @return User|null
     */
    protected function getUser()
    {
        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return null;
        }

        if (!\is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return null;
        }

        return $user;
    }

    protected function em(): EntityManager
    {
        return $this->getDoctrine()->getManager();
    }

}
