<?php

namespace App\Controller\Homefeed;

use App\Controller\ControllerBaseTrait;
use App\Controller\ControllerExternalServices;
use App\Entity\Host;
use App\Entity\Image;
use App\Entity\User;
use App\Form\UserNewPassword;
use App\Form\UserType;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @Route("/homefeed")
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property Host host
 */
class SettingsController extends AbstractController
{
    use ControllerBaseTrait;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
    }

    /**
     * INSTAGRAM
     *
     * step 1
     *
     * @Route("/instagram", name="instagram")
     */
    public function instagram()
    {
        return $this->redirect('https://api.instagram.com/oauth/authorize?client_id=157791319496276&scope=user_profile&response_type=code&redirect_uri=https://app.safe2trade.dk/homefeed/instagram-connect');
    }

    /**
     * INSTAGRAM
     *
     * step 2
     *
     * @Route("/instagram-connect", name="instagram_connect")
     * @param Request $request
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function instagramConnect(Request $request)
    {
        //get access token
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://api.instagram.com/oauth/access_token");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "client_id=157791319496276&client_secret=838d316015cfef7cae35fa3ccae6548e&grant_type=authorization_code&redirect_uri=https://app.safe2trade.dk/homefeed/instagram-connect&code=".$request->get("code"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close ($ch);
        $content = json_decode($server_output, true);
        $accessToken = $content["access_token"];

        //get user name
        $content = file_get_contents('https://graph.instagram.com/me?fields=id,username&access_token='.$accessToken);
        $content = json_decode($content, true);
        $username = $content["username"];
        $id = $content["id"];

        $this->getUser()->setSomeInstagram($username);
        $this->getUser()->setSomeInstagramId($id);
        $this->getUser()->setSomeInstagramName($username);
        $this->em()->flush();

        $this->addFlash("success", "You are now connected to Instagram as ".$username);

        return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $this->getUser()->getUuid()]);

    }

    /**
     * FACEBOOK
     *
     * step 1
     *
     * @Route("/fb", name="fb")
     */
    public function fb()
    {
        return $this->redirect('https://www.facebook.com/v9.0/dialog/oauth?client_id=262976211932249&redirect_uri=https://app.safe2trade.dk/homefeed/connect&scope=user_link&state={st=state123abc,ds=123456789}');
    }

    /**
     * FACEBOOK
     *
     * step 2
     *
     * @Route("/connect", name="connect")
     * @param Request $request
     */
    public function fbConnect(Request $request)
    {
        $content = file_get_contents('https://graph.facebook.com/v9.0/oauth/access_token?client_id=262976211932249&redirect_uri=https://app.safe2trade.dk/homefeed/connect&client_secret=58512d23084ce1ecc87ae8e4120f2f03&code='.$request->get("code"));
        $content = json_decode($content, true);
        $accessToken = $content["access_token"];

        $post = ["fields" => "id, name, email, link"];

        header('Content-Type: application/json');
        $ch = curl_init('https://graph.facebook.com/v9.0/me');
        $post = json_encode($post);
        $authorization = "Authorization: Bearer ".$accessToken;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);

        $this->getUser()->setSomeFacebook($result["link"]);
        $this->getUser()->setSomeFacebookName($result["name"]);
        $this->em()->flush();

        $this->addFlash("success", "You are now connected to Facebook as ".$result["name"]);

        return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $this->getUser()->getUuid()]);
    }

    /**
     * SETTINGS PAGE
     *
     * @Route({
     *     "en": "/settings/{uuid}",
     *     "da": "/indstillinger/{uuid}"
     * }, name="homefeed_profile_settings")
     * @param Request $request
     * @param User $user
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws AlreadySubmittedException
     * @throws LogicException
     * @throws UnexpectedTypeException
     */
    public function homefeedProfileSettings(Request $request, User $user)
    {
        // allow access
        if ($user !== $this->getUser()) {
            $this->addFlash("error", "Adgang forbudt!");
            return $this->redirectToRoute("homefeed_profile", ["slug" => $user->getSlug()]);
        }

        if ($user->getLastChangedPersonalInfoAt() && $user->getLastChangedPersonalInfoAt()->format("U") > strtotime("-3 months")) {
            $this->addFlash("info", $this->swissArmyKnife->t("Det er kun muligt at ændre sin information én gang hver 3. måneder", "homefeed"));
        }

        //"before"-values
        $beforeHash = md5(implode(",", [
            $user->getFirstname(),
            $user->getLastname(),
            $user->getAddress(),
            $user->getZip(),
            $user->getCity(),
            $user->getCountry(),
            $user->getEmail(),
            $user->getPhone(),
            $user->getBirthday()->format("Y-m-d")
        ]));

        //profile image
        $defaultData = ["foo"=>"bar"];
        $formProfileImage = $this->createFormBuilder($defaultData)
            ->add("image", FileType::class, [
                "mapped" => false
            ])
            ->getForm();

        $formProfileImage->handleRequest($request);
        if ($formProfileImage->isSubmitted() && $formProfileImage->isValid()) {

            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $formProfileImage->get("image")->getData();

            $newFilename = false;
            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "profile_image_directory");
            }
            $user->setProfileImagePath($newFilename);

            //reset old featured image(s)
            $featuredImages = $user->getImagesFeaturedFirst(true);

            /** @var Image $oldFeaturedImage */
            foreach ($featuredImages as $oldFeaturedImage) {
                $oldFeaturedImage->setFeatured(false);
                $this->em()->flush();
            }

            //new image entity
            $image = new Image();
            $image->setHost($this->host);
            $image->setUuid(Uuid::v4());
            $image->setName($newFilename);
            $image->setPath("/uploads/profile/".$newFilename);
            $image->setFeatured(true);

            $this->em()->persist($image);
            $this->em()->flush();

            $user->addImage($image);
            $this->em()->flush();

            $this->addFlash("success", "Profilbillede gemt!");
            return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $user->getUuid()]);
        }

        //userForm
        $userForm = $this->createForm(UserType::class, $user);
        $userForm->add("Gem", SubmitType::class, [
            "attr" => [
                "class" => "btn btn-success float-right"
            ]
        ]);

        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {

            $email = $user->getEmail();
            $existingDeletedUser = $this->em()->getRepository("App:DeletedUser")->findOneBy(["host" => $this->host, "email" => $email]);
            $existingUser = $this->em()->getRepository("App:User")->createQueryBuilder("user")
                ->where("user.host = :host")
                ->setParameter("host", $this->host)
                ->andWhere("user.email = :email")
                ->setParameter("email", $email)
                ->andWhere("user.uuid != :uuid")
                ->setParameter("uuid", $this->getUser()->getUuid())
                ->getQuery()
                ->getResult()
            ;

            if ($existingDeletedUser || $existingUser) {
                $this->addFlash("error", "E-mailen (".$email.") findes allerede i systemet. Oplysningerne er ikke gemt.");
                return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $this->getUser()->getUuid()]);
            }

            //"after"-values
            $afterHash = md5(implode(",", [
                $user->getFirstname(),
                $user->getLastname(),
                $user->getAddress(),
                $user->getZip(),
                $user->getCity(),
                $user->getCountry(),
                $user->getEmail(),
                $user->getPhone(),
                $user->getBirthday()->format("Y-m-d")
            ]));

            //set "last changed personal information at (datetime)" due to possible 3 months "cooldown" - not allowed to change core information again
            if ($beforeHash != $afterHash) {
                $user->setLastChangedPersonalInfoAt(new DateTime("NOW"));
            }

            //update username
            $user->setUsername($user->getEmail());

            $this->em()->flush();
            $this->addFlash("success", "Indstillinger gemt!");
            return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $user->getUuid()]);
        }

        $formPassword = $this->createForm(UserNewPassword::class, $user);
        $formPassword->remove("email");
        $formPassword->add("Gem", SubmitType::class, ["attr" => ["class" => "btn btn-success btn-sm float-right"]]);
        $formPassword->handleRequest($request);
        if ($formPassword->isSubmitted() && $formPassword->isValid()) {
            $password = $formPassword->getData()->getPassword();
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setSecret($this->swissArmyKnife->makeSecret($password));
            $this->addFlash("success", "Adgangskoden er nu ændret");
            $this->em()->flush();
            return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $this->getUser()->getUuid()]);
        }

        return $this->render("homefeed/settings.html.twig", [
            "user" => $user,
            "formProfileImage" => $formProfileImage->createView(),
            "userForm" => $userForm->createView(),
            "formPassword" => $formPassword->createView()
        ]);
    }

    /**
     * DISCONNECT USER
     *
     * @Route("/settings/disconnect/instagram", name="homefeed_settings_disconnect_instagram")
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function settingsDisconnectInstagram()
    {
        $this->getUser()->setSomeInstagram("");
        $this->getUser()->setSomeInstagramName("");
        $this->em()->flush();

        return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $this->getUser()->getUuid()]);
    }

    /**
     * DISCONNECT USER
     *
     * @Route("/instagram-disconnect", name="homefeed_settings_disconnect_instagram_deauthorize")
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function settingsInstagramDeauthorizeCallbackURL(Request $request)
    {
        //$this->getUser()->setSomeInstagram("");
        //$this->getUser()->setSomeInstagramName("");
        //$this->em()->flush();

        return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $this->getUser()->getUuid()]);
    }

    /**
     * DISCONNECT USER
     *
     * @Route("/instagram-delete-data", name="homefeed_settings_disconnect_instagram_delete_data")
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function settingsInstagramDeleteData(Request $request)
    {
        //$this->getUser()->setSomeInstagram("");
        //$this->getUser()->setSomeInstagramName("");
        //$this->em()->flush();

        return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $this->getUser()->getUuid()]);
    }

    /**
     * DISCONNECT USER
     *
     * @Route("/settings/disconnect/user", name="homefeed_settings_disconnect_user")
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function settingsDisconnectUser()
    {
        $this->getUser()->setSomeFacebook("");
        $this->getUser()->setSomeFacebookName("");
        $this->em()->flush();

        return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $this->getUser()->getUuid()]);
    }

    /**
     * DELETE PROFILE IMAGE
     *
     * @Route("/settings/delete/image/{uuid}", name="homefeed_settings_delete_image")
     * @param Image $imageToDelete
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function settingsDeleteImage(Image $imageToDelete)
    {
        $user = $this->getUser();
        $wasFeatured = $imageToDelete->getFeatured();

        $user->removeImage($imageToDelete);
        $this->em()->flush();

        $this->em()->remove($imageToDelete);
        $this->em()->flush();

        //get updated list of images
        $images = $user->getImagesFeaturedFirst();

        //get new featured image, if "this image" was featured
        $newFeaturedImage = false;
        if ($wasFeatured) {
            foreach (array_reverse($images) as $image) {
                $newFeaturedImage = $image;
                break;
            }
        }

        //set user profile image path and save new featured image
        if ($newFeaturedImage) {
            $newFeaturedImage->setFeatured(true);
            $user->setProfileImagePath($newFeaturedImage->getName());
        }
        if(!$images || sizeof($images) < 1) {
            $user->setProfileImagePath(null);
        }

        $this->em()->flush();

        $this->addFlash("success", "Profilbillede er nu slettet!");

        return $this->redirectToRoute("homefeed_profile_settings", [
            "uuid" => $this->getUser()->getUuid()
        ]);
    }

    /**
     * SET FEATURED (first) PROFILE IMAGE
     *
     * @Route("/settings/feature/image/{uuid}", name="homefeed_feature_image")
     * @param Image $image
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function settingsFeatureImage(Image $image)
    {
        $user = $this->getUser();

        //reset old featured image(s)
        $featuredImages = $user->getImagesFeaturedFirst(true);

        /** @var Image $oldFeaturedImage */
        foreach ($featuredImages as $oldFeaturedImage) {
            $oldFeaturedImage->setFeatured(false);
            $this->em()->flush();
        }

        $image->setFeatured(true);
        $user->setProfileImagePath($image->getName());

        $this->em()->flush();

        $this->addFlash("success", "Standard profil billede er nu sat!");

        return $this->redirectToRoute("homefeed_profile_settings", [
            "uuid" => $this->getUser()->getUuid()
        ]);
    }

    /**
     * DELETE USER
     *
     * @Route("/settings/delete/user/{uuid}", name="homefeed_settings_delete_user", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @param TokenStorageInterface $tokenStorage
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function settingsDeleteUser(Request $request, User $user, TokenStorageInterface $tokenStorage)
    {
        $email = $user->getEmail();

        if ($this->isCsrfTokenValid("delete".$user->getId(), $request->request->get("_token"))) {
            //$this->swissArmyKnife->deleteUser($user);
            $user->setDeleteMe(true);
            $this->em()->flush();
        }

        $this->addFlash("success", "Der er nu sendt en anmodning om sletning til administrator");

        // force user logout!
        //$tokenStorage->setToken();

        //return $this->redirect("https://".$this->host->getDomain());

        return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $this->getUser()->getUuid()]);
    }

    /**
     * EXPORT DATA (GDPR)
     *
     * @Route("/settings/export/data/{uuid}", name="homefeed_settings_export_data")
     * @param User $user
     * @return BinaryFileResponse
     */
    public function settingsExportData(User $user): BinaryFileResponse
    {
        $data = [];
        $filesystem = new Filesystem();
        $fileName = $user->getUuid()."_".date("Y-m-d-H-i-s").".json";
        $filePath = __DIR__."/../../../public/uploads/tmp/".$fileName;

        //user
        list($user) = $this->em()->getRepository("App:User")->createQueryBuilder("entity")
            ->where("entity.id = :thisUserId")
            ->setParameter("thisUserId", $this->getUser()->getId())
            ->setMaxResults(1)
            ->getQuery()
            ->getArrayResult()
        ;
        unset($user["password"]);
        unset($user["secret"]);
        $data["user"] = $user;

        //deals
        $data["deals"] = $this->em()->getRepository("App:Deal")->createQueryBuilder("entity")
            ->where("entity.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("entity.buyer = :thisUser OR entity.seller = :thisUser")
            ->setParameter("thisUser", $this->getUser())
            ->orderBy("entity.createdAt", "ASC")
            ->getQuery()
            ->getArrayResult()
        ;

        //reviews
        $data["reviews"] = $this->em()->getRepository("App:Review")->createQueryBuilder("entity")
            ->where("entity.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("entity.fromUser = :thisUser OR entity.toUser = :thisUser")
            ->setParameter("thisUser", $this->getUser())
            ->orderBy("entity.createdAt", "ASC")
            ->getQuery()
            ->getArrayResult()
        ;

        //messages
        $data["messages"] = $this->em()->getRepository("App:Message")->createQueryBuilder("entity")
            ->where("entity.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("entity.fromUser = :thisUser OR entity.toUser = :thisUser")
            ->setParameter("thisUser", $this->getUser())
            ->orderBy("entity.createdAt", "ASC")
            ->getQuery()
            ->getArrayResult()
        ;

        $filesystem->dumpFile($filePath, json_encode($data));
        $file = new File($filePath);
        return $this->file($file, $fileName);
    }

    /**
     * UNSUBSCRIBE ActiveCampaign
     *
     * @Route("/settings/active-campaign/unsubscribe/{uuid}", name="homefeed_settings_unsubscribe_active_campaign")
     * @param User $user
     * @param ControllerExternalServices $externalServices
     * @return RedirectResponse
     */
    public function settingsUnsubscribeActiveCampaign(User $user, ControllerExternalServices $externalServices)
    {
        $externalServices->activeCampaignUnsubscribe($user);

        $this->addFlash("success", "Notifikationer er nu afmeldt!");

        return $this->redirectToRoute("homefeed_profile_settings", ["uuid" => $user->getUuid()]);
    }

}