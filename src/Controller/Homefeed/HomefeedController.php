<?php

namespace App\Controller\Homefeed;

use App\Controller\ControllerBaseTrait;
use App\Entity\Abuse;
use App\Entity\Deal;
use App\Entity\Host;
use App\Entity\Message;
use App\Entity\Notification;
use App\Entity\Review;
use App\Entity\ReviewChange;
use App\Entity\User;
use App\Form\DealType;
use App\Form\ReviewType;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @Route("/homefeed")
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property Host host
 */
class HomefeedController extends AbstractController
{
    use ControllerBaseTrait;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
    }

    /**
     * HOMEFEED
     *
     * @Route({
     *     "en": "/home",
     *     "da": "/"
     * }, name="homefeed_index")
     *
     * @param Request $request
     * @return Response
     */
    public function homefeedIndex(Request $request)
    {
        $blogs = $this->em()->getRepository("App:Blog")->getEntityQuery($request->get("q"));

        $deal = new Deal();
        $newDeals = $this->getDealsByUser($this->getUser(), $deal::STATUS_NEW);
        $currentDeals = $this->getDealsByUser($this->getUser(), $deal::STATUS_ACCEPTED);

        //how have I poked in the last day
        $pokedUsers = [];
        $todaysNotifications = $this->em()->getRepository("App:Notification")->getTodaysPokes($this->getUser());

        if ($todaysNotifications) {
            /** @var Notification $notification */
            foreach ($todaysNotifications as $notification) {
                $pokedUsers[] = $notification->getToUser()->getId();
            }
        }

        //if ($dealsWithThisProfile) {
        //    $showInfo = true;
        //}

        return $this->render("homefeed/homefeed.html.twig", [
            "blogs" => $blogs,
            "newDeals" => $newDeals,
            "currentDeals" => $currentDeals,
            "pokedUsers" => $pokedUsers,
            "reviewForm" => $this->createReviewForm()->createView()
        ]);
    }

    /**
     * SHOW ALL NOTIFICATIONS
     *
     * @Route({
     *     "en": "/notifications/{uuid}",
     *     "da": "/notifikationer/{uuid}"
     * }, name="homefeed_all_notifications")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function homefeedShowallNotification(Request $request, User $user)
    {
        // allow access
        if ($user !== $this->getUser()) {
            $this->addFlash("error", "Adgang forbudt!");
            return $this->redirectToRoute("homefeed_profile", ["slug" => $user->getSlug()]);
        }

        $notifications = $this->em()->getRepository("App:Notification")->createQueryBuilder("notification")
            ->where("notification.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("notification.toUser = :thisUser")
            ->setParameter("thisUser", $this->getUser())
            ->orderBy("notification.createdAt", "DESC")
            ->getQuery()
            ->getResult()
        ;

        return $this->render("homefeed/all_notifications.html.twig", [
            "user" => $user,
            "notifications" => $notifications
        ]);
    }

    /**
     * RENDER FUNCTION - Notifications
     *
     * @Route("/render/menu/notifications/", name="homefeed_render_menu_notifications")
     * @return Response
     */
    public function renderMenuNotifications()
    {
        //get unread notifications
        $unreadNotifications = $this->em()->getRepository("App:Notification")->createQueryBuilder("notification")
            ->join("notification.fromUser", "fromUser")
            ->select("notification.id")
            ->where("notification.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("notification.toUser = :thisUser")
            ->setParameter("thisUser", $this->getUser())
            ->andWhere("notification.seen != :true")
            ->setParameter("true", true)
            ->andWhere("fromUser.deleted IS NULL")
            ->orderBy("notification.createdAt", "DESC")
            ->getQuery()
            ->getResult()
        ;

        $limit = 10;
        if (sizeof($unreadNotifications) > $limit) {
            $limit = sizeof($unreadNotifications);
        }

        //get all both seen and unseen
        $notifications = $this->em()->getRepository("App:Notification")->createQueryBuilder("notification")
            ->join("notification.fromUser", "fromUser")
            ->where("notification.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("notification.toUser = :thisUser")
            ->setParameter("thisUser", $this->getUser())
            ->andWhere("fromUser.deleted IS NULL")
            ->orderBy("notification.createdAt", "DESC")
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;

        return $this->render("homefeed/include/render_menu_notifications.html.twig", [
            "unreadNotifications" => $unreadNotifications,
            "notifications" => $notifications
        ]);
    }

    /**
     * RENDER FUNCTION - Deals
     *
     * @Route("/render/menu/deals/", name="homefeed_render_menu_deals")
     * @return Response
     */
    public function renderMenuDeals()
    {
        $deal = new Deal();

        $newDeals = $this->em()->getRepository("App:Deal")->createQueryBuilder("deal")
            ->where("deal.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("deal.seller = :thisUser")
            ->setParameter("thisUser", $this->getUser())
            ->andWhere("deal.status = :statusNew")
            ->setParameter("statusNew", $deal::STATUS_NEW)
            ->andWhere("deal.createdAt LIKE :today")
            ->setParameter("today", date("Y-m-d")."%")
            ->orderBy("deal.createdAt", "DESC")
            ->getQuery()
            ->getResult()
        ;

        $limit = 10;
        if (sizeof($newDeals) > $limit) {
            $limit = sizeof($newDeals);
        }

        $deals = $this->em()->getRepository("App:Deal")->createQueryBuilder("deal")
            ->where("deal.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("deal.buyer = :thisUser OR deal.seller = :thisUser")
            ->setParameter("thisUser", $this->getUser())
            ->andWhere("deal.status != :statusDismissed")
            ->setParameter("statusDismissed", $deal::STATUS_DISMISSED)
            ->orderBy("deal.createdAt", "DESC")
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;

        return $this->render("homefeed/include/render_menu_deals.html.twig", [
            "newDeals" => $newDeals,
            "deals" => $deals
        ]);
    }

    /**
     * SHOW ALL DEAL
     *
     * @Route({
     *     "en": "/deals/{uuid}",
     *     "da": "/handler/{uuid}"
     * }, name="homefeed_all_deals")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function homefeedShowallDeals(Request $request, User $user)
    {
        // allow access
        if ($user !== $this->getUser()) {
            $this->addFlash("error", "Adgang forbudt!");
            return $this->redirectToRoute("homefeed_profile", ["slug" => $user->getSlug()]);
        }

        $deals = $this->em()->getRepository("App:Deal")->createQueryBuilder("deal")
            ->where("deal.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("deal.buyer = :thisUser OR deal.seller = :thisUser")
            ->setParameter("thisUser", $this->getUser())
            ->orderBy("deal.createdAt", "DESC")
            ->getQuery()
            ->getResult()
        ;

        return $this->render("homefeed/all_deals.html.twig", [
            "conditions" => $this->swissArmyKnife->getDealConditions(),
            "user" => $user,
            "deals" => $deals
        ]);
    }

    /**
     * RENDER FUNCTION - Messages
     *
     * @Route("/render/menu/messages/", name="homefeed_render_menu_messages")
     * @param MessageController $messageController
     * @return Response
     */
    public function renderMenuMessages(MessageController $messageController)
    {
        list($messages, $unreadMessages) = $messageController->homefeedMessagesOverview(true);

        return $this->render("homefeed/include/render_menu_messages.html.twig", [
            "unreadMessages" => $unreadMessages,
            "messages" => $messages
        ]);
    }

    /**
     * @param bool|Review $entity
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createReviewForm($entity = false) {

        $editReview = true;
        if(!$entity) {
            $editReview = false;
            $entity = new Review();
            $entity->setHost($this->host);
            $entity->setUuid(Uuid::v4());
        }

        $form = $this->createForm(ReviewType::class, $entity);
        $form
            ->add("dealuuid", HiddenType::class, [
                "mapped" => false,
                "required" => false,
            ])
            ->remove("deal")
            ->remove("fromUser")
            ->remove("toUser")
            ->remove("rating")
            ->add("rating", ChoiceType::class, [
                "choices" => [
                    5 => 5,
                    4 => 4,
                    3 => 3,
                    2 => 2,
                    1 => 1,
                ],
                "expanded" => true,
                "multiple" => false,
                "required" => true,
                "attr" => [
                    "class" => "checkboxes-as-stars"
                ]
            ])
        ;
        if($editReview) {
            $form
                ->add("reviewuuid", HiddenType::class, [
                    "mapped" => false,
                    "required" => false,
                    "data" => $entity->getUuid()
                ])
                ->add("deal", HiddenType::class, [
                    "data" => $entity->getDeal()->getId()
                ])
                ->add("fromUser", HiddenType::class, [
                    "data" => $entity->getFromUser()->getId()
                ])
                ->add("toUser", HiddenType::class, [
                    "data" => $entity->getToUser()->getId()
                ])
            ;
        }

        return $form;
    }

    /**
     * REPORT ABUSE
     *
     * @Route("/review/report/abuse/{uuid}/{reason}", name="review_report_abuse")
     * @Route("/review/report/abuse/{uuid}", name="review_report_abuse_other_reason")
     * @param Request $request
     * @param Review $review
     * @param bool $reason
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function reviewReportAbuse(Request $request, Review $review, $reason = false)
    {
        if (!$reason) {
            $reason = $request->get("text");
        }

        if (!trim($reason)) {
            $this->addFlash("error", "Ingen årsag blev givet. Prøv venligst igen.");
            return $this->redirectToRoute("homefeed_profile", ["slug" => $this->getUser()->getSlug()]);
        }

        $entity = new Abuse();
        $entity->setHost($this->host);
        $entity->setUuid(Uuid::v4());
        $entity->setType($entity::TYPE_REVIEW);
        $entity->setReview($review);
        $entity->setReason($reason);
        $entity->setInformer($this->getUser());
        $entity->setAbuser($review->getFromUser());

        $this->em()->persist($entity);
        $this->em()->flush();

        $this->addFlash("success", "Tak! Din anmeldelse er nu afsendt!");

        return $this->redirectToRoute("homefeed_profile", ["slug" => $this->getUser()->getSlug()]);
    }

    /**
     * NOTIFICATION
     *
     * @Route("/poke", name="deal_form_poke")
     * @param Request $request
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function pokeNotification(Request $request)
    {
        /** @var User $buyer */
        $buyer = $this->em()->getRepository("App:User")->findOneBy(["uuid" => $request->get("uuid")]);

        if($buyer) {

            $entity = new Notification();
            $this->swissArmyKnife->notify($entity::TYPE_POKE, $buyer);

            return $this->json([
                "status" => "success"
            ]);
        }

        return $this->json([
            "status" => "failed"
        ]);
    }

    /**
     * MARK NOTIFICATIONS AS READ/SEEN
     *
     * @Route("/notifications/set/seen", name="notifications_set_seen")
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function notificationsSetSeen()
    {
        /** @var User $buyer */
        $notifications = $this->em()->getRepository("App:Notification")->findBy(["host" => $this->host, "toUser" => $this->getUser()]);

        if($notifications) {

            /** @var Notification $notification */
            foreach ($notifications as $notification) {
                $notification->setSeen(true);
            }
            $this->em()->flush();
        }

        return $this->json([
            "status" => "success"
        ]);
    }

    /**
     * CREATE/EDIT REVIEW VIA MODAL
     *
     * @Route("/form/create/review", name="review_form_create_review")
     * @param Request $request
     * @return JsonResponse
     * @throws ORMException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws OptimisticLockException
     * @throws AlreadySubmittedException
     * @throws LogicException
     * @throws \Symfony\Component\Form\Exception\OutOfBoundsException
     * @throws \Symfony\Component\Form\Exception\RuntimeException
     * @throws UnexpectedTypeException
     */
    public function homefeedFormCreateReview(Request $request)
    {
        //EDIT - find existing review
        $reviewRequest = $request->get("review");
        $reviewuuid = $reviewRequest["reviewuuid"] ?? false;
        if ($reviewuuid) {
            $entity = $this->em()->getRepository("App:Review")->findOneBy([
                "host" => $this->host,
                "uuid" => $reviewuuid,
                "fromUser" => $this->getUser()
            ]);

            if (!$entity) {
                return $this->json([
                    "status" => "failed"
                ]);
            }
            $deal = $entity->getDeal();
        }

        //NEW review
        else {
            $entity = new Review();
            $entity->setHost($this->host);
            $entity->setUuid(Uuid::v4());
        }

        //form
        $form = $this->createForm(ReviewType::class, $entity);
        $form->add("dealuuid", HiddenType::class, [
                "mapped" => false,
                "required" => false,
            ]);

        //EDIT
        if ($reviewuuid) {
            $form->add("reviewuuid", HiddenType::class, [
                "mapped" => false,
                "required" => false,
            ]);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //NEW
            if (!$reviewuuid) {
                /** @var Deal $deal */
                $deal = $this->em()
                    ->getRepository("App:Deal")
                    ->findOneBy([
                        "uuid" => $form->get("dealuuid")
                            ->getData()
                    ])
                ;
                if (!$deal) {
                    return $this->json([
                        "status" => "failed",
                        "error" => "noDeal"
                    ]);
                }

                //update deal
                $deal->setStatus($deal::STATUS_COMPLETED);

                //set deal and users for review
                $entity->setDeal($deal);
                $entity->setFromUser($deal->getBuyer());
                $entity->setToUser($deal->getSeller());

                //@todo? Identify From -> is thisUser == buyer, is thisUser == seller?
            }

            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get("image")->getData();

            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "review_image_directory");
                $entity->setImage($newFilename);
            }

            //only persist if NEW
            if (!$reviewuuid) {
                $this->em()->persist($entity);
            }
            $this->em()->flush();
            $this->addFlash("success", "Bedømmelse gemt!");

            //save review changes
            $reviewChange = new ReviewChange();
            $reviewChange->setHost($this->host);
            $reviewChange->setReview($entity);
            $reviewChange->setHeadline($entity->getHeadline());
            $reviewChange->setComment($entity->getComment());
            $reviewChange->setRating($entity->getRating());
            $reviewChange->setImage($entity->getImage());
            $this->em()->persist($reviewChange);
            $this->em()->flush();

            //notify seller
            $entity = new Notification();
            $this->swissArmyKnife->notify($entity::TYPE_REVIEW, $deal->getSeller(), false, $deal);

            return $this->json([
                "status" => "success"
            ]);
        }

        return $this->json([
            "status" => "failed"
        ]);
    }

    /**
     * GET REVIEW FORM/HTML FOR MODAL
     *
     * @Route("/form/get/review/to/edit", name="form_get_review_to_edit")
     * @param Request $request
     * @return JsonResponse
     */
    public function formGetReviewToEdit(Request $request)
    {
        $entity = $this->em()->getRepository("App:Review")->findOneBy([
            "host" => $this->host,
            "uuid" => $request->get("uuid"),
            "fromUser" => $this->getUser()
        ]);

        if (!$entity) {
            return $this->json([
                "status" => "failed"
            ]);
        }

        $html = $this->renderView("homefeed/include/form_edit_review.html.twig", [
            "reviewForm" => $this->createReviewForm($entity)->createView(),
            "imagePath" => $entity->getImage()
        ]);

        return $this->json([
            "status" => "success",
            "html" => $html,
        ]);
    }

    /**
     * CREATE DEAL VIA MODAL
     *
     * @Route("/form/create/deal/{uuid}", name="deal_form_create_deal")
     * @param Request $request
     * @param User $seller
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function homefeedFormCreate(Request $request, User $seller)
    {
        $entity = new Deal();
        $entity->setHost($this->host);
        $entity->setUuid(Uuid::v4());
        $entity->setOrigin($entity::ORIGIN_USER);
        $entity->setStatus($entity::STATUS_NEW);

        $options = ["conditions" => $this->swissArmyKnife->getDealConditions(true)];
        $form = $this->createForm(DealType::class, $entity, $options);
        $form->remove("buyer");
        $form->remove("seller");
        $form->remove("status");
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setSeller($seller);
            $entity->setBuyer($this->getUser());
            //@todo: handle price correctly!

            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get("image")->getData();

            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "deal_image_directory");
                $entity->setImage($newFilename);
            }

            $this->em()->persist($entity);
            $this->em()->flush();

            //notify seller
            $notification = new Notification();
            $this->swissArmyKnife->notify($notification::TYPE_DEAL, $seller, false, $entity);

            $this->addFlash("success", $this->swissArmyKnife->t("Handelsanmodning oprettet!"));

            return $this->json([
                "status" => "success"
            ]);
        }

        return $this->json([
            "status" => "failed"
        ]);
    }

    /**
     * UPLOAD DEAL IMAGE AS TMP JUST TO SHOW USER BEFORE FINAL UPLOAD
     *
     * @Route("/form/upload/tmp/image/{form}", name="deal_form_upload_tmp_image")
     * @param Request $request
     * @param $form
     * @return JsonResponse
     */
    public function homefeedFormUploadTmpImage(Request $request, $form)
    {
        $newFilename = false;

        if ($form == "review") {
            $entity = new Review();
            $form = $this->createForm(ReviewType::class, $entity);
            $form->handleRequest($request);
        }
        else {
            $entity = new deal();
            $options = ["conditions" => $this->swissArmyKnife->getDealConditions(true)];
            $form = $this->createForm(DealType::class, $entity, $options);
            $form->handleRequest($request);
        }


        //image
        /** @var UploadedFile $imageFile */
        $imageFile = $form->get("image")->getData();

        if ($imageFile) {
            $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "tmp_image_directory");
        }

        if ($newFilename) {
            return $this->json([
                "status" => "success",
                "src" => "/uploads/tmp/".$newFilename
            ]);
        }

        return $this->json([
            "status" => "failed"
        ]);
    }

    /**
     * GET DEAL HTML FOR MODAL
     *
     * @Route("/form/get/deal", name="deal_form_get_deal")
     * @param Request $request
     * @return JsonResponse
     */
    public function homefeedFormGetDeal(Request $request)
    {
        /** @var Deal $deal */
        $deal = $this->em()->getRepository("App:Deal")->findOneBy(["uuid" => $request->get("dealuuid")]);

        if(!$deal) {
            return $this->json(["status" => "failed"]);
        }

        $extraText = false;
        if ($deal->getStatus() == $deal::STATUS_NEW && $deal->getBuyer()) {
            $extraText = $deal->getBuyer()->getName()." ".$this->swissArmyKnife->t("anmoder om at købe", "homefeed").":";
        }

        $conditions = $this->swissArmyKnife->getDealConditions();

        $html = $this->renderView("homefeed/include/modal_deal_content.html.twig", [
            "condition" => (is_numeric($deal->getProductCondition()) ? $conditions[$deal->getProductCondition()] : $deal->getProductCondition()),
            "deal" => $deal,
            "extraText" => $extraText
        ]);

        return $this->json([
            "status" => "success",
            "html" => $html,
            "uuid" => $deal->getUuid()
        ]);
    }

    /**
     * GET ACCEPT FOOTER BUTTONS HTML FOR ACCEPT-MODAL
     *
     * @Route("/form/get/deal/accept/footer", name="deal_form_get_model_accept_footer")
     * @param Request $request
     * @return JsonResponse
     */
    public function homefeedFormGetDealAcceptFooter(Request $request)
    {
        /** @var Deal $deal */
        $deal = $this->em()->getRepository("App:Deal")->findOneBy(["uuid" => $request->get("dealuuid")]);

        if(!$deal) {
            return $this->json(["status" => "failed"]);
        }

        $html = $this->renderView("homefeed/include/modal_accept_deal_footer.html.twig", [
            "deal" => $deal,
            "uuid" => $deal->getUuid()
        ]);

        return $this->json([
            "status" => "success",
            "html" => $html,
            "uuid" => $deal->getUuid()
        ]);
    }

    /**
     * HANDLE DEAL ACTION -> ACCEPT
     *
     * @Route("/form/accept/deal", name="deal_form_accept_deal")
     * @param Request $request
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function homefeedFormAcceptDeal(Request $request)
    {
        /** @var Deal $deal */
        $deal = $this->em()->getRepository("App:Deal")->findOneBy(["uuid" => $request->get("dealuuid")]);

        if(!$deal) {
            return $this->json(["status" => "failed"]);
        }

        $deal->setStatus($deal::STATUS_ACCEPTED);
        $deal->setAcceptedAt(new DateTime("NOW"));
        $this->em()->flush();

        return $this->json([
            "status" => "success",
        ]);
    }

    /**
     * HANDLE DEAL ACTION -> DISMISS
     *
     * @Route("/form/dismiss/deal/{dealuuid}", name="deal_dismiss_deal_by_uuid")
     * @Route("/form/dismiss/deal", name="deal_form_dismiss_deal")
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function homefeedFormDismissDeal(Request $request)
    {
        /** @var Deal $deal */
        $deal = $this->em()->getRepository("App:Deal")->findOneBy(["uuid" => $request->get("dealuuid")]);

        if(!$deal) {
            return $this->json(["status" => "failed"]);
        }

        $deal->setStatus($deal::STATUS_DISMISSED);
        $deal->setDismissedAt(new DateTime("NOW"));
        $this->em()->flush();

        //"clean up" notifications
        $notifications = $this->em()->getRepository("App:Notification")->createQueryBuilder("notification")
            ->where("notification.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("notification.deal = :deal")
            ->setParameter("deal", $deal)
            ->getQuery()
            ->getResult()
        ;
        if ($notifications) {
            foreach ($notifications as $notification) {
                $this->em()->remove($notification);
                $this->em()->flush();
            }
        }

        //create new dismissed-notification
        $notification = new Notification();
        $this->swissArmyKnife->notify($notification::TYPE_DEAL, $deal->getBuyer(), $this->getUser(), $deal, "dismissed");
        $this->swissArmyKnife->notify($notification::TYPE_DEAL, $deal->getSeller(), $this->getUser(), $deal, "dismissed");

        //redirect to homefeed
        $routeName = $request->attributes->get('_route');
        if ($routeName == "deal_dismiss_deal_by_uuid") {
            $this->addFlash("success", "Handlen er annulleret!");

            if ($request->get("useruuid") ) {

                /** @var User $user */
                $user = $this->em()->getRepository("App:User")->findOneBy(["uuid" => $request->get("useruuid")]);
                return $this->redirectToRoute("homefeed_profile", ["slug" => $user->getSlug()]);
            }
            return $this->redirectToRoute("homefeed_index");
        }

        //return json
        return $this->json([
            "status" => "success",
        ]);
    }

    /**
     * @param User $user
     * @param string $status
     * @param bool $buyerOrSeller
     * @return int|mixed|string
     */
    public function getDealsByUser(User $user, $status = "new", $buyerOrSeller = false)
    {
        $builder = $this->em()
            ->getRepository("App:Deal")
            ->createQueryBuilder("deal")
            ->leftJoin("deal.buyer", "buyer")
            ->leftJoin("deal.seller", "seller")
            ->where("deal.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("deal.status = :status")
            ->setParameter("status", $status)
            ->andWhere("buyer IS NOT NULL")
            ->andWhere("seller IS NOT NULL")
            ->andWhere("buyer.deleted IS NULL")
            ->andWhere("seller.deleted IS NULL")
            ->orderBy("deal.createdAt", "ASC")
        ;

        if ($buyerOrSeller == "buyer") {
            $builder->andWhere("deal.buyer = :user");
        }
        elseif ($buyerOrSeller == "seller") {
            $builder->andWhere("deal.seller = :user");
        }
        else {
            $builder->andWhere("deal.seller = :user OR deal.buyer = :user");
        }
        $builder->setParameter("user", $user);

        $deals = $builder->getQuery()->getResult();

        return $deals;
    }

    /**
     * GET SEARCH RESULTS
     *
     * @Route("/search", name="homefeed_search")
     * @param Request $request
     * @param CacheManager $cacheManager
     * @return void
     */
    public function homefeedSearch(Request $request, CacheManager $cacheManager)
    {
        $userResults = [];

        $q = $request->get("q");

        //search users
        $query = $this->em()
            ->getRepository("App:User")
            ->createQueryBuilder("user")
            ->where("user.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("user.rolesHierarchy = 30")
            ->andWhere("user.deleted IS NULL")
            ->andWhere("user.email LIKE :q OR user.firstname LIKE :q OR user.lastname LIKE :q OR user.zip LIKE :q OR user.address LIKE :q OR user.city LIKE :q OR user.phone LIKE :q OR CONCAT(user.firstname, ' ', user.lastname) LIKE :q")
            ->setParameter("q", "%".$q."%")
            ->getQuery()
        ;
        $entities = $query->getResult();

        /** @var User $entity */
        foreach ($entities as $entity) {

            /** @var string */
            $image = $cacheManager->getBrowserPath($entity->getProfileImage(), 'backend_profile_thumb');

            $userResults[$entity->getName()] = [
                "type" => "customer",
                "id" => $entity->getId(),
                //"text" => $entity->getName()." |||- ".$entity->getEmail(),
                "text" => $entity->getName(),
                "image" => $image,
                "url" => $this->generateUrl("homefeed_profile", ["slug" => $entity->getSlug()]),
            ];
        }

        $data = [
            "pagination" => [
                "more" => false
            ]
        ];
        if (sizeof($userResults) > 0) {
            ksort($userResults);
            $userResults = array_values($userResults);
            $data["results"][] = [
                "text" => "Profiler",
                "children" => $userResults
            ];
        }
        echo json_encode($data);
        exit;
    }

}