<?php

namespace App\Controller\Homefeed;

use App\Controller\ControllerBaseTrait;
use App\Entity\Deal;
use App\Entity\Host;
use App\Entity\Image;
use App\Entity\Notification;
use App\Entity\Review;
use App\Entity\User;
use App\Form\DealType;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property Host host
 */
class ProfileController extends AbstractController
{
    use ControllerBaseTrait;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
    }

    /**
     * SHOW PROFILE PAGE
     *  - BOTH MY PROFILE PAGE AND OTHERS
     *
     * @Route({
     *     "en": "/profile/{slug}",
     *     "da": "/profil/{slug}"
     * }, name="homefeed_profile")
     * @param Request $request
     * @param User $user
     * @param HomefeedController $homefeedController
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function homefeedProfile(Request $request, User $user, HomefeedController $homefeedController)
    {
        //init
        $deal = new Deal();
        $myProfilePage = false;
        $showInfo = false;
        if ($user === $this->getUser()) {
            $myProfilePage = true;
        }

        //profile image
        $defaultData = ["foo"=>"bar"];
        $formProfileImage = $homefeedController->createFormBuilder($defaultData)
            ->add("image", FileType::class, [
                "mapped" => false
            ])
            ->getForm();

        $formProfileImage->handleRequest($request);
        if ($formProfileImage->isSubmitted() && $formProfileImage->isValid()) {

            //image
            /** @var UploadedFile $imageFile */
            $imageFile = $formProfileImage->get("image")->getData();

            $newFilename = false;
            if ($imageFile) {
                $newFilename = $this->swissArmyKnife->uploadImage($imageFile, "profile_image_directory");
            }

            $user->setProfileImagePath($newFilename);

            //reset old featured image(s)
            $featuredImages = $user->getImagesFeaturedFirst(true);

            /** @var Image $oldFeaturedImage */
            foreach ($featuredImages as $oldFeaturedImage) {
                $oldFeaturedImage->setFeatured(false);
                $this->em()->flush();
            }

            //new image entity
            $image = new Image();
            $image->setHost($this->host);
            $image->setUuid(Uuid::v4());
            $image->setName($newFilename);
            $image->setPath("/uploads/profile/".$newFilename);
            $image->setFeatured(true);

            $this->em()->persist($image);
            $this->em()->flush();

            $user->addImage($image);
            $this->em()->flush();

            $this->addFlash("success", "Profilbillede gemt!");
            return $this->redirectToRoute("homefeed_profile", ["slug" => $user->getSlug()]);
        }

        //current deals
        $currentDeals = $homefeedController->getDealsByUser($user, $deal::STATUS_ACCEPTED);

        //new deals
        $newDeals = $homefeedController->getDealsByUser($user, $deal::STATUS_NEW);

        //do "I" have current deals with "this profile" -> show "my" info
        $dealsWithThisProfile = $this->em()->getRepository("App:Deal")->createQueryBuilder("deal")
            ->innerJoin("deal.buyer", "buyer")
            ->select("deal.id, buyer.id as buyerId, deal.dismissedAt")
            ->where("deal.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("(deal.seller = :me AND deal.buyer = :you) OR (deal.seller = :you AND deal.buyer = :me)")
            ->setParameter("me", $this->getUser())
            ->setParameter("you", $user)
            ->andWhere("deal.status = :statusAccepted OR deal.status = :statusPaid OR deal.status = :statusCompleted")
            ->setParameter("statusAccepted", $deal::STATUS_ACCEPTED)
            ->setParameter("statusPaid", $deal::STATUS_PAID)
            ->setParameter("statusCompleted", $deal::STATUS_COMPLETED)
            ->getQuery()
            ->getResult()
        ;
        if ($dealsWithThisProfile || $user->getShowPersonalInfo()) {
            $showInfo = true;
        }

        //do "I" have NEW deals with "this profile" - deny new deals
        $dealWithThisProfile = $this->em()->getRepository("App:Deal")->createQueryBuilder("deal")
            ->innerJoin("deal.buyer", "buyer")
            //->select("deal.id, buyer.id as buyerId")
            ->where("deal.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("(deal.seller = :you AND deal.buyer = :me)")
            ->setParameter("me", $this->getUser())
            ->setParameter("you", $user)
            //->andWhere("deal.status = :statusNew")
            //->setParameter("statusNew", $deal::STATUS_NEW)
            ->orderBy("deal.createdAt", "DESC")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        //reviews
        $reviews = $this->em()->getRepository("App:Review")->createQueryBuilder("review")
            ->where("review.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("review.rating >= 0")
            ->andWhere("review.toUser = :user")
            ->setParameter("user", $user)
            ->orderBy("review.createdAt", "DESC")
            ->getQuery()
            ->getResult()
        ;

        $totalRating = 0;
        if($reviews) {
            /** @var Review $review */
            foreach ($reviews as $review) {
                $totalRating += $review->getRating();
            }
            $totalRating = $totalRating / sizeof($reviews);
            $totalRating = round($totalRating, 1);

            $user->setRating($totalRating);
            $this->em()->flush();
        }

        //deal form init
        $options = ["conditions" => $this->swissArmyKnife->getDealConditions(true)];
        $form = $this->createForm(DealType::class, $deal, $options);
        $form->remove("buyer");
        $form->remove("seller");
        $form->remove("status");

        //how have I poked in the last day
        $pokedUsers = [];
        $todaysNotifications = [];
        if($this->getUser()) {
            $todaysNotifications = $this->em()->getRepository("App:Notification")->getTodaysPokes($this->getUser());
        }

        if ($todaysNotifications) {
            /** @var Notification $notification */
            foreach ($todaysNotifications as $notification) {
                $pokedUsers[] = $notification->getToUser()->getId();
            }
        }

        //twig options
        $options = [
            "user" => $user,
            "reviews" => $reviews,
            "currentDeals" => $currentDeals,
            "newDeals" => $newDeals,
            "showInfo" => $showInfo,
            "myProfilePage" => $myProfilePage,
            "form" => $form->createView(),
            "reviewForm" => $homefeedController->createReviewForm()->createView(),
            "formProfileImage" => $formProfileImage->createView(),
            "pokedUsers" => $pokedUsers,
            "dealWithThisProfile" => $dealWithThisProfile
        ];

        //show MY profile page vs show OTHERS' profile page
        if ($myProfilePage) {
            return $this->render("homefeed/my_profile.html.twig", $options);
        }
        else {
            return $this->render("homefeed/profile.html.twig", $options);
        }
    }
}