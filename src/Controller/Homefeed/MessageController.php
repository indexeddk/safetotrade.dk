<?php

namespace App\Controller\Homefeed;

use App\Controller\ControllerBaseTrait;
use App\Entity\Host;
use App\Entity\Message;
use App\Entity\User;
use App\Form\MessageType;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @Route("/homefeed")
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property HostHelper hostHelper
 * @property MailHelper mailer
 * @property SwissArmyKnife swissArmyKnife
 * @property Host host
 */
class MessageController extends AbstractController
{
    use ControllerBaseTrait;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer, HostHelper $hostHelper, SwissArmyKnife $swissArmyKnife)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->swissArmyKnife = $swissArmyKnife;
        $this->host = $this->hostHelper->getHost();
    }

    /**
     * MESSAGES USER-2-USER
     *
     * @Route({
     *     "en": "/messages/{uuid}",
     *     "da": "/beskeder/{uuid}"
     * }, name="homefeed_messages")
     * @param User $user
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function homefeedMessages(User $user)
    {
        //form
        $entity = new Message();
        $form = $this->createForm(MessageType::class, $entity);

        $messages = $this->em()->getRepository("App:Message")->createQueryBuilder("message")
            ->where("message.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("(message.fromUser = :me AND message.toUser = :you) OR (message.fromUser = :you AND message.toUser = :me)")
            ->setParameter("me", $this->getUser())
            ->setParameter("you", $user)
            ->orderBy("message.createdAt", "ASC")
            ->getQuery()
            ->getResult()
        ;

        //mark messages as seen (only for messages to "this user")
        if ($messages) {

            /** @var Message $message */
            foreach ($messages as $message) {
                if($message->getToUser() === $this->getUser()) {
                    $message->setSeen(true);
                    $this->em()->flush();
                }
            }
        }

        return $this->render("homefeed/messages_chat_room.html.twig", [
            "user" => $user,
            "form" => $form->createView(),
            "messages" => $messages
        ]);
    }

    /**
     * MESSAGES OVERVIEW
     *
     * @Route({
     *     "en": "/messages",
     *     "da": "/beskeder"
     * }, name="homefeed_messages_overview")
     * @param bool $returnArrays
     * @return array[]|Response
     */
    public function homefeedMessagesOverview($returnArrays = false)
    {
        //$unreadMessages = $this->em()->getRepository("App:Message")->createQueryBuilder("message")
        //    ->where("message.host = :host")
        //    ->setParameter("host", $this->host)
        //    ->andWhere("message.toUser = :me")
        //    ->setParameter("me", $this->getUser())
        //    ->andWhere("message.seen != :true")
        //    ->setParameter("true", true)
        //    ->groupBy("message.fromUser")
        //    ->getQuery()
        //    ->getResult()
        //;

        //$limit = 10;
        //if (sizeof($unreadMessages) > $limit) {
        //    $limit = sizeof($unreadMessages);
        //}

        //$messages = $this->em()->getRepository("App:Message")->createQueryBuilder("message")
        //    ->where("message.host = :host")
        //    ->setParameter("host", $this->host)
        //    ->andWhere("message.toUser = :me")
        //    ->setParameter("me", $this->getUser())
        //    ->groupBy("message.fromUser")
        //    ->setMaxResults($limit)
        //    ->getQuery()
        //    ->getResult()
        //;

        $allMessages = $this->em()->getRepository("App:Message")->createQueryBuilder("message")
            ->where("message.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("message.toUser = :me OR message.fromUser = :me")
            ->setParameter("me", $this->getUser())
            //->andWhere("message.createdAt > :datetime")
            //->setParameter("datetime", date("Y-m-d H:i:s", strtotime("-18 months")))
            ->orderBy("message.createdAt", "DESC")
            ->getQuery()
            ->getResult()
        ;

        $messages = [];
        $unreadMessages = [];

        /** @var Message $message */
        foreach ($allMessages as $message) {

            $otherUser = $message->getFromUser();
            if($otherUser === $this->getUser()) {
                $otherUser = $message->getToUser();
            }

            if (!$message->getSeen() && $message->getFromUser() !== $this->getUser()) {
                $unreadMessages[] = $message->getId();
            }

            //only include the latest message
            $msgKey = $otherUser->getId()."-".$this->getUser()->getId();
            if(!isset($messages[$msgKey])) {
                $messages[$msgKey] = $message;
            }
        }

        if ($returnArrays) {
            return [$messages, $unreadMessages];
        }

        return $this->render("homefeed/messages_overview.html.twig", [
            "unreadMessages" => $unreadMessages,
            "messages" => $messages
        ]);
    }

    /**
     * SEND MESSAGE
     *
     * @Route("/messages/send/message/{uuid}", name="homefeed_messages_send_message")
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function homefeedMessagesSendMessage(Request $request, User $user)
    {
        $entity = new Message();
        $form = $this->createForm(MessageType::class, $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //do not save if no text
            if (!trim($entity->getText())) {
                return $this->json([
                    "status" => "failed"
                ]);
            }

            $entity->setHost($this->host);
            $entity->setUuid(Uuid::v4());
            $entity->setToUser($user);
            $entity->setFromUser($this->getUser());
            $entity->setSeen(false);

            $this->em()->persist($entity);
            $this->em()->flush();

        }

        $html = $this->renderView("homefeed/include/message.html.twig", [
            "message" => $entity
        ]);

        return $this->json([
            "status" => "success",
            "html" => $html
        ]);
    }

    /**
     * GET NEW MESSAGES
     *
     * @Route("/messages/get/new/{uuid}", name="homefeed_messages_get_new")
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function homefeedMessagesGetNew(Request $request, User $user)
    {
        $lastUpdated = $request->get("lastUpdated");

        $messages = $this->em()->getRepository("App:Message")->createQueryBuilder("message")
            ->where("message.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("message.fromUser = :you")
            ->setParameter("you", $user)
            ->andWhere("message.toUser = :me")
            ->setParameter("me", $this->getUser())
            ->andWhere("message.createdAt >= :lastUpdated")
            ->setParameter("lastUpdated", $lastUpdated)
            ->orderBy("message.createdAt", "ASC")
            ->getQuery()
            ->getResult()
        ;

        $html = "";
        if ($messages) {

            /** @var Message $message */
            foreach ($messages as $message) {
                $message->setSeen(true);
                $this->em()->flush();
                $html .= $this->renderView("homefeed/include/message.html.twig", [
                    "message" => $message
                ]);
            }
        }
        else {
            return $this->json([
                "status" => "no-new-messages",
                "lastUpdated" => date("Y-m-d H:i:s")
            ]);
        }

        return $this->json([
            "status" => "success",
            "html" => $html,
            "lastUpdated" => date("Y-m-d H:i:s")
        ]);
    }

    /**
     * DELETE MESSAGE
     *
     * @Route("/messages/delete/{uuid}", name="homefeed_messages_delete_message", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function homefeedMessagesDeleteMessage(Request $request, User $user)
    {
        return $this->json([
            "status" => "success"
        ]);
    }
}