<?php

namespace App\Menu;

use App\Service\SwissArmyKnife;
use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class Menu
{
    /** @var FactoryInterface */
    private $factory;
    /** @var SwissArmyKnife */
    private $sak;

    /**
     * @param FactoryInterface $factory
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(FactoryInterface $factory, SwissArmyKnife $swissArmyKnife)
    {
        $this->factory = $factory;
        $this->sak = $swissArmyKnife;
    }

    /*
     * FRONTEND
     */
    public function frontendMainMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem("root");

        $menu->addChild("Log ind", ["route" => "app_login"]);

        return $menu;
    }

    /*
     * VOLUNTARY
     */
    private function backendVoluntarySidebarMenuArray()
    {

        $menu = [

            //menupunkt
            [
                "route" => "help_page",
                "label" => $this->sak->t("Hjælp"),
                "icon" => "far fa-question-circle",
                "target" => "_blank"
            ],

            [
                "route" => "app_logout",
                "label" => $this->sak->t("Log ud"),
                "icon" => "fa fa-power-off"
            ],
        ];

        return $menu;
    }

    /*
     * BACKEND
     */
    private function backendHostmasterSidebarMenuArray()
    {

        $menu = [

            //menupunkt
            [
                "route" => "base_backend_hostmaster_home",
                "label" => $this->sak->t("Hjem hostmaster"),
                "icon" => "fa fa-home"
            ],

            [
                "route" => "backend_hostmaster_company_index",
                "label" => $this->sak->t("Companies"),
                "icon" => "fa fa-users"
            ],

            //menupunkt
            [
                "route" => "base_backend_hosts",
                "label" => $this->sak->t("Domæner"),
                "icon" => "fa fa-sitemap",
                "children" => [

                    //underpunkt
                    [
                        "route" => "base_backend_hosts",
                        "label" => $this->sak->t("Se domæner")
                    ],

                    //underpunkt
                    [
                        "route" => "base_backend_hosts_create",
                        "label" => $this->sak->t("Opret domæne")
                    ],

                    //underpunkt
                    [
                        "route" => "base_backend_hosts_edit",
                        "label" => $this->sak->t("Rediger domæne"),
                        "hidden" => true
                    ]

                ]
            ]

        ];

        return $menu;
    }

    /*
     * BACKEND
     */
    private function backendEmployeeSidebarMenuArray()
    {

        $menu = [

            //menupunkt
            [
                "route" => "base_backend_admin_home",
                "label" => $this->sak->t("Hjem"),
                "icon" => "fa fa-home"
            ],

            [
                "route" => "backend_admin_deal_index",
                "label" => $this->sak->t("Handler"),
                "icon" => "fas fa-handshake"
            ],

            [
                "route" => "backend_admin_review_index",
                "label" => $this->sak->t("Anmeldelser"),
                "icon" => "fas fa-star-half-alt"
            ],

            //[
            //    "route" => "backend_admin_blog_index",
            //    "label" => $this->sak->t("Blog"),
            //    "icon" => "fab fa-blogger"
            //],

            [
                "route" => "backend_admin_message_index",
                "label" => $this->sak->t("Beskeder"),
                "icon" => "far fa-envelope"
            ],

            [
                "route" => "backend_admin_user_index",
                "label" => $this->sak->t("Brugere"),
                "icon" => "fa fa-users"
            ],

        ];

        return $menu;
    }

    /*
     * BACKEND
     */
    private function backendAdminSidebarMenuArray()
    {

        $menu = [

            //menupunkt
            [
                "route" => "base_backend_admin_home",
                "label" => $this->sak->t("Hjem"),
                "icon" => "fa fa-home"
            ],

//            [
//                "route" => "backend_admin_team_index",
//                "label" => $this->sak->t("Teams"),
//                "icon" => "fa fa-users"
//            ],

            [
                "route" => "backend_admin_deal_index",
                "label" => $this->sak->t("Handler"),
                "icon" => "fas fa-handshake"
            ],

            [
                "route" => "backend_admin_review_index",
                "label" => $this->sak->t("Anmeldelser"),
                "icon" => "fas fa-star-half-alt"
            ],

            //[
            //    "route" => "backend_admin_blog_index",
            //    "label" => $this->sak->t("Blog"),
            //    "icon" => "fab fa-blogger"
            //],

            [
                "route" => "backend_admin_message_index",
                "label" => $this->sak->t("Beskeder"),
                "icon" => "far fa-envelope"
            ],

            //[
            //    "route" => "backend_statistic_index",
            //    "label" => $this->sak->t("Statistik"),
            //    "icon" => "fas fa-poll"
            //],
            //
            //[
            //    "route" => "backend_admin_region_index",
            //    "label" => $this->sak->t("Hospitaler"),
            //    "icon" => "far fa-hospital"
            //],

            [
                "route" => "backend_admin_user_index",
                "label" => $this->sak->t("Brugere"),
                "icon" => "fa fa-users"
            ],

            [
                "route" => "backend_admin_employee_index",
                "label" => $this->sak->t("Administratorer"),
                "icon" => "fas fa-users-cog"
            ],

            //[
            //    "route" => "backend_admin_content_index",
            //    "label" => $this->sak->t("Tekster"),
            //    "icon" => "fas fa-paragraph"
            //],

            //menupunkt
//            [
//                "route" => "base_backend_admin",
//                "label" => $this->sak->t("Adminstratorer"),
//                "icon" => "fa fa-user",
//                "children" => [
//
//                    //underpunkt
//                    [
//                        "route" => "base_backend_admin",
//                        "label" => $this->sak->t("Se administratorer")
//                    ],
//
//                    //underpunkt
//                    [
//                        "route" => "base_backend_admin_create",
//                        "label" => $this->sak->t("Opret administrator")
//                    ],
//
//                    //underpunkt
//                    [
//                        "route" => "base_backend_admin_edit",
//                        "label" => $this->sak->t("Rediger administrator"),
//                        "hidden" => true
//                    ]
//                ],
//            ],

            //menupunkt
//            [
//                "route" => "base_backend_test1",
//                "label" => $this->sak->t("Kunder"),
//                "icon" => "fa fa-users"
//            ],

            //menupunkt
//            [
//                "route" => "base_backend_test2",
//                "label" => $this->sak->t("Produkter"),
//                "icon" => "fa fa-cube",
//                "children" => [
//
//                    //underpunkt
//                    [
//                        "route" => "base_backend_test21",
//                        "label" => $this->sak->t("Produkter")
//                    ],
//
//                    //underpunkt
//                    [
//                        "route" => "base_backend_test22",
//                        "label" => $this->sak->t("Mærker")
//                    ]
//                ]
//            ],

            //menupunkt
//            [
//                "route" => "base_backend_test3",
//                "label" => $this->sak->t("Sider"),
//                "icon" => "fa fa-file"
//            ]
        ];

        return $menu;
    }

    private function generateBackendSidebarMenu($routeName, $request, $role = false)
    {

        if($role == "voluntary") {
            $menuArray = $this->backendVoluntarySidebarMenuArray();
        }
        elseif($role == "employee") {
            $menuArray = $this->backendEmployeeSidebarMenuArray();
        }
        elseif($role == "hostmaster") {
            $menuArray = $this->backendHostmasterSidebarMenuArray();
        }
        else {
            $menuArray = $this->backendAdminSidebarMenuArray();
        }


        $menu = $this->factory->createItem("root", [
            "childrenAttributes" => [
                "class" => "nav nav-main",
            ],
        ]);

        foreach ($menuArray as $data) {

            if (isset($data["children"])) {
                $parent = $menu->addChild(mb_strtolower($data["route"] . "_" . $data["label"], "UTF-8"), [
                    "route" => $data["route"],
                    "extras" => ["safe_label" => true],
                    "label" => "<i class=\"" . $data["icon"] . "\" aria-hidden=\"true\"></i><span>" . $data["label"] . "</span>",
                    "attributes" => ["class" => "nav-parent"],
                    "linkAttributes" => ["onclick" => "return false;"],
                    "childrenAttributes" => ["class" => "nav nav-children"],
                ]);

                $childrenRoutes = [];
                foreach ($data["children"] as $childData) {
                    if(!isset($childData["hidden"])) {
                        $parent->addChild(mb_strtolower($childData["route"] . "_" . $childData["label"], "UTF-8"), [
                            "route" => $childData["route"],
                            "label" => $childData["label"]
                        ]);
                    }
                    $childrenRoutes[] = $childData["route"];
                }
                if (in_array($routeName, $childrenRoutes)) {
                    $parent->setCurrent(true);
                }
            } else {

                // link attributes
                $linkAttributes = [];
                if(isset($data["target"])) {
                    $linkAttributes["target"] = $data["target"];
                }

                $menu->addChild(mb_strtolower($data["route"] . "_" . $data["label"], "UTF-8"), [
                    "route" => $data["route"],
                    "extras" => ["safe_label" => true],
                    "label" => "<i class=\"" . $data["icon"] . "\" aria-hidden=\"true\"></i><span>" .$data["label"]. "</span>",
                    "linkAttributes" => $linkAttributes
                ]);
            }
        }

        return $menu;

    }

    public function backendVoluntarySidebarMenu(RequestStack $requestStack)
    {
        //active switch
        $request = $requestStack->getCurrentRequest();
        $routeName = $request->get('_route');

        $menu = $this->generateBackendSidebarMenu($routeName, $request, "voluntary");

        return $menu;
    }

    public function backendEmployeeSidebarMenu(RequestStack $requestStack)
    {
        //active switch
        $request = $requestStack->getCurrentRequest();
        $routeName = $request->get('_route');

        $menu = $this->generateBackendSidebarMenu($routeName, $request, "employee");

        return $menu;
    }

    public function backendHostmasterSidebarMenu(RequestStack $requestStack)
    {
        //active switch
        $request = $requestStack->getCurrentRequest();
        $routeName = $request->get('_route');

        $menu = $this->generateBackendSidebarMenu($routeName, $request, "hostmaster");

        return $menu;
    }

    public function backendAdminSidebarMenu(RequestStack $requestStack)
    {
        //active switch
        $request = $requestStack->getCurrentRequest();
        $routeName = $request->get('_route');

        $menu = $this->generateBackendSidebarMenu($routeName, $request);

        return $menu;
    }


//    public function backendSidebarMenu(RequestStack $requestStack)
//    {
//
//        //active switch
//        $request = $requestStack->getCurrentRequest();
//        $routeName = $request->get('_route');
//
//        $menu = $this->factory->createItem("root", [
//            "childrenAttributes" => [
//                "class" => "nav nav-main",
//            ],
//        ]);
//
//        //home
//        $parent = $menu->addChild("Home", [
//            "route" => "base_backend_admin_home",
//            "extras" => ["safe_label" => true],
//            "label" => "<i class=\"fa fa-home\" aria-hidden=\"true\"></i><span>Hjem</span>"
//        ]);
//
//        //admins
//        $parent = $menu->addChild("Admins", [
//            "route" => "base_backend_admin",
//            "extras" => ["safe_label" => true],
//            "label" => "<i class=\"fa fa-user\" aria-hidden=\"true\"></i><span>Adminstratorer</span>",
//            "attributes" => ["class" => "nav-parent"],
//            "linkAttributes" => ["onclick" => "return false;"],
//            "childrenAttributes" => ["class" => "nav nav-children"],
//        ]);
//        $parent->addChild("viewAdmins", [
//            "route" => "base_backend_admin",
//            "label" => "Se administratorer"
//        ]);
//        $parent->addChild("createAdmin", [
//            "route" => "base_backend_admin_create",
//            "label" => "Opret administrator"
//        ]);
//        if (in_array($routeName, ["base_backend_admin", "base_backend_admin_create"])) {
//            $parent->setCurrent(true);
//        }
//
//        //companies
//        $parent = $menu->addChild("Companies", [
//            "route" => "base_backend_test1",
//            "extras" => ["safe_label" => true],
//            "label" => "<i class=\"fa fa-building\" aria-hidden=\"true\"></i><span>Virksomheder</span>"
//        ]);
//
//        //products
//        $parent = $menu->addChild("Products", [
//            "route" => "base_backend_test2",
//            "extras" => ["safe_label" => true],
//            "label" => "<i class=\"fa fa-cube\" aria-hidden=\"true\"></i><span>Produkter</span>",
//            "attributes" => ["class" => "nav-parent"],
//            "linkAttributes" => ["onclick" => "return false;"],
//            "childrenAttributes" => ["class" => "nav nav-children"],
//        ]);
//        $parent->addChild("Products", [
//            "route" => "base_backend_test21",
//            "label" => "Produkter"
//        ]);
//        $parent->addChild("Brands", [
//            "route" => "base_backend_test22",
//            "label" => "Mærker"
//        ]);
//        if (in_array($routeName, ["base_backend_test21", "base_backend_test22"])) {
//            $parent->setCurrent(true);
//        }
//
//
//        //pages
//        $parent = $menu->addChild("Pages", [
//            "route" => "base_backend_test3",
//            "extras" => ["safe_label" => true],
//            "label" => "<i class=\"fa fa-file\" aria-hidden=\"true\"></i><span>Sider</span>"
//        ]);
//
//        return $menu;
//    }

}
