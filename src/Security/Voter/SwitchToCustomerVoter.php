<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class SwitchToCustomerVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['CAN_SWITCH_USER']) && $subject instanceof UserInterface;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // if the user is anonymous or if the subject is not a user, do not grant access
        if (!$user instanceof UserInterface || !$subject instanceof UserInterface) {
            return false;
        }

        // Allow ROLE_HOSTMASTER to impersonate
        if ($this->security->isGranted('ROLE_HOSTMASTER')) {
            return true;
        }

        // Allow ROLE_ADMIN to impersonate
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        //if ($user->isAllowedToSwitch()) {
        //    return true;
        //}

        return false;
    }
}
