<?php

namespace App\Form;

use App\Entity\Content;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class ContentType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Content $entity */
//        $entity = $builder->getData();
//        $company = $entity->getCompany();

        $builder
            ->add('humanName', null, [
                'label' => 'Navn',
            ])
            ->add('name', null, [
                'label' => 'Nøgle',
            ])
            ->add('text', null, [
                'label' => 'Indhold / Tekst',
                'attr' => [
                    'style' => 'min-height: 250px;'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Content::class,
            'translation_domain' => 'backend',
        ]);
    }
}
