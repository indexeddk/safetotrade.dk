<?php

namespace App\Form\Type;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class,[
                'label' => 'E-mail'
            ])
            //->add('password', PasswordType::class)
            ->add('firstname', TextType::class, [
                'label' => 'Fornavn'
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Efternavn'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Gem'
            ])
            ->add('address', TextType::class, [
                'label' => 'Adresse'
            ])
            ->add('zip', IntegerType::class, [
                'label' => 'Postnr.'
            ])
            ->add('city', TextType::class, [
                'label' => 'By'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            "translation_domain" => "backend"
        ]);
    }
}
