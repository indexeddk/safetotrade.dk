<?php

namespace App\Form;

use App\Entity\Review;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Review $entity */
        $entity = $builder->getData();
        //$company = $entity->getCompany();

        $builder
            ->add("deal", EntityType::class, [
                "class" => "App\Entity\Deal",
                "query_builder" => function (EntityRepository $er) {
                    return $er->createQueryBuilder("deal")
                        ->orderBy("deal.createdAt", "DESC")
                        ;
                },
                "choice_label" => function ($entity) {
                    return ($entity->getSeller() ? $entity->getSeller()->getName() : "")." => ".($entity->getBuyer() ? $entity->getBuyer()->getName() : "")." (".$entity->getCreatedAt()->format("Y-m-d H:i:s").")";
                },
                "placeholder" => "Vælg handel",
                "label" => "Handel",
                "expanded" => false,
                "multiple" => false,
                "required" => false,
                "attr" => [
                    "class" => "select2"
                ],
            ])
            ->add("fromUser", EntityType::class, [
                "class" => "App\Entity\User",
                "query_builder" => function (EntityRepository $er) {
                    return $er->createQueryBuilder("user")
                        ->where("user.rolesHierarchy = 30")
                        ->orderBy("user.firstname", "ASC")
                        ->addOrderBy("user.lastname", "ASC")
                        ;
                },
                "choice_label" => function ($entity) {
                    return $entity->getFirstname()." ".$entity->getLastname()." - ".$entity->getEmail()."";
                },
                "placeholder" => "Vælg bruger",
                "label" => "Fra",
                "expanded" => false,
                "multiple" => false,
                "required" => false,
                "attr" => [
                    "class" => "select2"
                ],
            ])
            ->add("toUser", EntityType::class, [
                "class" => "App\Entity\User",
                "query_builder" => function (EntityRepository $er) {
                    return $er->createQueryBuilder("user")
                        ->where("user.rolesHierarchy = 30")
                        ->orderBy("user.firstname", "ASC")
                        ->addOrderBy("user.lastname", "ASC")
                        ;
                },
                "choice_label" => function ($entity) {
                    return $entity->getFirstname()." ".$entity->getLastname()." - ".$entity->getEmail()."";
                },
                "placeholder" => "Vælg bruger",
                "label" => "Til",
                "expanded" => false,
                "multiple" => false,
                "required" => false,
                "attr" => [
                    "class" => "select2"
                ],
            ])
            ->add("headline", TextType::class, [
                "required" => false,
                "label" => "Overskrift",
                "attr" => [
                    "placeholder" => "Giv din bedømmelse en overskrift",
                    "maxlength" => 50
                ],
            ])
            ->add("comment", TextareaType::class, [
                "required" => false,
                "label" => "Tekst",
                "attr" => [
                    "rows" => 10,
                    "placeholder" => "Beskrivelse - Hvad begrunder dit valg antal stjerner?",
                    "maxlength" => 1000
                ],
            ])
            ->add("rating", ChoiceType::class, [
                "label" => "Bedømmelse (0-5)",
                "choices" => [
                    0 => 0,
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => 5,
                ],
                "expanded" => true,
                "multiple" => false,
                "required" => false
            ])
            ->add("image", FileType::class, [
                "label" => "Billede",
                "mapped" => false,
                "required" => false,
                "constraints" => [
                    new Image()
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Review::class,
            "translation_domain" => "backend",
        ]);
    }
}
