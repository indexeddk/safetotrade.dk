<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserNewPassword extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("email", EmailType::class,[
                "disabled" => true,
                "label" => "E-mail"
            ])
            ->add("password", RepeatedType::class, [
                "type" => PasswordType::class,
                "invalid_message" => "Adgangskoderne skal være ens. Prøv igen.",
                "options" => ["attr" => ["class" => "password-field"]],
                "required" => true,
                "first_options"  => [
                    "label" => "Adgangskode",
                    "attr" => [
                        "class" => "form-control",
                        "placeholder" => "Ny adgangskode (min. 8 tegn)"
                    ]
                ],
                "second_options" => [
                    "label" => "Gentag adgangskode",
                    "attr" => [
                        "class" => "form-control",
                        "placeholder" => "Gentag adgangskode"
                    ]
                ],


            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => User::class,
            "translation_domain" => "backend"
        ]);
    }
}
