<?php

namespace App\Form;

use App\Entity\Deal;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class DealType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Deal $entity */
        $entity = $builder->getData();

        $builder
            ->add("buyer", EntityType::class, [
                "class" => "App\Entity\User",
                "query_builder" => function (EntityRepository $er) {
                    return $er->createQueryBuilder("user")
                        ->where("user.rolesHierarchy = 30")
                        ->orderBy("user.firstname", "ASC")
                        ->addOrderBy("user.lastname", "ASC")
                        ;
                },
                "choice_label" => function ($entity) {
                    return $entity->getFirstname()." ".$entity->getLastname()." - ".$entity->getEmail()."";
                },
                "placeholder" => "Vælg køber",
                "label" => "Køber",
                "expanded" => false,
                "multiple" => false,
                "required" => true,
                "attr" => [
                    "class" => "select2"
                ],
            ])
            ->add("seller", EntityType::class, [
                "class" => "App\Entity\User",
                "query_builder" => function (EntityRepository $er) {
                    return $er->createQueryBuilder("user")
                        ->where("user.rolesHierarchy = 30")
                        ->orderBy("user.firstname", "ASC")
                        ->addOrderBy("user.lastname", "ASC")
                        ;
                },
                "choice_label" => function ($entity) {
                    return $entity->getFirstname()." ".$entity->getLastname()." - ".$entity->getEmail()."";
                },
                "placeholder" => "Vælg sælger",
                "label" => "Sælger",
                "expanded" => false,
                "multiple" => false,
                "required" => false,
                "attr" => [
                    "class" => "select2"
                ],
            ])
            ->add("status", ChoiceType::class, [
                "label" => "Status",
                "choices" => [
                    "Ny" => $entity::STATUS_NEW,
                    "Accepteret" => $entity::STATUS_ACCEPTED,
                    "Betalt" => $entity::STATUS_PAID,
                    "Afvist" => $entity::STATUS_DISMISSED,
                    "Gennemført" => $entity::STATUS_COMPLETED,
                ],
            ])

            ->add("productName", null, [
                "label" => "Produktnavn",
                "attr" => [
                    "placeholder" => "Produktnavn",
                    "maxlength" => 50
                ]
            ])
            ->add("description", TextareaType::class, [
                "required" => false,
                "label" => "Beskrivelse",
                "attr" => [
                    "rows" => 5,
                    "placeholder" => "Valgfri beskrivelse",
                    "maxlength" => 500
                ]
            ])
            ->add("price", NumberType::class, [
                "label" => "Pris",
                "attr" => [
                    "placeholder" => "Pris"
                ],
                "scale" => 2
            ])
            ->add("currency", ChoiceType::class, [
                "label" => "Valuta",
                "choices" => [
                    $entity::CURRENCY_DKK => $entity::CURRENCY_DKK,
                    $entity::CURRENCY_EUR => $entity::CURRENCY_EUR,
                    $entity::CURRENCY_SEK => $entity::CURRENCY_SEK,
                    $entity::CURRENCY_NOK => $entity::CURRENCY_NOK,
                    $entity::CURRENCY_USD => $entity::CURRENCY_USD,
                    $entity::CURRENCY_GBP => $entity::CURRENCY_GBP,
                ]
            ])
            ->add("productCondition", ChoiceType::class, [
                "label" => "Stand",
                "choices" => $options["conditions"],
                "placeholder" => "Vælg stand"
            ])
            ->add("image", FileType::class, [
                "label" => "Billede",
                "mapped" => false,
                "required" => false,
                "constraints" => [
                    new Image()
                ],
            ])

            //->add("customer", EntityType::class, [
            //    "class" => "Indexed\CustomerBundle\Entity\Customer",
            //    "query_builder" => function (EntityRepository $er) use ($entity) {
            //        return $er->createQueryBuilder("c")
            //            ->where("c.company = :company")
            //            ->setParameter("company", $entity->getCompany())
            //            ->orderBy("c.firstName", "ASC");
            //    },
            //    //"choice_label" => "name",
            //    "choice_label" => function ($entity) {
            //        return $entity->getFirstName()." ".$entity->getLastName()." - ".$entity->getStreetAddress().", ".$entity->getZipcode()." ".$entity->getCity();
            //    },
            //
            //    "multiple" => false,
            //    "required" => false,
            //    "placeholder" => "Choose",
            //    "attr" => [
            //        "class" => "selectpicker select2 select-possible-parent-customer",
            //        "data-live-search" => true,
            //    ],
            //])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Deal::class,
            "translation_domain" => "backend",
        ]);
        $resolver->setRequired("conditions");
    }
}
