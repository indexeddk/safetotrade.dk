<?php

namespace App\Form;

use App\Entity\Message;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("text", TextareaType::class, [
                "required" => false,
                "label" => false,
                "attr" => [
                    "placeholder" => "Skriv besked...",
                    //"maxlength" => 1000
                ],
            ])
            //->add("image", FileType::class, [
            //    "label" => "Billede",
            //    "mapped" => false,
            //    "required" => false,
            //    "constraints" => [
            //        new Image()
            //    ],
            //])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Message::class,
            "translation_domain" => "homefeed",
        ]);
    }
}
