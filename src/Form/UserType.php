<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class UserType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $entity */ 
        $entity = $builder->getData();
        //$company = $entity->getCompany();

        $disableCoreInfo = false;
        if ($entity->getLastChangedPersonalInfoAt() && $entity->getLastChangedPersonalInfoAt()->format("U") > strtotime("-3 months")) {
            $disableCoreInfo =  true;
        }

        $years = [];
        for ($i=0 ; $i<100 ; $i++) {
            $years[] = date("Y") - $i;
        }

        $builder
            ->add("birthday", DateType::class, [
                "label" => "Fødselsdato",
                "years" => $years,
                "disabled" => $disableCoreInfo
            ])
            ->add("firstname", null, [
                "label" => "Fornavn",
                "disabled" => $disableCoreInfo
            ])
            ->add("lastname", null, [
                "label" => "Efternavn",
                "disabled" => $disableCoreInfo
            ])
            ->add("email", EmailType::class, [
                "label" => "E-mail",
                "disabled" => $disableCoreInfo
            ])
            ->add("phone", null, [
                "label" => "Telefon",
                "disabled" => $disableCoreInfo
            ])
            ->add("address", null, [
                "label" => "Adresse",
                "disabled" => $disableCoreInfo
            ])
            ->add("zip", IntegerType::class, [
                "label" => "Postnr.",
                "attr" => [
                    "placeholder" => "Postnr.",
                    "class" => "zip-autocomplete",
                    "min" => "1000",
                    "max" => "9999"
                ],
                "disabled" => $disableCoreInfo
            ])
            ->add("city", TextType::class, [
                "label" => "By",
                "attr" => [
                    "placeholder" => "Bynavn",
                    "class" => "city-autocomplete"
                ],
                "disabled" => $disableCoreInfo
            ])
            ->add("country", CountryType::class, [
                "label" => "Land",
                "empty_data" => "DK",
                "disabled" => $disableCoreInfo
            ])
            ->add("showPersonalInfo", ChoiceType::class, [
                "required" => true,
                "label" => false,
                "choices" => [
                    "Mulighed 1: Vis alle personlige oplysninger til brugere som besøger min profil (jeg har intet at skjule)" => true,
                    "Mulighed 2: Vis kun mine oplysninger for brugere jeg handler/har handlet med" => false
                ],
                "expanded" => true,
                "multiple" => false,
                "empty_data" => false,
            ])
            //->add("someFacebook", null, [
            //    "required" => false,
            //    "label" => "Link til Facebook",
            //    "attr" => [
            //        "placeholder" => "https://www.facebook.com/xxxxxxxxxx/"
            //    ]
            //])
            //->add("someInstagram", null, [
            //    "required" => false,
            //    "label" => "Link til Instagram",
            //    "attr" => [
            //        "placeholder" => "https://www.instagram.com/xxxxxxxxxx/"
            //    ]
            //])
            //->add("someLinkedIn", null, [
            //    "required" => false,
            //    "label" => "Link til LinkedIn",
            //    "attr" => [
            //        "placeholder" => "https://www.linkedin.com/in/xxxxxxxxxx/"
            //    ]
            //])
            //->add("someTwitter", null, [
            //    "required" => false,
            //    "label" => "Link til Twitter",
            //    "attr" => [
            //        "placeholder" => "https://twitter.com/xxxxxxxxxx"
            //    ]
            //])
        ;

        ///** @var User $entity */
        //$entity = $builder->getData();
        //if (!$entity->getId()) {
        //    $choices = [];
        //    $choices["customer"] = User::ROLE_CUSTOMER;
        //    $builder->add("rolesHierarchy", ChoiceType::class, [
        //        "label" => "Rolle",
        //        "choices" => $choices,
        //        "placeholder" => "Vælg rolle"
        //    ]);
        //}
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => User::class,
            "translation_domain" => "backend",
        ]);
    }
}
