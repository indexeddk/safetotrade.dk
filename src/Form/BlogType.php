<?php

namespace App\Form;

use App\Entity\Blog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Blog $entity */
        $entity = $builder->getData();
        //$company = $entity->getCompany();

        $builder
            ->add("headline", null, [
                "label" => "Overskrift",
            ])
            ->add("text", TextareaType::class, [
                "required" => false,
                "label" => "Tekst",
                "attr" => [
                    "rows" => 5
                ]
            ])
            ->add('image', FileType::class, [
                'label' => 'Billede',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new Image()
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Blog::class,
            "translation_domain" => "backend",
        ]);
    }
}
