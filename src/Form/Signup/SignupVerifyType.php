<?php

namespace App\Form\Signup;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SignupVerifyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $entity */
        $entity = $builder->getData();

        $builder
            ->add("verifyEmail", TextType::class, [
                "mapped" => false,
                "required" => false,
                "label" => "Godkend E-mail",
                "attr" => [
                    "placeholder" => "Bekræftelseskode"
                ]
            ])
            ->add("verifySms", TextType::class, [
                "mapped" => false,
                "required" => false,
                "label" => "Godkend Mobilnummer",
                "attr" => [
                    "placeholder" => "Bekræftelseskode"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => User::class,
            "translation_domain" => "frontend"
        ]);
    }
}
