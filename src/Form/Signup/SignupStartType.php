<?php

namespace App\Form\Signup;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SignupStartType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("firstname", TextType::class, [
                "label" => false,
                "attr" => [
                    "placeholder" => "Fornavn",
                    "autocomplete" => "new-password"
                ]
            ])
            ->add("lastname", TextType::class, [
                "label" => false,
                "attr" => [
                    "placeholder" => "Efternavn",
                    "autocomplete" => "new-password"
                ]
            ])
            ->add("email", TextType::class,[
                "label" => false,
                "attr" => [
                    "placeholder" => "E-mail",
                    "autocomplete" => "new-password"
                ]
            ])
            ->add("phone", TextType::class,[
                "label" => false,
                "attr" => [
                    "placeholder" => "Mobilnummer",
                    "autocomplete" => "new-password"
                ],
            ])
            ->add("password", RepeatedType::class, [
                "mapped" => false,
                "type" => PasswordType::class,
                "invalid_message" => "Adgangskoderne skal være ens. Prøv igen.",
                "options" => ["attr" => ["class" => "password-field"]],
                "required" => true,
                "first_options"  => [
                    "label" => false,
                    "attr" => [
                        "class" => "form-control",
                        "placeholder" => "Vælg adgangskode (min. 8 tegn)",
                        "autocomplete" => "new-password"
                    ]
                ],
                "second_options" => [
                    "label" => false,
                    "attr" => [
                        "class" => "form-control",
                        "placeholder" => "Gentag adgangskode",
                        "autocomplete" => "new-password"
                    ]
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => User::class,
            "translation_domain" => "frontend"
        ]);
    }
}
