<?php

namespace App\Form\Signup;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SignupInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $entity */
        $entity = $builder->getData();

        $years = [];
        for ($i=0 ; $i<100 ; $i++) {
            $years[] = date("Y") - $i;
        }

        $builder
            ->add("birthday", DateType::class, [
                "label" => "Fødselsdato",
                "years" => $years
            ])
            ->add("gender", ChoiceType::class, [
                "label" => "Køn",
                "choices" => [
                    "Mand" => $entity::GENDER_MALE,
                    "Kvinde" => $entity::GENDER_FEMALE,
                    "Andet" => $entity::GENDER_OTHER,
                    "Ser ikke dette relevant" => $entity::GENDER_NOT_RELEVANT,
                ]
            ])
            ->add("address", TextType::class, [
                "label" => "Adresse",
                "attr" => [
                    "placeholder" => "Adresse"
                ]
            ])
            ->add("zip", IntegerType::class, [
                "label" => false,
                "attr" => [
                    "placeholder" => "Postnr.",
                    "class" => "zip-autocomplete",
                    "min" => "1000",
                    "max" => "9999"
                ]
            ])
            ->add("city", TextType::class, [
                "label" => false,
                "attr" => [
                    "placeholder" => "Bynavn",
                    "class" => "city-autocomplete"
                ]
            ])
            ->add("country", CountryType::class, [
                "label" => false,
                "attr" => [
                    "class" => "d-block"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => User::class,
            "translation_domain" => "frontend"
        ]);
    }
}
