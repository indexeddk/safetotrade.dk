<?php /** @noinspection PhpUnused */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HostRepository")
 */
class Host
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $domain;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="host")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Slug(fields={"domain"})
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Company", mappedBy="host")
     */
    private $companies;

    /**
     * @ORM\OneToMany(targetEntity=Page::class, mappedBy="host", orphanRemoval=true)
     */
    private $pages;

    /**
     * @ORM\OneToMany(targetEntity=Inquiry::class, mappedBy="host")
     */
    private $inquiries;

    /**
     * @ORM\OneToMany(targetEntity=Blog::class, mappedBy="host")
     */
    private $blogs;

    /**
     * @ORM\OneToMany(targetEntity=Deal::class, mappedBy="host")
     */
    private $deals;

    /**
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="host")
     */
    private $reviews;

    /**
     * @ORM\OneToMany(targetEntity=DeletedUser::class, mappedBy="host")
     */
    private $deletedUsers;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="host")
     */
    private $notifications;

    /**
     * @ORM\OneToMany(targetEntity=Abuse::class, mappedBy="host")
     */
    private $abuses;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="host")
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity=ReviewChange::class, mappedBy="host")
     */
    private $reviewChanges;

    public function __construct()
    {
        $this->companies = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->inquiries = new ArrayCollection();
        $this->blogs = new ArrayCollection();
        $this->deals = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        $this->deletedUsers = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->abuses = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->reviewChanges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function setUsers($users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Company[]
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    public function addCompany(Company $company): self
    {
        if (!$this->companies->contains($company)) {
            $this->companies[] = $company;
            $company->setHost($this);
        }

        return $this;
    }

    public function removeCompany(Company $company): self
    {
        if ($this->companies->contains($company)) {
            $this->companies->removeElement($company);
            // set the owning side to null (unless already changed)
            if ($company->getHost() === $this) {
                $company->setHost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Page[]
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function addPage(Page $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
            $page->setHost($this);
        }

        return $this;
    }

    public function removePage(Page $page): self
    {
        if ($this->pages->removeElement($page)) {
            // set the owning side to null (unless already changed)
            if ($page->getHost() === $this) {
                $page->setHost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Inquiry[]
     */
    public function getInquiries(): Collection
    {
        return $this->inquiries;
    }

    public function addInquiry(Inquiry $inquiry): self
    {
        if (!$this->inquiries->contains($inquiry)) {
            $this->inquiries[] = $inquiry;
            $inquiry->setHost($this);
        }

        return $this;
    }

    public function removeInquiry(Inquiry $inquiry): self
    {
        if ($this->inquiries->removeElement($inquiry)) {
            // set the owning side to null (unless already changed)
            if ($inquiry->getHost() === $this) {
                $inquiry->setHost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Blog[]
     */
    public function getBlogs(): Collection
    {
        return $this->blogs;
    }

    public function addBlog(Blog $blog): self
    {
        if (!$this->blogs->contains($blog)) {
            $this->blogs[] = $blog;
            $blog->setHost($this);
        }

        return $this;
    }

    public function removeBlog(Blog $blog): self
    {
        if ($this->blogs->removeElement($blog)) {
            // set the owning side to null (unless already changed)
            if ($blog->getHost() === $this) {
                $blog->setHost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Deal[]
     */
    public function getDeals(): Collection
    {
        return $this->deals;
    }

    public function addDeal(Deal $deal): self
    {
        if (!$this->deals->contains($deal)) {
            $this->deals[] = $deal;
            $deal->setHost($this);
        }

        return $this;
    }

    public function removeDeal(Deal $deal): self
    {
        if ($this->deals->removeElement($deal)) {
            // set the owning side to null (unless already changed)
            if ($deal->getHost() === $this) {
                $deal->setHost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Review[]
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setHost($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getHost() === $this) {
                $review->setHost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DeletedUser[]
     */
    public function getDeletedUsers(): Collection
    {
        return $this->deletedUsers;
    }

    public function addDeletedUser(DeletedUser $deletedUser): self
    {
        if (!$this->deletedUsers->contains($deletedUser)) {
            $this->deletedUsers[] = $deletedUser;
            $deletedUser->setHost($this);
        }

        return $this;
    }

    public function removeDeletedUser(DeletedUser $deletedUser): self
    {
        if ($this->deletedUsers->removeElement($deletedUser)) {
            // set the owning side to null (unless already changed)
            if ($deletedUser->getHost() === $this) {
                $deletedUser->setHost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setHost($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getHost() === $this) {
                $notification->setHost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Abuse[]
     */
    public function getAbuses(): Collection
    {
        return $this->abuses;
    }

    public function addAbuse(Abuse $abuse): self
    {
        if (!$this->abuses->contains($abuse)) {
            $this->abuses[] = $abuse;
            $abuse->setHost($this);
        }

        return $this;
    }

    public function removeAbuse(Abuse $abuse): self
    {
        if ($this->abuses->removeElement($abuse)) {
            // set the owning side to null (unless already changed)
            if ($abuse->getHost() === $this) {
                $abuse->setHost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setHost($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getHost() === $this) {
                $image->setHost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ReviewChange[]
     */
    public function getReviewChanges(): Collection
    {
        return $this->reviewChanges;
    }

    public function addReviewChange(ReviewChange $reviewChange): self
    {
        if (!$this->reviewChanges->contains($reviewChange)) {
            $this->reviewChanges[] = $reviewChange;
            $reviewChange->setHost($this);
        }

        return $this;
    }

    public function removeReviewChange(ReviewChange $reviewChange): self
    {
        if ($this->reviewChanges->removeElement($reviewChange)) {
            // set the owning side to null (unless already changed)
            if ($reviewChange->getHost() === $this) {
                $reviewChange->setHost(null);
            }
        }

        return $this;
    }

}
