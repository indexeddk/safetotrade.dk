<?php /** @noinspection PhpUnused */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository"),
 * @ORM\Table(
 *  uniqueConstraints={
 *      @UniqueConstraint(name="email_unique",columns={"email", "host_id"}),
 *      @UniqueConstraint(name="username_unique",columns={"username", "host_id"}),
 *      @UniqueConstraint(name="phone_unique",columns={"phone", "host_id"}),
 *  }
 * )
 * @UniqueEntity(
 *  fields={"host", "email"},
 *  errorPath="email",
 *  message="Denne e-mail er allerede i brug. Prøv venligst igen."
 * )
 * @UniqueEntity(
 *  fields={"host", "phone"},
 *  errorPath="phone",
 *  message="Dette mobilnummer er allerede i brug. Prøv venligst igen."
 * )
 */
class User implements UserInterface
{
    const ROLE_HOSTMASTER = 110;
    const ROLE_SUPER_ADMIN = 100;
    const ROLE_ADMIN = 90;
    const ROLE_LEADER = 80;
    const ROLE_EMPLOYEE = 70;
    const ROLE_VOLUNTARY = 40;
    const ROLE_CUSTOMER = 30;
    const ROLE_USER = 10;

    const GENDER_MALE = "male";
    const GENDER_FEMALE = "female";
    const GENDER_OTHER = "other";
    const GENDER_NOT_RELEVANT = "not_relevant";

    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="guid", unique=true)
     */
    protected $uuid;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * This is automatic set when roles is set
     *
     * @ORM\Column(type="smallint")
     */
    private $rolesHierarchy;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secret;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Host", inversedBy="users")
     */
    private $host;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Slug(fields={"firstname", "lastname"})
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $username;

    public const DEFAULT_PROFILE_IMAGE_PATH = "/assets/backend/images/profile.png";
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $profileImagePath = USER::DEFAULT_PROFILE_IMAGE_PATH;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Team", inversedBy="members")
     */
    private $team;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="employees")
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $verificationCodeEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $verificationCodeSms;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     */
    private $verifiedEmail = false;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     */
    private $verifiedSms = false;

    /**
     * @ORM\OneToMany(targetEntity=Blog::class, mappedBy="createdBy")
     */
    private $blogs;

    /**
     * @ORM\OneToMany(targetEntity=Deal::class, mappedBy="buyer")
     */
    private $buyerDeals;

    /**
     * @ORM\OneToMany(targetEntity=Deal::class, mappedBy="seller")
     */
    private $sellerDeals;

    /**
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="fromUser")
     */
    private $fromReviews;

    /**
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="toUser")
     */
    private $toReview;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=1, nullable=true)
     */
    private $rating;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="fromUser")
     */
    private $notificationFrom;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="toUser")
     */
    private $notificationTo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastChangedPersonalInfoAt;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     */
    private $showPersonalInfo = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $someFacebook;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $someInstagram;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $someLinkedIn;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $someTwitter;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $someFacebookName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $someInstagramName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $someInstagramId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $someLinkedInName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $someTwitterName;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="toUser")
     */
    private $massagesTo;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="fromUser")
     */
    private $messagesFrom;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $deleted;

    /**
     * @ORM\OneToMany(targetEntity=Abuse::class, mappedBy="informer")
     */
    private $abusesInformer;

    /**
     * @ORM\OneToMany(targetEntity=Abuse::class, mappedBy="abuser")
     */
    private $abusers;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="user")
     */
    private $images;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $deleteMe;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     */
    private $allowedToSwitchUser = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastSmsSent;

    public function __construct()
    {
        $this->blogs = new ArrayCollection();
        $this->buyerDeals = new ArrayCollection();
        $this->sellerDeals = new ArrayCollection();
        $this->fromReviews = new ArrayCollection();
        $this->toReview = new ArrayCollection();
        $this->notificationFrom = new ArrayCollection();
        $this->notificationTo = new ArrayCollection();
        $this->massagesTo = new ArrayCollection();
        $this->messagesFrom = new ArrayCollection();
        $this->abusesInformer = new ArrayCollection();
        $this->abusers = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Also determine and sets $this->rolesHierarchy
     *
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        $newLevel = 0;
        foreach ($roles as $role) {
            switch ($role) {
                case 'ROLE_HOSTMASTER':
                    $rolesHierarchy = User::ROLE_HOSTMASTER;
                    break;
                case 'ROLE_SUPER_ADMIN':
                    $rolesHierarchy = User::ROLE_SUPER_ADMIN;
                    break;
                case 'ROLE_ADMIN':
                    $rolesHierarchy = User::ROLE_ADMIN;
                    break;
                case 'ROLE_LEADER':
                    $rolesHierarchy = User::ROLE_LEADER;
                    break;
                case 'ROLE_EMPLOYEE':
                    $rolesHierarchy = User::ROLE_EMPLOYEE;
                    break;
                case 'ROLE_VOLUNTARY':
                    $rolesHierarchy = User::ROLE_VOLUNTARY;
                    break;
                case 'ROLE_CUSTOMER':
                    $rolesHierarchy = User::ROLE_CUSTOMER;
                    break;
                case 'ROLE_USER':
                    $rolesHierarchy = User::ROLE_USER;
                    break;
                default:
                    $rolesHierarchy = 1;
                    break;
            }

            if ($rolesHierarchy > $newLevel) {
                $newLevel = $rolesHierarchy;
            }
        }

        $this->rolesHierarchy = $newLevel;

        return $this;
    }

    /**
     * @param string $role
     * @return boolean
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->getFirstname()." ".$this->getLastname();
    }

    public function getSecret(): ?string
    {
        return $this->secret;
    }

    public function setSecret(string $secret): self
    {
        $this->secret = $secret;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getHost(): ?Host
    {
        return $this->host;
    }

    public function setHost(?Host $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getProfileImage(): ?string
    {
        if (!$this->profileImagePath) {
            return self::DEFAULT_PROFILE_IMAGE_PATH;
        }
        if ($this->profileImagePath != self::DEFAULT_PROFILE_IMAGE_PATH) {
            return "/uploads/profile/".$this->profileImagePath;
        }
        return $this->profileImagePath;
    }

    public function getProfileImagePath(): ?string
    {
        return $this->profileImagePath;
    }

    public function setProfileImagePath(?string $profileImagePath): self
    {
        $this->profileImagePath = $profileImagePath;

        return $this;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getRolesHierarchy(): ?int
    {
        return $this->rolesHierarchy;
    }

    /**
     * Also determine and sets $this->roles
     *
     * @param int $rolesHierarchy
     * @return $this
     */
    public function setRolesHierarchy(int $rolesHierarchy): self
    {
        $this->rolesHierarchy = $rolesHierarchy;

        switch ($rolesHierarchy) {
            case User::ROLE_HOSTMASTER:
                $roles = ['ROLE_HOSTMASTER'];
                break;
            case User::ROLE_SUPER_ADMIN:
                $roles = ['ROLE_SUPER_ADMIN'];
                break;
            case User::ROLE_ADMIN:
                $roles = ['ROLE_ADMIN'];
                break;
            case User::ROLE_LEADER:
                $roles = ['ROLE_LEADER'];
                break;
            case User::ROLE_EMPLOYEE:
                $roles = ['ROLE_EMPLOYEE'];
                break;
            case User::ROLE_VOLUNTARY:
                $roles = ['ROLE_VOLUNTARY'];
                break;
            case User::ROLE_CUSTOMER:
                $roles = ['ROLE_CUSTOMER'];
                break;
            case User::ROLE_USER:
                $roles = ['ROLE_USER'];
                break;
            default:
                $roles = [];
                break;
        }
        $this->roles = $roles;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZip(): ?int
    {
        return $this->zip;
    }

    public function setZip(?int $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getVerificationCodeEmail(): ?string
    {
        return $this->verificationCodeEmail;
    }

    public function setVerificationCodeEmail(?string $verificationCodeEmail): self
    {
        $this->verificationCodeEmail = $verificationCodeEmail;

        return $this;
    }

    public function getVerificationCodeSms(): ?string
    {
        return $this->verificationCodeSms;
    }

    public function setVerificationCodeSms(?string $verificationCodeSms): self
    {
        $this->verificationCodeSms = $verificationCodeSms;

        return $this;
    }

    public function getVerifiedEmail(): ?bool
    {
        return $this->verifiedEmail;
    }

    public function setVerifiedEmail(?bool $verifiedEmail): self
    {
        $this->verifiedEmail = $verifiedEmail;

        return $this;
    }

    public function getVerifiedSms(): ?bool
    {
        return $this->verifiedSms;
    }

    public function setVerifiedSms(?bool $verifiedSms): self
    {
        $this->verifiedSms = $verifiedSms;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(?string $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return Collection|Blog[]
     */
    public function getBlogs(): Collection
    {
        return $this->blogs;
    }

    public function addBlog(Blog $blog): self
    {
        if (!$this->blogs->contains($blog)) {
            $this->blogs[] = $blog;
            $blog->setCreatedBy($this);
        }

        return $this;
    }

    public function removeBlog(Blog $blog): self
    {
        if ($this->blogs->removeElement($blog)) {
            // set the owning side to null (unless already changed)
            if ($blog->getCreatedBy() === $this) {
                $blog->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Deal[]
     */
    public function getBuyerDeals(): Collection
    {
        return $this->buyerDeals;
    }

    public function addBuyerDeal(Deal $buyerDeal): self
    {
        if (!$this->buyerDeals->contains($buyerDeal)) {
            $this->buyerDeals[] = $buyerDeal;
            $buyerDeal->setBuyer($this);
        }

        return $this;
    }

    public function removeBuyerDeal(Deal $buyerDeal): self
    {
        if ($this->buyerDeals->removeElement($buyerDeal)) {
            // set the owning side to null (unless already changed)
            if ($buyerDeal->getBuyer() === $this) {
                $buyerDeal->setBuyer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Deal[]
     */
    public function getSellerDeals(): Collection
    {
        return $this->sellerDeals;
    }

    public function addSellerDeal(Deal $sellerDeal): self
    {
        if (!$this->sellerDeals->contains($sellerDeal)) {
            $this->sellerDeals[] = $sellerDeal;
            $sellerDeal->setSeller($this);
        }

        return $this;
    }

    public function removeSellerDeal(Deal $sellerDeal): self
    {
        if ($this->sellerDeals->removeElement($sellerDeal)) {
            // set the owning side to null (unless already changed)
            if ($sellerDeal->getSeller() === $this) {
                $sellerDeal->setSeller(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Review[]
     */
    public function getFromReviews(): Collection
    {
        return $this->fromReviews;
    }

    public function addFromReview(Review $fromReview): self
    {
        if (!$this->fromReviews->contains($fromReview)) {
            $this->fromReviews[] = $fromReview;
            $fromReview->setFromUser($this);
        }

        return $this;
    }

    public function removeFromReview(Review $fromReview): self
    {
        if ($this->fromReviews->removeElement($fromReview)) {
            // set the owning side to null (unless already changed)
            if ($fromReview->getFromUser() === $this) {
                $fromReview->setFromUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Review[]
     */
    public function getToReview(): Collection
    {
        return $this->toReview;
    }

    public function addToReview(Review $toReview): self
    {
        if (!$this->toReview->contains($toReview)) {
            $this->toReview[] = $toReview;
            $toReview->setToUser($this);
        }

        return $this;
    }

    public function removeToReview(Review $toReview): self
    {
        if ($this->toReview->removeElement($toReview)) {
            // set the owning side to null (unless already changed)
            if ($toReview->getToUser() === $this) {
                $toReview->setToUser(null);
            }
        }

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->rating;
    }

    public function setRating(?string $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotificationFrom(): Collection
    {
        return $this->notificationFrom;
    }

    public function addNotificationFrom(Notification $notificationFrom): self
    {
        if (!$this->notificationFrom->contains($notificationFrom)) {
            $this->notificationFrom[] = $notificationFrom;
            $notificationFrom->setFromUser($this);
        }

        return $this;
    }

    public function removeNotificationFrom(Notification $notificationFrom): self
    {
        if ($this->notificationFrom->removeElement($notificationFrom)) {
            // set the owning side to null (unless already changed)
            if ($notificationFrom->getFromUser() === $this) {
                $notificationFrom->setFromUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotificationTo(): Collection
    {
        return $this->notificationTo;
    }

    public function addNotificationTo(Notification $notificationTo): self
    {
        if (!$this->notificationTo->contains($notificationTo)) {
            $this->notificationTo[] = $notificationTo;
            $notificationTo->setToUser($this);
        }

        return $this;
    }

    public function removeNotificationTo(Notification $notificationTo): self
    {
        if ($this->notificationTo->removeElement($notificationTo)) {
            // set the owning side to null (unless already changed)
            if ($notificationTo->getToUser() === $this) {
                $notificationTo->setToUser(null);
            }
        }

        return $this;
    }

    public function getLastChangedPersonalInfoAt(): ?\DateTimeInterface
    {
        return $this->lastChangedPersonalInfoAt;
    }

    public function setLastChangedPersonalInfoAt(?\DateTimeInterface $lastChangedPersonalInfoAt): self
    {
        $this->lastChangedPersonalInfoAt = $lastChangedPersonalInfoAt;

        return $this;
    }

    public function getShowPersonalInfo(): ?bool
    {
        return $this->showPersonalInfo;
    }

    public function setShowPersonalInfo(?bool $showPersonalInfo): self
    {
        $this->showPersonalInfo = $showPersonalInfo;

        return $this;
    }

    public function getSomeFacebook(): ?string
    {
        return $this->someFacebook;
    }

    public function setSomeFacebook(?string $someFacebook): self
    {
        $this->someFacebook = $someFacebook;

        return $this;
    }

    public function getSomeInstagram(): ?string
    {
        return $this->someInstagram;
    }

    public function setSomeInstagram(?string $someInstagram): self
    {
        $this->someInstagram = $someInstagram;

        return $this;
    }

    public function getSomeLinkedIn(): ?string
    {
        return $this->someLinkedIn;
    }

    public function setSomeLinkedIn(?string $someLinkedIn): self
    {
        $this->someLinkedIn = $someLinkedIn;

        return $this;
    }

    public function getSomeTwitter(): ?string
    {
        return $this->someTwitter;
    }

    public function setSomeTwitter(?string $someTwitter): self
    {
        $this->someTwitter = $someTwitter;

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMassagesTo(): Collection
    {
        return $this->massagesTo;
    }

    public function addMassagesTo(Message $massagesTo): self
    {
        if (!$this->massagesTo->contains($massagesTo)) {
            $this->massagesTo[] = $massagesTo;
            $massagesTo->setToUser($this);
        }

        return $this;
    }

    public function removeMassagesTo(Message $massagesTo): self
    {
        if ($this->massagesTo->removeElement($massagesTo)) {
            // set the owning side to null (unless already changed)
            if ($massagesTo->getToUser() === $this) {
                $massagesTo->setToUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessagesFrom(): Collection
    {
        return $this->messagesFrom;
    }

    public function addMessagesFrom(Message $messagesFrom): self
    {
        if (!$this->messagesFrom->contains($messagesFrom)) {
            $this->messagesFrom[] = $messagesFrom;
            $messagesFrom->setFromUser($this);
        }

        return $this;
    }

    public function removeMessagesFrom(Message $messagesFrom): self
    {
        if ($this->messagesFrom->removeElement($messagesFrom)) {
            // set the owning side to null (unless already changed)
            if ($messagesFrom->getFromUser() === $this) {
                $messagesFrom->setFromUser(null);
            }
        }

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return Collection|Abuse[]
     */
    public function getAbusesInformer(): Collection
    {
        return $this->abusesInformer;
    }

    public function addAbusesInformer(Abuse $abusesInformer): self
    {
        if (!$this->abusesInformer->contains($abusesInformer)) {
            $this->abusesInformer[] = $abusesInformer;
            $abusesInformer->setInformer($this);
        }

        return $this;
    }

    public function removeAbusesInformer(Abuse $abusesInformer): self
    {
        if ($this->abusesInformer->removeElement($abusesInformer)) {
            // set the owning side to null (unless already changed)
            if ($abusesInformer->getInformer() === $this) {
                $abusesInformer->setInformer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Abuse[]
     */
    public function getAbusers(): Collection
    {
        return $this->abusers;
    }

    public function addAbuser(Abuse $abuser): self
    {
        if (!$this->abusers->contains($abuser)) {
            $this->abusers[] = $abuser;
            $abuser->setAbuser($this);
        }

        return $this;
    }

    public function removeAbuser(Abuse $abuser): self
    {
        if ($this->abusers->removeElement($abuser)) {
            // set the owning side to null (unless already changed)
            if ($abuser->getAbuser() === $this) {
                $abuser->setAbuser(null);
            }
        }

        return $this;
    }

    /**
     * @param bool $onlyFeatured
     * @return array
     */
    public function getImagesFeaturedFirst($onlyFeatured = false): array
    {
        $images = [];

        /** @var Image $image */
        foreach ($this->images as $image) {
            if($image->getFeatured()) {
                array_unshift($images , $image);
            }
            elseif (!$onlyFeatured) {
                $images[] = $image;
            }
        }

        return $images;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setUser($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getUser() === $this) {
                $image->setUser(null);
            }
        }

        return $this;
    }

    public function getDeleteMe(): ?bool
    {
        return $this->deleteMe;
    }

    public function setDeleteMe(?bool $deleteMe): self
    {
        $this->deleteMe = $deleteMe;

        return $this;
    }

    public function getAllowedToSwitchUser(): ?bool
    {
        return $this->allowedToSwitchUser;
    }

    public function setAllowedToSwitchUser(?bool $allowedToSwitchUser): self
    {
        $this->allowedToSwitchUser = $allowedToSwitchUser;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSomeFacebookName()
    {
        return $this->someFacebookName;
    }

    /**
     * @param mixed $someFacebookName
     * @return User
     */
    public function setSomeFacebookName($someFacebookName)
    {
        $this->someFacebookName = $someFacebookName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSomeInstagramName()
    {
        return $this->someInstagramName;
    }

    /**
     * @param mixed $someInstagramName
     * @return User
     */
    public function setSomeInstagramName($someInstagramName)
    {
        $this->someInstagramName = $someInstagramName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSomeLinkedInName()
    {
        return $this->someLinkedInName;
    }

    /**
     * @param mixed $someLinkedInName
     * @return User
     */
    public function setSomeLinkedInName($someLinkedInName)
    {
        $this->someLinkedInName = $someLinkedInName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSomeTwitterName()
    {
        return $this->someTwitterName;
    }

    /**
     * @param mixed $someTwitterName
     * @return User
     */
    public function setSomeTwitterName($someTwitterName)
    {
        $this->someTwitterName = $someTwitterName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSomeInstagramId()
    {
        return $this->someInstagramId;
    }

    /**
     * @param mixed $someInstagramId
     * @return User
     */
    public function setSomeInstagramId($someInstagramId)
    {
        $this->someInstagramId = $someInstagramId;
        return $this;
    }

    public function getLastSmsSent(): ?\DateTimeInterface
    {
        return $this->lastSmsSent;
    }

    public function setLastSmsSent(?\DateTimeInterface $lastSmsSent): self
    {
        $this->lastSmsSent = $lastSmsSent;

        return $this;
    }

}
