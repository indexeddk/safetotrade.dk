<?php

namespace App\Entity;

use App\Repository\AbuseRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=AbuseRepository::class)
 */
class Abuse
{
    use TimestampableEntity;

    const TYPE_REVIEW   = "review";
    const TYPE_USER     = "user";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="guid")
     */
    private $uuid;

    /**
     * @ORM\ManyToOne(targetEntity=Host::class, inversedBy="abuses")
     */
    private $host;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="abuses")
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="abusesInformer")
     */
    private $informer;

    /**
     * @ORM\ManyToOne(targetEntity=Review::class, inversedBy="abuses")
     */
    private $review;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="abusers")
     */
    private $abuser;

    /**
     * @ORM\Column(type="text")
     */
    private $reason;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getHost(): ?Host
    {
        return $this->host;
    }

    public function setHost(?Host $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getInformer(): ?User
    {
        return $this->informer;
    }

    public function setInformer(?User $informer): self
    {
        $this->informer = $informer;

        return $this;
    }

    public function getReview(): ?Review
    {
        return $this->review;
    }

    public function setReview(?Review $review): self
    {
        $this->review = $review;

        return $this;
    }

    public function getAbuser(): ?User
    {
        return $this->abuser;
    }

    public function setAbuser(?User $abuser): self
    {
        $this->abuser = $abuser;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }
}
