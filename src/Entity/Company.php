<?php /** @noinspection PhpUnused */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\host", inversedBy="companies")
     */
    private $host;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Team", mappedBy="company")
     */
    private $teams;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="company")
     */
    private $employees;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Content", mappedBy="company")
     */
    private $contents;

    /**
     * @ORM\OneToMany(targetEntity=Inquiry::class, mappedBy="company")
     */
    private $inquiries;

    /**
     * @ORM\OneToMany(targetEntity=Blog::class, mappedBy="company")
     */
    private $blogs;

    /**
     * @ORM\OneToMany(targetEntity=Deal::class, mappedBy="company")
     */
    private $deals;

    /**
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="company")
     */
    private $reviews;

    /**
     * @ORM\OneToMany(targetEntity=DeletedUser::class, mappedBy="company")
     */
    private $deletedUsers;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="company")
     */
    private $notifications;

    /**
     * @ORM\OneToMany(targetEntity=Abuse::class, mappedBy="company")
     */
    private $abuses;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="company")
     */
    private $images;

    public function __construct()
    {
        $this->teams = new ArrayCollection();
        $this->employees = new ArrayCollection();
        $this->contents = new ArrayCollection();
        $this->inquiries = new ArrayCollection();
        $this->blogs = new ArrayCollection();
        $this->deals = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        $this->deletedUsers = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->abuses = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHost(): ?host
    {
        return $this->host;
    }

    public function setHost(?host $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Team[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
            $team->setCompany($this);
        }

        return $this;
    }

    public function removeTeam(Team $team): self
    {
        if ($this->teams->contains($team)) {
            $this->teams->removeElement($team);
            // set the owning side to null (unless already changed)
            if ($team->getCompany() === $this) {
                $team->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getEmployees(): Collection
    {
        return $this->employees;
    }

    public function addEmployee(User $employee): self
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
            $employee->setCompany($this);
        }

        return $this;
    }

    public function removeEmployee(User $employee): self
    {
        if ($this->employees->contains($employee)) {
            $this->employees->removeElement($employee);
            // set the owning side to null (unless already changed)
            if ($employee->getCompany() === $this) {
                $employee->setCompany(null);
            }
        }

        return $this;
    }
    
    /**
     * @return Collection|Content[]
     */
    public function getContents(): Collection
    {
        return $this->contents;
    }

    public function addContent(Content $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
            $content->setCompany($this);
        }

        return $this;
    }

    public function removeContent(Content $content): self
    {
        if ($this->contents->contains($content)) {
            $this->contents->removeElement($content);
            // set the owning side to null (unless already changed)
            if ($content->getCompany() === $this) {
                $content->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Inquiry[]
     */
    public function getInquiries(): Collection
    {
        return $this->inquiries;
    }

    public function addInquiry(Inquiry $inquiry): self
    {
        if (!$this->inquiries->contains($inquiry)) {
            $this->inquiries[] = $inquiry;
            $inquiry->setCompany($this);
        }

        return $this;
    }

    public function removeInquiry(Inquiry $inquiry): self
    {
        if ($this->inquiries->removeElement($inquiry)) {
            // set the owning side to null (unless already changed)
            if ($inquiry->getCompany() === $this) {
                $inquiry->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Blog[]
     */
    public function getBlogs(): Collection
    {
        return $this->blogs;
    }

    public function addBlog(Blog $blog): self
    {
        if (!$this->blogs->contains($blog)) {
            $this->blogs[] = $blog;
            $blog->setCompany($this);
        }

        return $this;
    }

    public function removeBlog(Blog $blog): self
    {
        if ($this->blogs->removeElement($blog)) {
            // set the owning side to null (unless already changed)
            if ($blog->getCompany() === $this) {
                $blog->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Deal[]
     */
    public function getDeals(): Collection
    {
        return $this->deals;
    }

    public function addDeal(Deal $deal): self
    {
        if (!$this->deals->contains($deal)) {
            $this->deals[] = $deal;
            $deal->setCompany($this);
        }

        return $this;
    }

    public function removeDeal(Deal $deal): self
    {
        if ($this->deals->removeElement($deal)) {
            // set the owning side to null (unless already changed)
            if ($deal->getCompany() === $this) {
                $deal->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Review[]
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setCompany($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getCompany() === $this) {
                $review->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DeletedUser[]
     */
    public function getDeletedUsers(): Collection
    {
        return $this->deletedUsers;
    }

    public function addDeletedUser(DeletedUser $deletedUser): self
    {
        if (!$this->deletedUsers->contains($deletedUser)) {
            $this->deletedUsers[] = $deletedUser;
            $deletedUser->setCompany($this);
        }

        return $this;
    }

    public function removeDeletedUser(DeletedUser $deletedUser): self
    {
        if ($this->deletedUsers->removeElement($deletedUser)) {
            // set the owning side to null (unless already changed)
            if ($deletedUser->getCompany() === $this) {
                $deletedUser->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setCompany($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getCompany() === $this) {
                $notification->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Abuse[]
     */
    public function getAbuses(): Collection
    {
        return $this->abuses;
    }

    public function addAbuse(Abuse $abuse): self
    {
        if (!$this->abuses->contains($abuse)) {
            $this->abuses[] = $abuse;
            $abuse->setCompany($this);
        }

        return $this;
    }

    public function removeAbuse(Abuse $abuse): self
    {
        if ($this->abuses->removeElement($abuse)) {
            // set the owning side to null (unless already changed)
            if ($abuse->getCompany() === $this) {
                $abuse->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setCompany($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getCompany() === $this) {
                $image->setCompany(null);
            }
        }

        return $this;
    }
}
