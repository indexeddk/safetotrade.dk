<?php

namespace App\Entity;

use App\Repository\ReviewRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=ReviewRepository::class)
 */
class Review
{

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="guid")
     */
    private $uuid;

    /**
     * @ORM\ManyToOne(targetEntity=Host::class, inversedBy="reviews")
     */
    private $host;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="reviews")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity=Deal::class, inversedBy="reviews")
     */
    private $deal;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="fromReviews")
     */
    private $fromUser;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="toReview")
     */
    private $toUser;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $internalComment;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $headline;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=Abuse::class, mappedBy="review")
     */
    private $abuses;

    /**
     * @ORM\OneToMany(targetEntity=ReviewChange::class, mappedBy="review")
     */
    private $reviewChanges;

    public function __construct()
    {
        $this->abuses = new ArrayCollection();
        $this->reviewChanges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getFromUser(): ?User
    {
        return $this->fromUser;
    }

    public function setFromUser(?User $fromUser): self
    {
        $this->fromUser = $fromUser;

        return $this;
    }

    public function getToUser(): ?User
    {
        return $this->toUser;
    }

    public function setToUser(?User $toUser): self
    {
        $this->toUser = $toUser;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getInternalComment(): ?string
    {
        return $this->internalComment;
    }

    public function setInternalComment(?string $internalComment): self
    {
        $this->internalComment = $internalComment;

        return $this;
    }

    public function getDeal(): ?Deal
    {
        return $this->deal;
    }

    public function setDeal(?Deal $deal): self
    {
        $this->deal = $deal;

        return $this;
    }

    public function getHost(): ?Host
    {
        return $this->host;
    }

    public function setHost(?Host $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getHeadline(): ?string
    {
        return $this->headline;
    }

    public function setHeadline(?string $headline): self
    {
        $this->headline = $headline;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Abuse[]
     */
    public function getAbuses(): Collection
    {
        return $this->abuses;
    }

    public function addAbuse(Abuse $abuse): self
    {
        if (!$this->abuses->contains($abuse)) {
            $this->abuses[] = $abuse;
            $abuse->setReview($this);
        }

        return $this;
    }

    public function removeAbuse(Abuse $abuse): self
    {
        if ($this->abuses->removeElement($abuse)) {
            // set the owning side to null (unless already changed)
            if ($abuse->getReview() === $this) {
                $abuse->setReview(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ReviewChange[]
     */
    public function getReviewChanges(): Collection
    {
        return $this->reviewChanges;
    }

    public function addReviewChange(ReviewChange $reviewChange): self
    {
        if (!$this->reviewChanges->contains($reviewChange)) {
            $this->reviewChanges[] = $reviewChange;
            $reviewChange->setReview($this);
        }

        return $this;
    }

    public function removeReviewChange(ReviewChange $reviewChange): self
    {
        if ($this->reviewChanges->removeElement($reviewChange)) {
            // set the owning side to null (unless already changed)
            if ($reviewChange->getReview() === $this) {
                $reviewChange->setReview(null);
            }
        }

        return $this;
    }
}
