<?php

namespace App\Command;

use App\Entity\Company;
use App\Entity\Host;
use App\Entity\User;
use App\Service\SwissArmyKnife;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

class SetupCommand extends Command
{
    protected static $defaultName = 'app:setup';

    private $em;
    private $passwordEncoder;
    private $swissArmyKnife;

    /**
     * SetupCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder, SwissArmyKnife $swissArmyKnife)
    {
        parent::__construct();

        /** @var EntityManagerInterface em */
        $this->em = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->swissArmyKnife = $swissArmyKnife;
    }

    protected function configure()
    {
        $this
            ->setDescription('Use this command for the init setup of project');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        //host
        $hosts = $this->em->getRepository(Host::class)->findAll();
        if (sizeof($hosts) == 0) {
            $host = new Host();
            $host->setName("Indexed");
            $host->setDomain("test.indexed.dk");
            $this->em->persist($host);
            $this->em->flush();

            $output->writeln(" -> Host 'test.indexed.dk' created");

        } else {
            $output->writeln(" -> The project already has at least one host");
        }

        //company
        $companies = $this->em->getRepository(Company::class)->findAll();
        if (sizeof($companies) == 0) {
            $company = new Company();
            $company->setName("Indexed");
            $company->setHost($host);
            $this->em->persist($company);
            $this->em->flush();

            $output->writeln(" -> Company 'Indexed' created");

        } else {
            $output->writeln(" -> The project already has at least one company");
        }

        //hostmaster
        $hostmaster = $this->em->getRepository(User::class)->findOneBy(["username" => "hostmaster"]);
        if (!$hostmaster) {

            if ($host && $company) {

                $entity = new User();
                $entity
                    ->setHost($host)
                    ->setCompany($company)
                    ->setUuid(Uuid::v4())
                    ->setEmail("hostmaster")
                    ->setUsername("hostmaster")
                    ->setRoles(["ROLE_HOSTMASTER"])
                    ->setFirstname("Host")
                    ->setLastname("Master");

                //password and secret
                $password = "test";
                $entity
                    ->setPassword($this->passwordEncoder->encodePassword($entity, $password))
                    ->setSecret($this->swissArmyKnife->makeSecret($password));

                $this->em->persist($entity);
                $this->em->flush();

                $output->writeln(" -> 'Hostmaster' created");
            }

        } else {
            $output->writeln(" -> 'Hostmaster' already exists");
        }

        //admin
        $admin = $this->em->getRepository(User::class)->findOneBy(["username" => "admin"]);
        if (!$admin) {

            if ($host && $company) {

                $entity = new User();
                $entity
                    ->setHost($host)
                    ->setCompany($company)
                    ->setUuid(Uuid::v4())
                    ->setEmail("admin")
                    ->setUsername("admin")
                    ->setRoles(["ROLE_ADMIN"])
                    ->setFirstname("admin")
                    ->setLastname("admin");

                //password and secret
                $password = "test";
                $entity
                    ->setPassword($this->passwordEncoder->encodePassword($entity, $password))
                    ->setSecret($this->swissArmyKnife->makeSecret($password));

                $this->em->persist($entity);
                $this->em->flush();

                $output->writeln(" -> 'Admin' created");
            }

        } else {
            $output->writeln(" -> 'Admin' already exists");
        }

        $io->success('Done. Have a nice day!');

        return 0;
    }
}
