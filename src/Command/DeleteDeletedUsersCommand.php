<?php

namespace App\Command;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DeleteDeletedUsersCommand extends Command
{
    protected static $defaultName = "users:cleanup";

    private EntityManagerInterface $em;
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * SetupCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct();

        /** @var EntityManager em */
        $this->em = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        $this
            ->setDescription('Use this command for deleting expired DeletesUsers (+1 year)');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        //host
        $deletedUsers = $this->em->getRepository("App:DeletedUser")->createQueryBuilder("deletedUser")
            ->where("deletedUser.createdAt < :oneYearAgo")
            ->setParameter("oneYearAgo", date("Y-m-d H:i:s", strtotime("-1 year")))
            ->getQuery()
            ->getResult()
        ;

        if ($deletedUsers) {
            foreach ($deletedUsers as $deletedUser) {
                $uuid = $deletedUser->getUuid();
                $this->em->remove($deletedUser);
                $this->em->flush();

                $output->writeln(" -> User ".$uuid." is deleted");
            }
        }
        else {
            $output->writeln(" -> No users to delete");
        }

        $io->success('Done. Have a nice day!');

        return 0;
    }
}
