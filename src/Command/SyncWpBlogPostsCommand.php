<?php

namespace App\Command;

use App\Entity\Blog;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SyncWpBlogPostsCommand extends Command
{
    protected static $defaultName = "app:syncBlogsPosts";

    private EntityManagerInterface $em;

    /**
     * SetupCommand constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        /** @var EntityManager em */
        $this->em = $entityManager;
    }

    protected function configure()
    {
        $this->setDescription('Use this command for syncronising blog posts with the word press site');
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $blogWpIdsById = [];
        $blogs = $this->em->getRepository("App:Blog")->findAll();
        if($blogs) {
            /** @var Blog $blog */
            foreach ($blogs as $blog) {
                $blogWpIdsById[$blog->getId()] = $blog->getWpId();
            }
        }

        $wpBlogs = file_get_contents("https://safe2trade.dk/blogs.json.php");
        $wpBlogs = json_decode($wpBlogs, true);
        foreach ($wpBlogs as $wpBlog) {

            //does blog exist
            if (($key = array_search($wpBlog["id"], $blogWpIdsById)) !== false) {
                unset($blogWpIdsById[$key]);

                //update blog
                $entity = $this->em->getRepository("App:Blog")->findOneBy(["id" => $key]);

                if ($entity) {
                    $entity->setHeadline($wpBlog["title"]);
                    $entity->setText($wpBlog["content"]);
                    $entity->setImage($wpBlog["image"]);
                    $entity->setSlug($wpBlog["permalink"]);

                    $this->em->flush();
                }
            }

            //create blog
            else {
                $host = $this->em->getRepository('App:Host')->findOneBy(['id' => 1]);
                if (!$host) {
                    $host = $this->em->getRepository('App:Host')->findOneBy(['id' => 7]);
                }

                $entity = new Blog();
                $entity->setHost($host);
                $entity->setCreatedAt(new DateTime($wpBlog["datetime"]));
                $entity->setHeadline($wpBlog["title"]);
                $entity->setText($wpBlog["content"]);
                $entity->setImage($wpBlog["image"]);
                $entity->setSlug($wpBlog["permalink"]);
                $entity->setWpId($wpBlog["id"]);

                $this->em->persist($entity);
                $this->em->flush();

                //new
                echo "*";
            }

            //progress
            echo ".";
        }

        //if blog id is still in this array -> delete it
        if ($blogWpIdsById) {
            foreach ($blogWpIdsById as $blogId => $wpId) {
                $blog = $this->em->getRepository("App:Blog")->findOneBy(["id" => $blogId]);
                if ($blog) {
                    $this->em->remove($blog);
                    $this->em->flush();
                    echo "x";
                }
            }
        }

        echo "\n";
        $io->success('Done. Have a nice day!');

        return 0;
    }
}
