<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201117103126 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE form_group DROP FOREIGN KEY FK_EE87E006EDD7E19E');
        $this->addSql('ALTER TABLE reg_value DROP FOREIGN KEY FK_C8CB36424F6E5C03');
        $this->addSql('ALTER TABLE stat_template_form_group DROP FOREIGN KEY FK_8A9796834F6E5C03');
        $this->addSql('ALTER TABLE hospital_category DROP FOREIGN KEY FK_2CD18F3463DBB69');
        $this->addSql('ALTER TABLE registration DROP FOREIGN KEY FK_62A8A7A763DBB69');
        $this->addSql('ALTER TABLE hospital DROP FOREIGN KEY FK_4282C85B98260155');
        $this->addSql('ALTER TABLE reg_value DROP FOREIGN KEY FK_C8CB3642833D8F43');
        $this->addSql('ALTER TABLE stat_template_form_group DROP FOREIGN KEY FK_8A979683DD18F015');
        $this->addSql('DROP TABLE form_group');
        $this->addSql('DROP TABLE hospital');
        $this->addSql('DROP TABLE hospital_category');
        $this->addSql('DROP TABLE reg_value');
        $this->addSql('DROP TABLE region');
        $this->addSql('DROP TABLE registration');
        $this->addSql('DROP TABLE stat_template');
        $this->addSql('DROP TABLE stat_template_form_group');
        $this->addSql('ALTER TABLE user ADD phone VARCHAR(255) DEFAULT NULL, ADD gender VARCHAR(255) DEFAULT NULL, ADD birthday DATE DEFAULT NULL, ADD address VARCHAR(255) DEFAULT NULL, ADD zip INT DEFAULT NULL, ADD city VARCHAR(255) DEFAULT NULL, ADD country VARCHAR(255) DEFAULT NULL, ADD verification_code_email VARCHAR(255) DEFAULT NULL, ADD verification_code_sms VARCHAR(255) DEFAULT NULL, CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE form_group (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, upper_form_group_id INT DEFAULT NULL, name VARCHAR(160) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type VARCHAR(20) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, choices LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:simple_array)\', INDEX IDX_EE87E00612469DE2 (category_id), INDEX IDX_EE87E006EDD7E19E (upper_form_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE hospital (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, region_id INT NOT NULL, name VARCHAR(40) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_4282C85B979B1AD6 (company_id), INDEX IDX_4282C85B98260155 (region_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE hospital_category (hospital_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_2CD18F3412469DE2 (category_id), INDEX IDX_2CD18F3463DBB69 (hospital_id), PRIMARY KEY(hospital_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE reg_value (id INT AUTO_INCREMENT NOT NULL, registration_id INT NOT NULL, form_group_id INT NOT NULL, quantity INT DEFAULT NULL, text VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_C8CB36424F6E5C03 (form_group_id), INDEX IDX_C8CB3642833D8F43 (registration_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE region (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, name VARCHAR(40) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_F62F176979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE registration (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, user_id INT DEFAULT NULL, hospital_id INT DEFAULT NULL, category_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_62A8A7A712469DE2 (category_id), INDEX IDX_62A8A7A763DBB69 (hospital_id), INDEX IDX_62A8A7A7979B1AD6 (company_id), INDEX IDX_62A8A7A7A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE stat_template (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, category_id INT DEFAULT NULL, name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, precedence INT DEFAULT NULL, INDEX IDX_1E57CE1412469DE2 (category_id), INDEX IDX_1E57CE14979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE stat_template_form_group (stat_template_id INT NOT NULL, form_group_id INT NOT NULL, INDEX IDX_8A9796834F6E5C03 (form_group_id), INDEX IDX_8A979683DD18F015 (stat_template_id), PRIMARY KEY(stat_template_id, form_group_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE form_group ADD CONSTRAINT FK_EE87E00612469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE form_group ADD CONSTRAINT FK_EE87E006EDD7E19E FOREIGN KEY (upper_form_group_id) REFERENCES form_group (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE hospital ADD CONSTRAINT FK_4282C85B979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE hospital ADD CONSTRAINT FK_4282C85B98260155 FOREIGN KEY (region_id) REFERENCES region (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE hospital_category ADD CONSTRAINT FK_2CD18F3412469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hospital_category ADD CONSTRAINT FK_2CD18F3463DBB69 FOREIGN KEY (hospital_id) REFERENCES hospital (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reg_value ADD CONSTRAINT FK_C8CB36424F6E5C03 FOREIGN KEY (form_group_id) REFERENCES form_group (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE reg_value ADD CONSTRAINT FK_C8CB3642833D8F43 FOREIGN KEY (registration_id) REFERENCES registration (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F176979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A712469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A763DBB69 FOREIGN KEY (hospital_id) REFERENCES hospital (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A7979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE stat_template ADD CONSTRAINT FK_1E57CE1412469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE stat_template ADD CONSTRAINT FK_1E57CE14979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE stat_template_form_group ADD CONSTRAINT FK_8A9796834F6E5C03 FOREIGN KEY (form_group_id) REFERENCES form_group (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stat_template_form_group ADD CONSTRAINT FK_8A979683DD18F015 FOREIGN KEY (stat_template_id) REFERENCES stat_template (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user DROP phone, DROP gender, DROP birthday, DROP address, DROP zip, DROP city, DROP country, DROP verification_code_email, DROP verification_code_sms, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
