<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324194549 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD some_facebook_name LONGTEXT DEFAULT NULL, ADD some_instagram_name LONGTEXT DEFAULT NULL, ADD some_linked_in_name LONGTEXT DEFAULT NULL, ADD some_twitter_name LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP some_facebook_name, DROP some_instagram_name, DROP some_linked_in_name, DROP some_twitter_name');
    }
}
