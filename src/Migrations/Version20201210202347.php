<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201210202347 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, to_user_id INT DEFAULT NULL, from_user_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', text LONGTEXT DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, seen TINYINT(1) DEFAULT \'0\', seen_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_B6BD307F29F6EE60 (to_user_id), INDEX IDX_B6BD307F2130303A (from_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F29F6EE60 FOREIGN KEY (to_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F2130303A FOREIGN KEY (from_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD last_changed_personal_info_at DATETIME DEFAULT NULL, ADD show_personal_info TINYINT(1) DEFAULT \'0\', ADD some_facebook VARCHAR(255) DEFAULT NULL, ADD some_instagram VARCHAR(255) DEFAULT NULL, ADD some_linked_in VARCHAR(255) DEFAULT NULL, ADD some_twitter VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE message');
        $this->addSql('ALTER TABLE user DROP last_changed_personal_info_at, DROP show_personal_info, DROP some_facebook, DROP some_instagram, DROP some_linked_in, DROP some_twitter');
    }
}
