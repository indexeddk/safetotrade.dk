<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201126123129 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE deal ADD buyer_id INT DEFAULT NULL, ADD seller_id INT DEFAULT NULL, ADD origin VARCHAR(255) NOT NULL, ADD status VARCHAR(255) NOT NULL, ADD product_name VARCHAR(255) NOT NULL, ADD price NUMERIC(12, 2) DEFAULT NULL, ADD currency VARCHAR(10) DEFAULT NULL, ADD product_condition VARCHAR(255) DEFAULT NULL, ADD description LONGTEXT DEFAULT NULL, ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE deal ADD CONSTRAINT FK_E3FEC1166C755722 FOREIGN KEY (buyer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE deal ADD CONSTRAINT FK_E3FEC1168DE820D9 FOREIGN KEY (seller_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_E3FEC1166C755722 ON deal (buyer_id)');
        $this->addSql('CREATE INDEX IDX_E3FEC1168DE820D9 ON deal (seller_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE deal DROP FOREIGN KEY FK_E3FEC1166C755722');
        $this->addSql('ALTER TABLE deal DROP FOREIGN KEY FK_E3FEC1168DE820D9');
        $this->addSql('DROP INDEX IDX_E3FEC1166C755722 ON deal');
        $this->addSql('DROP INDEX IDX_E3FEC1168DE820D9 ON deal');
        $this->addSql('ALTER TABLE deal DROP buyer_id, DROP seller_id, DROP origin, DROP status, DROP product_name, DROP price, DROP currency, DROP product_condition, DROP description, DROP image');
    }
}
