<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200624155226 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE form_group ADD upper_form_group_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE form_group ADD CONSTRAINT FK_EE87E006EDD7E19E FOREIGN KEY (upper_form_group_id) REFERENCES form_group (id)');
        $this->addSql('CREATE INDEX IDX_EE87E006EDD7E19E ON form_group (upper_form_group_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE form_group DROP FOREIGN KEY FK_EE87E006EDD7E19E');
        $this->addSql('DROP INDEX IDX_EE87E006EDD7E19E ON form_group');
        $this->addSql('ALTER TABLE form_group DROP upper_form_group_id, CHANGE category_id category_id INT NOT NULL');
    }
}
