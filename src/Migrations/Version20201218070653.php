<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201218070653 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE abuse (id INT AUTO_INCREMENT NOT NULL, host_id INT DEFAULT NULL, company_id INT DEFAULT NULL, informer_id INT DEFAULT NULL, review_id INT DEFAULT NULL, abuser_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', type VARCHAR(255) NOT NULL, reason VARCHAR(255) NOT NULL, INDEX IDX_98AF83311FB8D185 (host_id), INDEX IDX_98AF8331979B1AD6 (company_id), INDEX IDX_98AF83316FFB3625 (informer_id), INDEX IDX_98AF83313E2E969B (review_id), INDEX IDX_98AF83315DD2A64 (abuser_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, host_id INT DEFAULT NULL, company_id INT DEFAULT NULL, user_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(255) NOT NULL, text VARCHAR(255) DEFAULT NULL, path VARCHAR(255) NOT NULL, precedence INT DEFAULT NULL, featured TINYINT(1) DEFAULT NULL, INDEX IDX_C53D045F1FB8D185 (host_id), INDEX IDX_C53D045F979B1AD6 (company_id), INDEX IDX_C53D045FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE abuse ADD CONSTRAINT FK_98AF83311FB8D185 FOREIGN KEY (host_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE abuse ADD CONSTRAINT FK_98AF8331979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE abuse ADD CONSTRAINT FK_98AF83316FFB3625 FOREIGN KEY (informer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE abuse ADD CONSTRAINT FK_98AF83313E2E969B FOREIGN KEY (review_id) REFERENCES review (id)');
        $this->addSql('ALTER TABLE abuse ADD CONSTRAINT FK_98AF83315DD2A64 FOREIGN KEY (abuser_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F1FB8D185 FOREIGN KEY (host_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE abuse');
        $this->addSql('DROP TABLE image');
    }
}
