<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200702122850 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE stat_template (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, category_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, INDEX IDX_1E57CE14979B1AD6 (company_id), INDEX IDX_1E57CE1412469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stat_template_form_group (stat_template_id INT NOT NULL, form_group_id INT NOT NULL, INDEX IDX_8A979683DD18F015 (stat_template_id), INDEX IDX_8A9796834F6E5C03 (form_group_id), PRIMARY KEY(stat_template_id, form_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stat_template ADD CONSTRAINT FK_1E57CE14979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE stat_template ADD CONSTRAINT FK_1E57CE1412469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE stat_template_form_group ADD CONSTRAINT FK_8A979683DD18F015 FOREIGN KEY (stat_template_id) REFERENCES stat_template (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stat_template_form_group ADD CONSTRAINT FK_8A9796834F6E5C03 FOREIGN KEY (form_group_id) REFERENCES form_group (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stat_template_form_group DROP FOREIGN KEY FK_8A979683DD18F015');
        $this->addSql('DROP TABLE stat_template');
        $this->addSql('DROP TABLE stat_template_form_group');
    }
}
