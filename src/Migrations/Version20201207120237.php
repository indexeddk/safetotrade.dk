<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201207120237 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE deleted_user (id INT AUTO_INCREMENT NOT NULL, host_id INT DEFAULT NULL, company_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', email VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, zip VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, INDEX IDX_D8486B5C1FB8D185 (host_id), INDEX IDX_D8486B5C979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE poke (id INT AUTO_INCREMENT NOT NULL, poked_by_id INT DEFAULT NULL, poked_id INT NOT NULL, seen TINYINT(1) DEFAULT NULL, INDEX IDX_B221D4261D900889 (poked_by_id), INDEX IDX_B221D426AA290927 (poked_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE deleted_user ADD CONSTRAINT FK_D8486B5C1FB8D185 FOREIGN KEY (host_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE deleted_user ADD CONSTRAINT FK_D8486B5C979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE poke ADD CONSTRAINT FK_B221D4261D900889 FOREIGN KEY (poked_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE poke ADD CONSTRAINT FK_B221D426AA290927 FOREIGN KEY (poked_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE deleted_user');
        $this->addSql('DROP TABLE poke');
    }
}
