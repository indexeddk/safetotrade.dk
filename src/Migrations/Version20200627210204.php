<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200627210204 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE registration ADD hospital_id INT DEFAULT NULL, ADD category_id INT NOT NULL');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A763DBB69 FOREIGN KEY (hospital_id) REFERENCES hospital (id)');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A712469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_62A8A7A763DBB69 ON registration (hospital_id)');
        $this->addSql('CREATE INDEX IDX_62A8A7A712469DE2 ON registration (category_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE registration DROP FOREIGN KEY FK_62A8A7A763DBB69');
        $this->addSql('ALTER TABLE registration DROP FOREIGN KEY FK_62A8A7A712469DE2');
        $this->addSql('DROP INDEX IDX_62A8A7A763DBB69 ON registration');
        $this->addSql('DROP INDEX IDX_62A8A7A712469DE2 ON registration');
        $this->addSql('ALTER TABLE registration DROP hospital_id, DROP category_id');
    }
}
