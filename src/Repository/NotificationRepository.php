<?php

namespace App\Repository;

use App\Entity\Host;
use App\Entity\Notification;
use App\Entity\User;
use App\Service\HostHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Notification|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notification|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notification[]    findAll()
 * @method Notification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @property Host host
 */
class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, HostHelper $hostHelper)
    {
        parent::__construct($registry, Notification::class);
        $this->host = $hostHelper->getHost();
    }

    public function getTodaysPokes(User $fromUser)
    {
        $notification = new Notification();

        return $this->createQueryBuilder("notification")
            ->where("notification.host = :host")
            ->setParameter("host", $this->host)
            ->andWhere("notification.type = :type")
            ->setParameter("type", $notification::TYPE_POKE)
            ->andWhere("notification.fromUser = :me")
            ->setParameter("me", $fromUser)
            ->andWhere("notification.createdAt LIKE :today")
            ->setParameter("today", date("Y-m-d")."%")
            ->getQuery()
            ->getResult()
        ;
    }
}
