<?php

namespace App\Repository;

use App\Entity\Blog;
use App\Service\HostHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Blog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Blog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Blog[]    findAll()
 * @method Blog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @property \App\Entity\Host|object host
 */
class BlogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, HostHelper $hostHelper)
    {
        parent::__construct($registry, Blog::class);
        $this->host = $hostHelper->getHost();
    }

    public function getEntityQuery($q)
    {
        if ($q) {
            $qFields = ["productName"];
            $searchWhere = "entity.".implode(" LIKE :q OR entity.", $qFields)." LIKE :q";

            return $this->createQueryBuilder("entity")
                ->where("entity.host = :host")
                ->setParameter("host", $this->host)
                ->andWhere($searchWhere)
                ->setParameter('q', "%".$q."%")
                ->orderBy("entity.createdAt", "DESC")
                ->getQuery()
                ->getResult()
                ;
        }
        else {
            return $this->createQueryBuilder("entity")
                ->where("entity.host = :host")
                ->setParameter("host", $this->host)
                ->orderBy("entity.createdAt", "DESC")
                ->getQuery()
                ->getResult()
                ;
        }
    }

}
