<?php

namespace App\Repository;

use App\Entity\Review;
use App\Service\HostHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @property \App\Entity\Host|object host
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, HostHelper $hostHelper)
    {
        parent::__construct($registry, Review::class);
        $this->host = $hostHelper->getHost();
    }

    public function getEntityQuery($q)
    {
        if ($q) {
            $qFields = ["productName"];
            $searchWhere = "entity.".implode(" LIKE :q OR entity.", $qFields)." LIKE :q";

            return $this->createQueryBuilder("entity")
                ->leftJoin("entity.abuses", "abuse")
                ->where("entity.host = :host")
                ->setParameter("host", $this->host)
                ->andWhere($searchWhere)
                ->setParameter('q', "%".$q."%")
                ->orderBy("entity.createdAt", "DESC")
            ;
        }
        else {
            return $this->createQueryBuilder("entity")
                ->where("entity.host = :host")
                ->setParameter("host", $this->host)
                ->orderBy("entity.createdAt", "DESC")
            ;
        }
    }

}
