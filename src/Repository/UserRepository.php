<?php

namespace App\Repository;

use App\Entity\Host;
use App\Entity\User;
use App\Service\HostHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Symfony\Component\Security\Core\Security;

/**
 * @method User|null find($id, $lockMode = NULL, $lockVersion = NULL)
 * @method User|null findOneBy(array $criteria, array $orderBy = NULL)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
 * @property Host|object host
 * @property Security security
 */
class UserRepository extends ServiceEntityRepository
{

    /** @var HostHelper $hostHelper */
    private $hostHelper;

    /**
     * UserRepository constructor.
     *
     * @param ManagerRegistry $registry
     * @param HostHelper $hostHelper
     * @param Security $security
     */
    public function __construct(ManagerRegistry $registry, HostHelper $hostHelper, Security $security)
    {
        parent::__construct($registry, User::class);
        $this->hostHelper = $hostHelper;
        $this->host = $hostHelper->getHost();
        $this->security = $security;
    }

    /**
     * @param $q
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserEntityQuery($q)
    {
        if ($q) {

            $qFields = ["firstname","lastname","email","phone","address","zip","city"];
            $searchWhere = "entity.".implode(" LIKE :q OR entity.", $qFields)." LIKE :q OR CONCAT(entity.firstname,' ',entity.lastname) LIKE :q";

            $queryBuilder = $this->createQueryBuilder('entity')
                ->where('entity.host = :host')
                ->setParameter('host', $this->host)
                ->andWhere('entity.rolesHierarchy < :role')
                ->setParameter('role', User::ROLE_VOLUNTARY)
                ->andWhere($searchWhere)
                ->setParameter('q', "%".$q."%")
                ->orderBy("entity.deleteMe", "DESC")
                ->addOrderBy("entity.createdAt", "DESC")
            ;
        }
        else {
            $queryBuilder = $this->createQueryBuilder('entity')
                ->where('entity.host = :host')
                ->setParameter('host', $this->host)
                ->andWhere('entity.rolesHierarchy < :role')
                ->andWhere('entity.deleted IS NULL')
                ->setParameter('role', User::ROLE_VOLUNTARY)
                ->orderBy("entity.deleteMe", "DESC")
                ->addOrderBy("entity.createdAt", "DESC")
            ;
        }

        return $queryBuilder;
    }

    /**
     * @param $q
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getEmployeeEntityQuery($q)
    {
        if ($q) {

            $qFields = ["firstname","lastname","email","username"];
            $searchWhere = "entity.".implode(" LIKE :q OR entity.", $qFields)." LIKE :q OR CONCAT(entity.firstname,' ',entity.lastname) LIKE :q";

            $queryBuilder = $this->createQueryBuilder('entity')
                ->where('entity.host = :host')
                ->setParameter('host', $this->host)
                ->andWhere('entity.company = :company')
                ->setParameter('company', $this->security->getUser()->getCompany())
                ->andWhere('entity.rolesHierarchy >= :role')
                ->setParameter('role', User::ROLE_VOLUNTARY)
                ->andWhere($searchWhere)
                ->setParameter('q', "%".$q."%")
            ;
        }
        else {
            $queryBuilder = $this->createQueryBuilder('entity')
                ->where('entity.host = :host')
                ->setParameter('host', $this->host)
                ->andWhere('entity.company = :company')
                ->setParameter('company', $this->security->getUser()->getCompany())
                ->andWhere('entity.rolesHierarchy >= :role')
                ->setParameter('role', User::ROLE_VOLUNTARY);
        }

        return $queryBuilder;
    }

    /**
     * @param $q
     * @return Query
     */
    public function getBySearch($q)
    {
        $qFields = ["email", "firstname", "lastname"];
        $aWhereString = "entity." . implode(" LIKE :q OR entity.", $qFields) . " LIKE :q";

        return $this->createQueryBuilder("entity")

            //host
            ->innerJoin("entity.host", "host")
            ->where("host = :host")
            ->setParameter("host", $this->hostHelper->getHost())

            //role
            ->andWhere("entity.roles LIKE :role")
            ->setParameter("role", "%ROLE_ADMIN%")

            //slug
            ->andWhere("entity.slug IS NOT NULL")

            //q
            ->andWhere($aWhereString)
            ->setParameter("q", "%" . $q . "%")
            ->orderBy("entity.id", "ASC")
            ->getQuery();
    }

    /**
     * @param Host|null $host
     * @return Query
     */
    public function getAllAdminsByHost(Host $host = NULL)
    {
        $builder = $this->createQueryBuilder("entity");

        //host
        $builder
            ->innerJoin("entity.host", "host")
            ->where("host = :host")
            ->andWhere("entity.slug IS NOT NULL")
        ;


        if ($host) {
            $builder->setParameter("host", $host);
        } else {
            $builder->setParameter("host", $this->hostHelper->getHost());
        }

        //role
        //$builder
        //    ->andWhere("entity.roles LIKE :role")
        //   ->setParameter("role", "%ROLE_ADMIN%")
        //   ->orderBy("entity.id", "ASC");

        return $builder->getQuery();
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function getAllAdmins()
    {
        return $this->createQueryBuilder("entity")
            ->where("entity.roles LIKE :role")
            ->setParameter("role", "%ROLE_ADMIN%")
            ->orderBy("entity.id", "ASC")
//            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder("u")
            ->andWhere("u.exampleField = :val")
            ->setParameter("val", $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
