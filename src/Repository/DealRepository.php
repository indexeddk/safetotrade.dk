<?php

namespace App\Repository;

use App\Entity\Deal;
use App\Service\HostHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Deal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Deal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Deal[]    findAll()
 * @method Deal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @property \App\Entity\Host|object host
 */
class DealRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, HostHelper $hostHelper)
    {
        parent::__construct($registry, Deal::class);
        $this->host = $hostHelper->getHost();
    }

    public function getEntityQuery($q)
    {
        if ($q) {
            $qFields = ["productName"];
            $searchWhere = "entity.".implode(" LIKE :q OR entity.", $qFields)." LIKE :q";

            return $this->createQueryBuilder("entity")
                ->where("entity.host = :host")
                ->setParameter("host", $this->host)
                ->andWhere($searchWhere)
                ->setParameter('q', "%".$q."%")
                ->addOrderBy("entity.createdAt", "DESC")
            ;
        }
        else {
            return $this->createQueryBuilder("entity")
                ->where("entity.host = :host")
                ->setParameter("host", $this->host)
                ->addOrderBy("entity.createdAt", "DESC")
            ;
        }
    }
}
