<?php

namespace App\Repository;

use App\Entity\Message;
use App\Service\HostHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @property \App\Entity\Host|object host
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, HostHelper $hostHelper)
    {
        parent::__construct($registry, Message::class);
        $this->host = $hostHelper->getHost();
    }

    public function getEntityQuery($q)
    {
        $fromUserWhere = "fromUser.firstname LIKE :q OR fromUser.lastname LIKE :q OR CONCAT(fromUser.firstname,' ',fromUser.lastname) LIKE :q";
        $toUserWhere = "toUser.firstname LIKE :q OR toUser.lastname LIKE :q OR CONCAT(toUser.firstname,' ',toUser.lastname) LIKE :q";
        
        if ($q) {
            return $this->createQueryBuilder("entity")
                ->leftJoin("entity.fromUser", "fromUser")
                ->leftJoin("entity.toUser", "toUser")
                ->where("entity.host = :host")
                ->setParameter("host", $this->host)
                ->andWhere("entity.text LIKE :q OR ".$fromUserWhere." OR ".$toUserWhere)
                ->setParameter('q', "%".$q."%")
                ->orderBy("entity.createdAt", "DESC")
            ;
        }
        else {
            return $this->createQueryBuilder("entity")
                ->where("entity.host = :host")
                ->setParameter("host", $this->host)
                ->orderBy("entity.createdAt", "DESC")
            ;
        }
    }

}
