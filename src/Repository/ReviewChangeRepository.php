<?php

namespace App\Repository;

use App\Entity\ReviewChange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReviewChange|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReviewChange|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReviewChange[]    findAll()
 * @method ReviewChange[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewChangeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReviewChange::class);
    }

    // /**
    //  * @return ReviewChange[] Returns an array of ReviewChange objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReviewChange
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
