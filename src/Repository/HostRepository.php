<?php

namespace App\Repository;

use App\Entity\Host;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Host|null find($id, $lockMode = null, $lockVersion = null)
 * @method Host|null findOneBy(array $criteria, array $orderBy = null)
 * @method Host[]    findAll()
 * @method Host[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HostRepository extends ServiceEntityRepository
{
    /**
     * HostRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Host::class);
    }

    /**
     * @param $q
     * @return \Doctrine\ORM\Query
     */
    public function getBySearch($q)
    {
        $qFields = ["name","domain"];
        $aWhereString = "entity.".implode(" LIKE :q OR entity.", $qFields)." LIKE :q";

        return $this->createQueryBuilder("entity")

            //q
            ->andWhere($aWhereString)
            ->setParameter("q", "%".$q."%")

            ->orderBy("entity.id", "ASC")
            ->getQuery()
            ;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllHosts()
    {
        return $this->createQueryBuilder("entity")
            ->orderBy("entity.id", "DESC")
            ->getQuery()
            ;
    }
}
